using System;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class LinkWindow {
  public Entity ent;
  public Rect rect;
  public List<LinkWindow> children;

  public LinkWindow(Entity ent) {
    this.ent = ent;
    children = new List<LinkWindow>();
  }

  public override string ToString() {
    return ent.id;
  }

}
