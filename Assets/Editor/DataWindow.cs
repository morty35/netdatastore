using System;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using Object = System.Object;

namespace DS {
	
	public class DataWindow : EditorWindow {
		public bool debug = false;
		public bool isInit = false;

		public const int NUM_COLUMNS = 3;
		public const int NUM_LINK_COL = 3;
	
		// dimension variables
		public int buttonWidth = 140;
		public int linkWidth = 300;
		public int fieldWidth = 120;
		public int xButtonWidth = 30;
		public Vector2 scrollPos = Vector2.zero;
		public int scrollWidth = 400;
		public int scrollHeight = 600;

		// edit variables
		public bool edit = false;
		public bool lastEdit = false;

		public bool lastUseNames = true;
		public bool useNames = true;
		
		public DataSet data = Data.master;

		//------------------------------------------------------------
		[MenuItem("Window/Data Window")] 
		public static void ShowWindow() {
			EditorWindow.GetWindow(typeof(DataWindow));
		}

		//------------------------------------------------------------
		public void init() {
			resetData();
		}

		//------------------------------------------------------------
		public void resetData() {
			if (debug) {
				Debug.Log("resetData");
			}
			data = Data.master;

			if (data != null) {
				if (debug) {
					Debug.Log("init");
				}
				isInit = true;
			}
			refresh();
			mode = DataWindowMode.entities;
			selEnt = null;
		}

		//------------------------------------------------------------
		public void clearData() {
			if (debug) {
				Debug.Log("clearData");
			}

			data = Data.master;
			data.clearAll();
		
			mode = DataWindowMode.entities;
			selEnt = null;
			activeItem = -1;
			currentGraphType = null;
			refresh();
		}
		

		//------------------------------------------------------------
		public void refresh() {
			if (debug) {
				Debug.Log("refresh");
			}
			nodeTypeList = null;			
			nodeSetList = null;
			nodeList = null;
			linkTypeList = null;			
			linkSetList = null;
			linkList = null;
			groupEnts = null;
			parentTypes = null;
			parentSets = null;
			childTypes = null;
			childSets = null;
			fieldValues = new Dictionary<Field, string>();
			fieldColors = new Dictionary<Field, Color>();
			fieldListValues = new Dictionary<Field, List<string>>();
			fieldListColors = new Dictionary<Field, List<Color>>();
			selNodeSets = null;
			selNodeTypes = null;
			selLinkSets = null;
			newNodeTypes = null;
			newNodeSets = null;
			newLinkSets = null;
			parentLinks = null;
			childLinks = null;
			linkWindows = null;
			linkTypes = null;
			selectedField = null;
			selectedFieldInt = -1;
			editFieldText = "";

			loadLinkData();
			loadEntitiesData();
		}

		//------------------------------------------------------------
		GUIStyle buttonStyle;
		public void OnGUI() {
			buttonStyle = new GUIStyle(GUI.skin.button);
			
			// scrollview
			if (mode != DataWindowMode.graphs) {
				scrollPos = GUILayout.BeginScrollView(scrollPos);
			}
			else {
				scrollPos = GUI.BeginScrollView(new Rect(0,0,position.width, position.height), 
												scrollPos, new Rect(0,0,maxXSize, maxYSize));
			}

			if (!isInit) {
				init();
			}

			// Header
			displayRefreshReset();

			// File Loader
			displayFileLoader();

			// DataFilter
			displayDataFilters();
			
			// Edit toggle
			edit = GUILayout.Toggle(edit, "Edit");
			if (lastEdit != edit) {
				refresh();
				lastEdit = edit;
			}
			// Names toggle
			useNames = GUILayout.Toggle(useNames, "Use Names");
			if (lastUseNames != useNames) {
				refresh();
				lastUseNames = useNames;
			}
			GUILayout.Space(10);

			GUILayout.Space(10);

			// Display Modes
			displayModes();

			if (mode != DataWindowMode.graphs) {
				GUILayout.EndScrollView();
			}
			else {
				GUI.EndScrollView();
			}

		}

		//------------------------------------------------------------
		public void displayEntitiesWindow() {
			if (data != null) {

				// Entities Header
				GUILayout.BeginHorizontal();
				GUILayout.Label("Entities", EditorStyles.boldLabel);
				if (GUILayout.Button("All", GUILayout.Width(50))) {
					selectAllEntities();
				}
				if (GUILayout.Button("None", GUILayout.Width(50))) {
					selectNoneEntities();
				}
				GUILayout.EndHorizontal();
				GUILayout.Space(10);

				loadEntitiesData();

				// Add entity
				if (edit) {
					displayAddEntity();
				}

				// display NodeTypes
				displayEntitiesNodeTypes();

				// display NodeSets
				displayEntitiesNodeSets();

				// display Nodes
				displayEntitiesNodes();

				// display LinkTypes
				displayEntitiesLinkTypes();

				// display LinkSets
				displayEntitiesLinkSets();

				// display Links
				displayEntitiesLinks();
			}

		}

		//------------------------------------------------------------
		public Dictionary<Field, string> fieldValues = new Dictionary<Field, string>();
		public Dictionary<Field, Color> fieldColors = new Dictionary<Field, Color>();
		public Dictionary<Field, List<string>> fieldListValues = new Dictionary<Field, List<string>>();
		public Dictionary<Field, List<Color>> fieldListColors = new Dictionary<Field, List<Color>>();
		public bool specific = false;

		public void displayFieldsWindow() {
			List<Entity> allEnts;
			if (useFilter && activeFilter != null) {
				allEnts = new List<Entity>(activeFilter.matches(data.allEnts(), data));
			}
			else {
				allEnts = new List<Entity>(data.allEnts());
			}
			allEnts.Sort();

			specific = GUILayout.Toggle(specific, "Specific Fields");
			GUILayout.Space(10);

			if (edit) {
				displayEditAddField(null);
				GUILayout.Space(10);		
			}


			foreach (Entity ent in allEnts) {
				List<Field> eFields = new List<Field>(data.fields(ent, specific));
				if (eFields == null || eFields.Count == 0) {
					continue;
				}
				// Display ent button
				if (entButton(ent)) {
					selectEnt(ent);
					return;
				}
				displayFieldsForEnt(ent);
				GUILayout.Space(10);
			}
		}

		//------------------------------------------------------------
		public void selectEnt(Entity ent) {
			selEnt = ent;
			mode = DataWindowMode.entity;
			if (ent.type == EntType.linkType) {
				currentGraphType = ent;
				loadLinkData();
			}
			refresh();
		}

		//------------------------------------------------------------
		public Entity selEnt = null;

		public void displayEntityWindow() {
			// if nothing selected, swtich to all entities
			if (selEnt == null) {
				mode = DataWindowMode.entities;
				return;
			}
		
			specific = GUILayout.Toggle(specific, "Specific Fields");
			GUILayout.Space(10);

			// Display Ent Type and id
			displayEntityHeader();

			if (selEnt == null) {
				return;
			}

			// NodeType, NodeSet, LinkType, LinkSet
			if (selEnt.type == EntType.nodeType 
				|| selEnt.type == EntType.nodeSet 
				|| selEnt.type == EntType.linkType
				|| selEnt.type == EntType.linkSet) {

				displayEntityTypeSet();
			} // end display nodes in nodeType
			GUILayout.Space(10);

			// Node
			if (selEnt.type == EntType.node) {
				displayEntityNode(selEnt);
			}

			// Link
			if (selEnt.type == EntType.link) {
				displayEntityLink();
			}

			//Fields
			GUILayout.Space(10);		
			GUILayout.Label("Fields", EditorStyles.boldLabel);
			GUILayout.Space(10);		

			if (edit) {
				displayEditAddField(selEnt);
				GUILayout.Space(10);		
				GUILayout.Label("Entity Fields", EditorStyles.boldLabel);
				GUILayout.Space(10);		
			}

			displayFieldsForEnt(selEnt);
		}

		//------------------------------------------------------------
		public Field selectedField = null;
		public int selectedFieldInt = -1;
		public string editFieldText = "";
		
		public void displayFieldsForEnt(Entity ent) {
			if (ent == null) {
				return;
			}

			List<Field> eFields = new List<Field>(data.fields(ent, specific));
			eFields.Sort();
			foreach (Field field in eFields) {
				// single
				if (field.fType == FieldType.single) {
					displaySingleField(field);
				}
				else if (field.fType == FieldType.list) {
					displayListField(field);
				}
			}

			// selectedField can only be string type
			if (selectedField != null && mode != DataWindowMode.fields) {
				GUILayout.Space(10);
				editFieldText = GUILayout.TextArea(editFieldText, GUILayout.Width(NUM_COLUMNS * buttonWidth));
				if (GUILayout.Button("Edit", GUILayout.Width(50))) {
					if (selectedField.fType == FieldType.single) {
						data.edit(selectedField, editFieldText);
					}
					else {
						data.edit(selectedField, selectedFieldInt, editFieldText);
					}
				}
			}
		}

		//************************************************************
		// Private methods
		//************************************************************

		private void addEnt(EntType type, string editAddEnt, 
							int editLinkType, int editLinkParent,
							int editLinkChild) {
			switch (type) {
			case EntType.nodeType:
				data.addNodeType(editAddEnt);
				break;
			case EntType.nodeSet:
				data.addNodeSet(editAddEnt);
				break;
			case EntType.node:
				data.addNode(editAddEnt);
				break;
			case EntType.linkType:
				data.addLinkType(editAddEnt);
				break;
			case EntType.linkSet:
				data.addLinkSet(editAddEnt);
				break;
			case EntType.link:
				data.addLink(data[editAddLinkTypeArray[editLinkType]],
							 data[editAddLinkNodeArray[editLinkParent]],
							 data[editAddLinkNodeArray[editLinkChild]]);
				break;
			}
		}

		//------------------------------------------------------------
		private void setEntColor(EntType type) { 
			switch (type) {
			case EntType.nodeType:
				GUI.backgroundColor = Color.cyan;
				break;
			case EntType.nodeSet:
				GUI.backgroundColor = new Color(.6f, .6f, 1f);
				break;
			case EntType.node:
				GUI.backgroundColor = Color.green;
				break;
			case EntType.linkType:
				GUI.backgroundColor = Color.red;
				break;
			case EntType.linkSet:
				GUI.backgroundColor = Color.magenta;
				break;
			case EntType.link:
				GUI.backgroundColor = Color.yellow;
				break;
			}
		}
		//------------------------------------------------------------
		private Color oldColor = GUI.backgroundColor;
		private void resetColor() {
			GUI.backgroundColor = oldColor;
		}

		//------------------------------------------------------------
		private bool entButton(Entity ent) {
			setEntColor(ent.type);
			if (ent.type == EntType.link) {
				if (GUILayout.Button(ent.id, buttonStyle, GUILayout.Width(linkWidth))) {
					resetColor();
					return true;
				}
				else {
					resetColor();
					return false;
				}
			}
			else {
				string id = ent.id;
				if (useNames && data.exists(ent.id, "name") &&
					data.data<string>(ent.id, "name") != "") {
					
					id = data.data<string>(ent, "name");
				}

				if (GUILayout.Button(id, buttonStyle, GUILayout.Width(buttonWidth))) {
					resetColor();
					return true;
				}
				else {
					resetColor();
					return false;
				}
			}			
		}		
		//------------------------------------------------------------
		private bool xButton(Entity xEnt) {
			if (xEnt != null) {
				setEntColor(xEnt.type);
			}
			if (GUILayout.Button("X", GUILayout.Width(xButtonWidth))) {
				resetColor();
				return true;
			}
			else {
				resetColor();
				return false;
			}
		}
		
		//------------------------------------------------------------
		private void selectAllEntities() {
			displayNodeTypes		= true;
			displayNodeSets			= true;
			displayNodes			= true;
			displayLinkTypes		= true;
			displayLinkSets			= true;
			displayLinks			= true;
		}

		//------------------------------------------------------------
		private void selectNoneEntities() {
			displayNodeTypes		= false;
			displayNodeSets			= false;
			displayNodes			= false;
			displayLinkTypes		= false;
			displayLinkSets			= false;
			displayLinks			= false;
		}

		//------------------------------------------------------------
		private EntType addFieldEntType = EntType.node;
		private string addFieldEntS = "";
		private FieldType addFieldType = FieldType.single;
		private DataType addFieldDataType = DataType.intType;
		private string addFieldString = "";
		private string addFieldData = "";
		private Color addFieldColor = Color.white;

		private void displayEditAddField(Entity ent) {
			GUILayout.Label("Add Field");

			if (ent != null) {
				addFieldEntType = ent.type;
			}
		
			// select ent
			if (ent == null) {
				GUILayout.BeginHorizontal();
				addFieldEntType = (EntType) EditorGUILayout.Popup((int)addFieldEntType, Enum.GetNames(typeof(EntType)), GUILayout.Width(buttonWidth));
				addFieldEntS = GUILayout.TextField(addFieldEntS, GUILayout.Width(buttonWidth));
				GUILayout.EndHorizontal();
			}

			// fieldType and DataType
			GUILayout.BeginHorizontal();
			addFieldType = (FieldType) EditorGUILayout.Popup((int) addFieldType, Enum.GetNames(typeof(FieldType)), GUILayout.Width(buttonWidth));
			addFieldDataType = (DataType)EditorGUILayout.Popup((int)addFieldDataType, Enum.GetNames(typeof(DataType)), GUILayout.Width(buttonWidth));
			GUILayout.EndHorizontal();

			// field id and data value
			GUILayout.BeginHorizontal();
			GUILayout.Label("ID", GUILayout.Width(20));
			addFieldString = GUILayout.TextField(addFieldString, GUILayout.Width(buttonWidth));
			if (addFieldDataType == DataType.colorType) {
				GUILayout.Label("Color", GUILayout.Width(35));
				addFieldColor = EditorGUILayout.ColorField(addFieldColor, GUILayout.Width(buttonWidth)); 
			}
			else {
				GUILayout.Label("Data", GUILayout.Width(35));
				addFieldData = GUILayout.TextField(addFieldData, GUILayout.Width(buttonWidth));
			}

			// add Field button
			if (GUILayout.Button("Add Field", GUILayout.Width(buttonWidth))) {
				Entity addFieldEnt;
				if (ent == null) {
					addFieldEnt = data[addFieldEntS];
				}
				else {
					addFieldEnt = ent;
				}

				FieldType fType = addFieldType;
				switch(addFieldDataType) {
				case DataType.intType:
					int valueInt;
					if (int.TryParse(addFieldData, out valueInt)) {
						// single
						if (fType == FieldType.single) {
							data.addField<int>(addFieldEnt, addFieldString, valueInt);
						}
						// list
						else if (fType == FieldType.list) {
							Field fieldList;
							if (!data.exists(addFieldEnt.id, addFieldString)) {
								fieldList = data.addFieldList<int>(addFieldEnt, addFieldString);
							}
							else {
								fieldList = data.field(addFieldEnt, addFieldString);
							}
							data.addToDataList(fieldList, valueInt);
						}
					}
					else {
						Debug.LogError("Invalid value: " + addFieldData);
					}
					break;
				case DataType.colorType:
					// single
					if (fType == FieldType.single) {
						data.addField<Color>(addFieldEnt, addFieldString, addFieldColor);
					}
					// list
					else if (fType == FieldType.list) {
						Field fieldList;
						if (!data.exists(addFieldEnt.id, addFieldString)) {
							fieldList = data.addFieldList<Color>(addFieldEnt, addFieldString);
						}
						else {
							fieldList = data.field(addFieldEnt, addFieldString);
						}
						data.addToDataList(fieldList, addFieldColor);
					}
					break;
				case DataType.floatType:
					float valueFl;
					if (float.TryParse(addFieldData, out valueFl)) {
						// single
						if (fType == FieldType.single) {
							data.addField<float>(addFieldEnt, addFieldString, valueFl);
						}
						// list
						else if (fType == FieldType.list) {
							Field fieldList;
							if (!data.exists(addFieldEnt.id, addFieldString)) {
								fieldList = data.addFieldList<float>(addFieldEnt, addFieldString);
							}
							else {
								fieldList = data.field(addFieldEnt, addFieldString);
							}
							data.addToDataList(fieldList, valueFl);
						}
					}
					else {
						Debug.LogError("Invalid value: " + addFieldData);
					}
					break;
				case DataType.stringType:
					// single
					if (fType == FieldType.single) {
						data.addField<string>(addFieldEnt, addFieldString, addFieldData);
					}
					// list
					else if (fType == FieldType.list) {
						Field fieldList;
						if (!data.exists(addFieldEnt.id, addFieldString)) {
							fieldList = data.addFieldList<string>(addFieldEnt, addFieldString);
						}
						else {
							fieldList = data.field(addFieldEnt, addFieldString);
						}
						data.addToDataList(fieldList, addFieldData);
					}
					break;
				case DataType.entityType:
					if (addFieldData == "") {
						data.addField<Entity>(addFieldEnt, addFieldString, null);
					}
					else if (data.exists(addFieldData)) {
						Entity valueEnt = data[addFieldData];
						// single
						if (fType == FieldType.single) {
							data.addField<Entity>(addFieldEnt, addFieldString, valueEnt);
						}
						// list
						else if (fType == FieldType.list) {
							Field fieldList;
							if (!data.exists(addFieldEnt.id, addFieldString)) {
								fieldList = data.addFieldList<Entity>(addFieldEnt, addFieldString);
							}
							else {
								fieldList = data.field(addFieldEnt, addFieldString);
							}
							data.addToDataList(fieldList, valueEnt);
						}
					}
					else {
						Debug.LogError("Invalid value: " + addFieldData);
					}
					break;
				case DataType.dataType: 
					DataObject dataO = DataList.getNewDataObject(addFieldData);
					// single
					if (fType == FieldType.single) {
						data.addField<DataObject>(addFieldEnt, addFieldString, dataO);
					}
					// list
					else if (fType == FieldType.list) {
						Field fieldList;
						if (!data.exists(addFieldEnt.id, addFieldString)) {
							fieldList = data.addFieldList<DataObject>(addFieldEnt, addFieldString);
						}
						else {
							fieldList = data.field(addFieldEnt, addFieldString);
						}
						data.addToDataList(fieldList, dataO);
					}
					break;
				}
			}
			GUILayout.EndHorizontal();
		}

		//------------------------------------------------------------
		private void loadEntitiesData() {
			if (nodeTypeList == null) {
				if (useFilter && activeFilter != null) {
					nodeTypeList = new List<Entity>(activeFilter.matches(data.allNodeTypes(), data));
				}
				else {
					nodeTypeList = new List<Entity>(data.allNodeTypes());
				}
				nodeTypeList.Sort();
			}
			if (nodeSetList == null) {
				if (useFilter && activeFilter != null) {
					nodeSetList = new List<Entity>(activeFilter.matches(data.allNodeSets(), data));
				}
				else {
					nodeSetList = new List<Entity>(data.allNodeSets());
				}
				nodeSetList.Sort();
			}
			if (nodeList == null) {
				if (useFilter && activeFilter != null) {
					nodeList = new List<Entity>(activeFilter.matches(data.allNodes(), data));
				}
				else {
					nodeList = new List<Entity>(data.allNodes());
				}
				nodeList.Sort();
				List<Entity> allLinkNodeList = new List<Entity>(data.allNodes());
				editAddLinkNodeArray = new string[allLinkNodeList.Count];
				for (int i = 0; i < allLinkNodeList.Count; i++) {
					string id = allLinkNodeList[i].id;
					if (useNames && data.exists(allLinkNodeList[i].id, "name")) {
						id = data.data<string>(allLinkNodeList[i], "name");
					}
					editAddLinkNodeArray[i] = id;
				}
			}
			if (linkTypeList == null) {
				if (useFilter && activeFilter != null) {
					linkTypeList = new List<Entity>(activeFilter.matches(data.allLinkTypes(), data));
				}
				else {
					linkTypeList = new List<Entity>(data.allLinkTypes());
				}
				linkTypeList.Sort();
				List<Entity> allLinkTypeList = new List<Entity>(data.allLinkTypes());
				editAddLinkTypeArray = new string[allLinkTypeList.Count];
				for (int i = 0; i < allLinkTypeList.Count; i++) {
					editAddLinkTypeArray[i] = allLinkTypeList[i].id;
				}
			}
			if (linkSetList == null) {
				if (useFilter && activeFilter != null) {
					linkSetList = new List<Entity>(activeFilter.matches(data.allLinkSets(), data));
				}
				else {
					linkSetList = new List<Entity>(data.allLinkSets());
				}
				linkSetList.Sort();
			}
			if (linkList == null) {
				if (useFilter && activeFilter != null) {
					linkList = new List<Entity>(activeFilter.matches(data.allLinks(), data));
				}
				else {
					linkList = new List<Entity>(data.allLinks());
				}
				linkList.Sort();
			}
		}
	
		//------------------------------------------------------------
		public string[] editAddLinkTypeArray;
		public string[] editAddLinkNodeArray;
		public int editLinkType = 0; 
		public int editLinkParent = 0;
		public int editLinkChild = 0;

		public string editAddEnt = "";
		public EntType editAddEntType = EntType.node;

		private void displayAddEntity() {
			GUILayout.Label("Add Entity", GUILayout.MaxWidth(100));
			GUILayout.BeginHorizontal();
			if (editAddEntType != EntType.link) {
				editAddEnt = GUILayout.TextField(editAddEnt, GUILayout.Width(buttonWidth));
			}
			editAddEntType = (EntType) EditorGUILayout.Popup((int)editAddEntType, Enum.GetNames(typeof(EntType)), GUILayout.Width(buttonWidth));
			// Add Link
			if (editAddEntType == EntType.link) {
				GUILayout.EndHorizontal();
				GUILayout.Label("Type:Parent:Child", GUILayout.MaxWidth(150));
				GUILayout.BeginHorizontal();					
				editLinkType = EditorGUILayout.Popup(editLinkType, editAddLinkTypeArray, GUILayout.Width(buttonWidth));
				editLinkParent = EditorGUILayout.Popup(editLinkParent, editAddLinkNodeArray, GUILayout.Width(buttonWidth));
				editLinkChild = EditorGUILayout.Popup(editLinkChild, editAddLinkNodeArray, GUILayout.Width(buttonWidth));
				GUILayout.EndHorizontal();
				GUILayout.Space(10);
				GUILayout.BeginHorizontal();					
			}
			if(GUILayout.Button("Add Ent", GUILayout.Width(buttonWidth))) {
				addEnt(editAddEntType, editAddEnt, editLinkType, editLinkParent, editLinkChild);
				refresh();
			}
			GUILayout.EndHorizontal();
			GUILayout.Space(10);
		}

		//------------------------------------------------------------
		string filename = "";

		private void displayFileLoader() {
			GUILayout.Space(10);
			GUILayout.Label("Load/Save File", EditorStyles.boldLabel);
			GUILayout.BeginHorizontal();
			filename = GUILayout.TextField(filename, GUILayout.Width(buttonWidth));
			if (GUILayout.Button("Load", GUILayout.Width(buttonWidth))) {
				if (activeFilter != null && useFilterFile) {
					List<DataFilter> readFilters = new List<DataFilter>{activeFilter};
					DataLoader.readDataFile(filename, data, readFilters);
				}
				else {
					DataLoader.readDataFile(filename, data);
				}
				refresh();
			}
			if (GUILayout.Button("Save", GUILayout.Width(buttonWidth))) {
				if (activeFilter != null && useFilterFile) {
					List<DataFilter> writeFilters = new List<DataFilter>{activeFilter};
					DataLoader.writeDataFile(filename, data, writeFilters);
				}
				else {
					DataLoader.writeDataFile(filename, data);
				}
			}
			GUILayout.EndHorizontal();
			GUILayout.Space(10);
		
		}   

		//------------------------------------------------------------
		private List<DataFilter> dataFilters = new List<DataFilter>();
		private bool lastDisplayFilters = false;
		private bool displayFilters = false;
		private DataFilter activeFilter = null;
		private string newFilterId = "";
		private int activeItem = -1;
		private string filterAddEnt = "";
		private bool useFilter = false;
		private bool useFilterFile = false;
		private string filterFileName = "";
		
		private void displayDataFilters() {
			displayFilters = GUILayout.Toggle(displayFilters, "DataFilters", EditorStyles.boldLabel);

			// reset spacing on graphs
			if (lastDisplayFilters != displayFilters) {
				if (mode == DataWindowMode.graphs) {
					loadLinkData();
				}
				lastDisplayFilters = displayFilters;
			}

			// show filter section
			if (displayFilters) {
				useFilter = GUILayout.Toggle(useFilter, "Use Filter");
				useFilterFile = GUILayout.Toggle(useFilterFile, "Use Save/Load Filter");

				// Load/Save filters
				GUILayout.BeginHorizontal();
				filterFileName = GUILayout.TextField(filterFileName, GUILayout.Width(buttonWidth));
				if (GUILayout.Button("Load", GUILayout.Width(50))) {
					dataFilters = DataLoader.readFilterFile(filterFileName);
				}
				if (GUILayout.Button("Save", GUILayout.Width(50))) {
					DataLoader.writeFilterFile(dataFilters, filterFileName);
				}
				if (GUILayout.Button("Clear", GUILayout.Width(50))) {
					dataFilters.Clear();
				}
				GUILayout.EndHorizontal();
				GUILayout.Space(10);
				
				GUILayout.BeginHorizontal();
				int i = 0;
				DataFilter deleteDataFilter = null;
				foreach (DataFilter filter in dataFilters) {
					if (i != 0 && i % NUM_COLUMNS == 0) {
						GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal();
					}
					Color oldTextColor = buttonStyle.normal.textColor;
					buttonStyle.normal.textColor = Color.white;
					if (filter == activeFilter) {
						GUI.backgroundColor = Color.black;
						if (GUILayout.Button("X", buttonStyle, GUILayout.Width(xButtonWidth))) {
							deleteDataFilter = filter;
						}
						if (GUILayout.Button(filter.id, buttonStyle, GUILayout.Width(buttonWidth))) {
							activeFilter = null;
						}
					}
					else {
						GUI.backgroundColor = Color.gray;
						if (GUILayout.Button("X", buttonStyle, GUILayout.Width(xButtonWidth))) {
							deleteDataFilter = filter;
						}
						if (GUILayout.Button(filter.id, buttonStyle, GUILayout.Width(buttonWidth))) {
							activeFilter = filter;
						}
					}
					buttonStyle.normal.textColor = oldTextColor;
					resetColor();
					i++;
				}
				// delete DataFilter
				if (deleteDataFilter != null) {
					dataFilters.Remove(deleteDataFilter);
					if (activeFilter == deleteDataFilter) {
						activeFilter = null;
						refresh();
					}
				}

				GUILayout.EndHorizontal();
				GUILayout.Space(10);

				// Display Filter Settings
				if (activeFilter != null) {
					GUILayout.Label(activeFilter.id);
					int deleteFilter = -1;
					for (int j = 0; j < activeFilter.filters.Count; j++) {
						GUILayout.BeginHorizontal();
						DataFilterItem filter = activeFilter.filters[j];
						// active row change to yellow
						if (activeItem == j) {
							GUI.backgroundColor = Color.yellow;
						}
						if (edit) {
							if (GUILayout.Button("X", GUILayout.Width(xButtonWidth))) {
								deleteFilter = j;
								break;
							}
							
							filter.type = (FilterType) EditorGUILayout.Popup((int) filter.type, Enum.GetNames(typeof(FilterType)), GUILayout.Width(buttonWidth));
							filter.mode = (FilterMode) EditorGUILayout.Popup((int) filter.mode, Enum.GetNames(typeof(FilterMode)), GUILayout.Width(buttonWidth));
						}
						else {
							GUILayout.Label(filter.type.ToString(), GUILayout.Width(buttonWidth));
							GUILayout.Label(filter.mode.ToString(), GUILayout.Width(buttonWidth));
						}
						if (GUILayout.Button("Ents", GUILayout.Width(50))) {
							if (activeItem == j) {
								activeItem = -1;
							}
							else {
								activeItem = j;
							}
						}
						// Move up and Down
						if (GUILayout.Button("Up", GUILayout.Width(50))) {
							if (j != 0) {
								activeFilter.filters.Remove(filter);
								activeFilter.filters.Insert(j-1, filter);
							}
							if (activeItem > 0) {
								activeItem--;
							}
							break;
						}
						if (GUILayout.Button("Down", GUILayout.Width(50))) {
							if (j != (activeFilter.filters.Count-1)) {
								activeFilter.filters.Remove(filter);
								activeFilter.filters.Insert(j+1, filter);
							}
							if (activeItem < activeFilter.filters.Count-1) {
								activeItem++;
							}

							break;
						}

						GUILayout.EndHorizontal();
						resetColor();
					} // end show item rows
					if (deleteFilter != -1) {
						activeFilter.filters.RemoveAt(deleteFilter);
						deleteFilter = -1;
					}

					// Add new filter to DataFilter
					if (edit) {
						if (GUILayout.Button("Add Filter", GUILayout.Width(100))) {
							activeFilter.filters.Add(new DataFilterItem());
						}
					}
					GUILayout.Space(10);

					// Show ent list for filter
					if (activeItem != -1 && activeFilter != null) {
						if (activeFilter.filters.Count > activeItem) {
							HashSet<string> filterEnts = activeFilter.filters[activeItem].ents;
							GUILayout.Label("Filter Ents", EditorStyles.boldLabel);
							GUILayout.BeginHorizontal();	
							int k = 0;
							string deleteEnt = null;
							foreach (string fEntS in filterEnts) {
								if (k != 0 && k % NUM_COLUMNS == 0) {
									GUILayout.EndHorizontal();
									GUILayout.BeginHorizontal();
								}
								k++;

								// show gray button
								if (!data.exists(fEntS)) {
									resetColor();
									if (edit) {
										if (GUILayout.Button("X", GUILayout.Width(xButtonWidth))) {
											deleteEnt = fEntS;
										}
									}
									if (GUILayout.Button(fEntS, GUILayout.Width(buttonWidth))) {
										// Do nothing
									}
									continue;
								}
								Entity fEnt = data[fEntS];
								if (edit) {
									if (xButton(fEnt)) {
										deleteEnt = fEnt.id;
									}
								}
								entButton(fEnt);
							} // end foreach
							GUILayout.EndHorizontal();

							// delete ent if clicked X
							if (deleteEnt != null) {
								filterEnts.Remove(deleteEnt);
								deleteEnt = null;
							}
							// Add new ent
							if (edit) {
								GUILayout.BeginHorizontal();
								filterAddEnt = GUILayout.TextField(filterAddEnt, GUILayout.Width(buttonWidth));
								if (GUILayout.Button("Add Ent", GUILayout.Width(buttonWidth))) {
									filterEnts.Add(filterAddEnt);
								}
								GUILayout.EndHorizontal();
							}

						}
						GUILayout.Space(10);
					}
					else {
						activeItem = -1;
					}
					
				} // end show active filter

				// Add DataFilter
				GUILayout.BeginHorizontal();
				newFilterId = GUILayout.TextField(newFilterId, GUILayout.Width(buttonWidth));
				if (GUILayout.Button("Add DataFilter", GUILayout.Width(buttonWidth))) {
					DataFilter newFilter = new DataFilter(newFilterId);
					dataFilters.Add(newFilter);
				}
				GUILayout.EndHorizontal();
				GUILayout.Space(10);
			}
			GUILayout.Space(10);
		}

		//------------------------------------------------------------
		private void displayRefreshReset() {
			GUILayout.Space(10);
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Refresh", GUILayout.Width(buttonWidth))) {
				refresh();
			}
			if (GUILayout.Button("Reset", GUILayout.Width(buttonWidth))) {
				resetData();
			}
			if (GUILayout.Button("Clear", GUILayout.Width(buttonWidth))) {
				clearData();
			}
			GUILayout.EndHorizontal();
		}

		//------------------------------------------------------------
		public enum DataWindowMode {
			entities, fields, graphs, entity
		};
		public DataWindowMode mode = DataWindowMode.entities;

		private void displayModes() {

			// Choose Display mode
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("entities", GUILayout.Width(buttonWidth))) {
				mode = DataWindowMode.entities;
			}
			if (GUILayout.Button("fields", GUILayout.Width(buttonWidth))) {
				mode = DataWindowMode.fields;
			}
			if (GUILayout.Button("graphs", GUILayout.Width(buttonWidth))) {
				mode = DataWindowMode.graphs;
			}
			GUILayout.EndHorizontal();		
			GUILayout.Space(10);

			// display different modes
			if (mode == DataWindowMode.entities) {
				displayEntitiesWindow();
			}
			else if (mode == DataWindowMode.entity) {
				displayEntityWindow();
			}
			else if (mode == DataWindowMode.fields) {
				displayFieldsWindow();
			}
			else if (mode == DataWindowMode.graphs) {
				displayGraphs();
			}
		}
	
		//------------------------------------------------------------
		public bool displayNodeTypes = true;
		public List<Entity> nodeTypeList;

		private void displayEntitiesNodeTypes() {
			setEntColor(EntType.nodeType);
			displayNodeTypes = GUILayout.Toggle(displayNodeTypes, "NodeTypes");
			if (displayNodeTypes) {
				if (nodeTypeList != null) {
					int i = 0;
					GUILayout.BeginHorizontal();
					foreach (Entity nodeType in nodeTypeList) {

						if (edit) {
							if (xButton(nodeType)) {
								data.rm(nodeType);
								refresh();
								return;
							}
						}
						if (entButton(nodeType)) {
							selectEnt(nodeType);
							return;
						}
						i++;
						if (i % NUM_COLUMNS == 0) {
							GUILayout.EndHorizontal();
							GUILayout.BeginHorizontal();
						}

					}
					GUILayout.EndHorizontal();
				}
			}
			resetColor();
		}

		//------------------------------------------------------------
		public bool displayNodeSets = true;
		public List<Entity> nodeSetList;

		private void displayEntitiesNodeSets() {
			setEntColor(EntType.nodeSet);
			displayNodeSets = GUILayout.Toggle(displayNodeSets, "NodeSets");
			if (displayNodeSets) {
				if (nodeSetList != null) {
					GUILayout.BeginHorizontal();
					int i = 0;
					foreach (Entity nodeSet in nodeSetList) {
						if (edit) {
							if (xButton(nodeSet)) {
								data.rm(nodeSet);
								refresh();
								return;
							}
						}
						if (entButton(nodeSet)) {
							selectEnt(nodeSet);
							return;
						}
						i++;
						if (i % NUM_COLUMNS == 0) {
							GUILayout.EndHorizontal();
							GUILayout.BeginHorizontal();
						}
					}
					GUILayout.EndHorizontal();
				}
			} 
			resetColor();
		}

		//------------------------------------------------------------
		public bool displayNodes = true;
		public List<Entity> nodeList;

		private void displayEntitiesNodes() {
			setEntColor(EntType.node);
			displayNodes = GUILayout.Toggle(displayNodes, "Nodes");
			if (displayNodes) {
				if (nodeList != null) {
					int i = 0;
					GUILayout.BeginHorizontal();
					foreach (Entity node in nodeList) {
						if (edit) {
							if (xButton(node)) {
								data.rm(node);
								refresh();
								return;
							}
						}
						if (entButton(node)) {
							selectEnt(node);
							return;
						}
						i++;
						if (i % NUM_COLUMNS == 0) {
							GUILayout.EndHorizontal();
							GUILayout.BeginHorizontal();
						}
					}
					GUILayout.EndHorizontal();
				}
			} 
			resetColor();
		}

		//------------------------------------------------------------
		public bool displayLinkTypes = true;
		public List<Entity> linkTypeList;

		private void displayEntitiesLinkTypes() {
			setEntColor(EntType.linkType);
			displayLinkTypes = GUILayout.Toggle(displayLinkTypes, "LinkTypes");
			if (displayLinkTypes) {
				if (linkTypeList != null) {	
					foreach (Entity linkType in linkTypeList) {
						GUILayout.BeginHorizontal();
						if (edit) {
							if (xButton(linkType)) {
								data.rm(linkType);
								refresh();
								return;
							}
						}
						if (entButton(linkType)) {
							selectEnt(linkType);
							return;
						}
						GUILayout.EndHorizontal();
					}
				}
			}
			resetColor();
		}
	
		//------------------------------------------------------------
		public bool displayLinkSets = true;
		public List<Entity> linkSetList;

		private void displayEntitiesLinkSets() {
			setEntColor(EntType.linkSet);
			displayLinkSets = GUILayout.Toggle(displayLinkSets, "LinkSets");
			if (displayLinkSets) {
				if (linkSetList != null) {
					foreach (Entity linkSet in linkSetList) {
						GUILayout.BeginHorizontal();
						if (edit) {
							if (xButton(linkSet)) {
								data.rm(linkSet);
								refresh();
								return;
							}
						}
						if (entButton(linkSet)) {
							selectEnt(linkSet);
							return;
						}
						GUILayout.EndHorizontal();
					}
				}
			} 
			resetColor();
		}

		//------------------------------------------------------------
		public bool displayLinks = true;
		public List<Entity> linkList;

		private void displayEntitiesLinks() {
			setEntColor(EntType.link);
			displayLinks = GUILayout.Toggle(displayLinks, "Links");
			if (displayLinks) {
				if (linkList != null) {
					foreach (Entity link in linkList) {
						GUILayout.BeginHorizontal();
						if (edit) {
							if (xButton(link)) {
								data.rm(link);
								refresh();
								return;
							}
						}
						if (entButton(link)) {
							selectEnt(link);
							return;
						}
						GUILayout.EndHorizontal();
					}
				}
			} 
			resetColor();
		}

		//------------------------------------------------------------
		private void displayEntityHeader() {
			GUILayout.Label(selEnt.type.ToString() + " : " + selEnt.id, EditorStyles.boldLabel);
			setEntColor(selEnt.type);
			GUILayout.BeginHorizontal();
			if (edit) {
				if (xButton(selEnt)) {
					data.rm(selEnt);
					selEnt = null;
					refresh();
					mode = DataWindowMode.entities;
					return;
				}
			}
			if (entButton(selEnt)) {
				// already selected
			}
			resetColor();
			GUILayout.EndHorizontal();
			GUILayout.Space(10);
		}

		//------------------------------------------------------------
		public List<Entity> groupEnts = null;
		public List<Entity> parentTypes = null;
		public List<Entity> parentSets = null;
		public List<Entity> childTypes = null;
		public List<Entity> childSets = null;
		public bool displayParentGroup = true;
		public string addToGroupEnt = "";

		private void displayEntityTypeSet() {
			if (groupEnts == null) {
				if (selEnt.type == EntType.nodeType || 
					selEnt.type == EntType.linkType) {
					if (data.exists(selEnt.id, "order")) {
						groupEnts = DataUtil.sort(data, data.typeEnts(selEnt), "order");
					}
					else {
						groupEnts = new List<Entity>(data.typeEnts(selEnt));
					}
				}
				else {
					groupEnts = new List<Entity>(data.setEnts(selEnt));
				}
			}

			// parentTypes and parentSets
			if (parentTypes == null) {
				parentTypes = new List<Entity>(data.entTypes(selEnt));
			}
			if (parentSets == null) {
				parentSets = new List<Entity>(data.entSets(selEnt));
			}
			// childTypes and childSets
			if (childTypes == null) {
				childTypes = new List<Entity>(data.childTypes(selEnt));
			}
			if (childSets == null) {
				childSets = new List<Entity>(data.childSets(selEnt));
			}

			// parentTypes
			if (parentTypes != null && parentTypes.Count > 0) {
				GUILayout.Label("Parent Types", EditorStyles.boldLabel);
				GUILayout.BeginHorizontal();
				int i = 0;
				foreach (Entity parentType in parentTypes) {
					if (edit) {
						if (xButton(parentType)) {
							data.rmEntFromType(selEnt, parentType);
							refresh();
							return;
						}
					}
					if (entButton(parentType)) {
						selectEnt(parentType);
						return;
					}
					i++;
					if (i % NUM_COLUMNS == 0) {
						GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal();					
					}
				}
				GUILayout.EndHorizontal();
			}

			// parentSets
			if (parentSets != null && parentSets.Count > 0) {
				GUILayout.Label("Parent Sets", EditorStyles.boldLabel);
				GUILayout.BeginHorizontal();
				int i = 0;
				foreach (Entity parentSet in parentSets) {
					if (edit) {
						if (xButton(parentSet)) {
							data.rmEntFromType(selEnt, parentSet);
							refresh();
							return;
						}
					}
					if (entButton(parentSet)) {
						selectEnt(parentSet);
						return;
					}
					i++;
					if (i % NUM_COLUMNS == 0) {
						GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal();					
					}
				}
				GUILayout.EndHorizontal();
			}

			// childTypes
			if (childTypes != null && childTypes.Count > 0) {
				GUILayout.Label("Child Types", EditorStyles.boldLabel);
				GUILayout.BeginHorizontal();
				int i = 0;
				foreach (Entity childType in childTypes) {
					if (edit) {
						if (xButton(childType)) {
							data.rmEntFromType(selEnt, childType);
							refresh();
							return;
						}
					}
					if (entButton(childType)) {
						selectEnt(childType);
						return;
					}
					i++;
					if (i % NUM_COLUMNS == 0) {
						GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal();					
					}
				}
				GUILayout.EndHorizontal();
			}

			// childSets
			if (childSets != null && childSets.Count > 0) {
				GUILayout.Label("Child Sets", EditorStyles.boldLabel);
				GUILayout.BeginHorizontal();
				int i = 0;
				foreach (Entity childSet in childSets) {
					if (edit) {
						if (xButton(childSet)) {
							data.rmEntFromType(selEnt, childSet);
							refresh();
							return;
						}
					}
					if (entButton(childSet)) {
						selectEnt(childSet);
						return;
					}
					i++;
					if (i % NUM_COLUMNS == 0) {
						GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal();					
					}
				}
				GUILayout.EndHorizontal();
			}
			
			if (groupEnts != null && groupEnts.Count > 0) {
				GUILayout.Label("Ents in group");
				GUILayout.BeginHorizontal();
				int i = 0;
				foreach (Entity ent in groupEnts) {
					if (edit && selEnt.type != EntType.linkType) {
						if (xButton(ent)) {
							if (selEnt.type == EntType.nodeType) {
								data.rmEntFromType(ent, selEnt);
							}
							else {
								data.rmEntFromSet(ent, selEnt);
							}
							refresh();
							return;
						}
					}
					if (entButton(ent)) {
						selectEnt(ent);
						return;
					}
					i++;
					if (i % NUM_COLUMNS == 0 
						|| selEnt.type == EntType.linkType 
						|| selEnt.type == EntType.linkSet) {
						GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal();					
					}
				}
				GUILayout.EndHorizontal();
			}

			resetColor();
			GUILayout.Space(10);
		
			// add ent to group
			if (edit && selEnt.type != EntType.linkType) {
			
				GUILayout.Label("Add to group");
				GUILayout.BeginHorizontal();
				int addWidth = buttonWidth;
				if (selEnt.type == EntType.linkSet) {
					addWidth *= 2;
				}
				addToGroupEnt = GUILayout.TextField(addToGroupEnt, GUILayout.Width(buttonWidth));			  
				if (GUILayout.Button("Add Ent", GUILayout.Width(100))) {
					if (selEnt.type == EntType.nodeType) {
						data.addEntToType(data[addToGroupEnt], selEnt);
					}
					else {
						data.addEntToSet(data[addToGroupEnt], selEnt);
					}
					refresh();
				}
				GUILayout.EndHorizontal();
			}
		}

		//------------------------------------------------------------
		private void displaySingleField(Field field) {
			GUILayout.BeginHorizontal();
			if (edit) {
				if (xButton(null)) {
					data.rm(field);
					return;
				}
			}
			GUILayout.Label(field.id, GUILayout.Width(buttonWidth));
			GUILayout.Label("single", GUILayout.Width(40));
			Object value = data.data<Object>(field);
			string dataType = DataUtil.dataTypeString(field.dType);
			string storedData = "";
			Color storedColor = Color.white;
			if (field.dType != DataType.colorType) {
				storedData = DataUtil.dataString(field.dType, value);
			}
			else {
				storedColor = (Color) value;
			}
			// allow field to be selected for editing longer text
			if (dataType == "string" && mode != DataWindowMode.fields) {
				if (selectedField == field) {
					GUI.backgroundColor = Color.yellow;
				}
				if (GUILayout.Button("string", GUILayout.Width(60))) {
					if (selectedField != field) {
						selectedField = field;
						editFieldText = data.data<string>(field);
					}
					else {
						selectedField = null;
						selectedFieldInt = -1;
						editFieldText = "";
					}
				}
				resetColor();
			}
			else {
				GUILayout.Label(dataType, GUILayout.Width(60));				
			}
			if (field.dType != DataType.colorType) {
				GUILayout.Label(storedData, GUILayout.Width(fieldWidth));			
			}
			else {
				EditorGUILayout.ColorField(storedColor, GUILayout.Width(buttonWidth)); 
			}

			// value
			if (edit) {
				string fValue = "";
				Color fColor = Color.white;
				if (field.dType != DataType.colorType) {
					if (fieldValues.ContainsKey(field)) {
						fieldValues.TryGetValue(field, out fValue);
					}
					else {
						fieldValues.Add(field, storedData);
						fValue = storedData;
					}
					fValue = GUILayout.TextField(fValue, GUILayout.Width(fieldWidth));
					fieldValues[field] = fValue;
				}
				else {
					if (fieldColors.ContainsKey(field)) {
						fieldColors.TryGetValue(field, out fColor);
					}
					else {
						fieldColors.Add(field, storedColor);
						fColor = storedColor;
					}
					fColor = EditorGUILayout.ColorField(fColor, GUILayout.Width(fieldWidth));
					fieldColors[field] = fColor;
				}				
			
				if (GUILayout.Button("Edit", GUILayout.Width(50))) {
					if (value is int) {
						data.edit<int>(field, DataUtil.toInt(fValue));
					}
					else if (value is float) {
						data.edit<float>(field, DataUtil.toFloat(fValue));
					}
					else if (value is Color) {
						data.edit<Color>(field, fColor);
					}
					else if (value is string) {
						data.edit<string>(field, fValue);
					}
					else if (value is Entity) {
						if (fValue == "") {
							data.edit<Entity>(field, null);
						}
						else {
							data.edit<Entity>(field, data[fValue]);
						}
					}
					else if (value is DataObject) {
						DataObject dataO = DataList.getNewDataObject(fValue);
						data.edit<DataObject>(field, dataO);
					}
					refresh();
				}
			}
			GUILayout.EndHorizontal();
		}

		//------------------------------------------------------------
		private void displayListField(Field field) {
			List<string> fValues = new List<string>();
			List<Color> fColors = new List<Color>();
			if (field.dType != DataType.colorType) {
				if (!fieldListValues.TryGetValue(field, out fValues)) {
					List<object> objValues = data.dataList<object>(field);
					fValues = new List<string>();
					foreach (object obj in objValues) {
						fValues.Add(DataUtil.dataString(field.dType, obj));
					}
					fieldListValues.Add(field, fValues);
				}
				else if (data.listCount(field) != fValues.Count) {	
					List<object> objValues = data.dataList<object>(field);
					fValues = new List<string>();
					foreach (object obj in objValues) {
						fValues.Add(DataUtil.dataString(field.dType, obj));
					}
					fieldListValues[field] = fValues;
				}
			}
			else {
				if (!fieldListColors.TryGetValue(field, out fColors)) {
					List<object> objColors = data.dataList<object>(field);
					fColors = new List<Color>();
					foreach (object obj in objColors) {
						fColors.Add((Color) obj);
					}
					fieldListColors.Add(field, fColors);
				}
				else if (data.listCount(field) != fColors.Count) {	
					List<object> objColors = data.dataList<object>(field);
					fColors = new List<Color>();
					foreach (object obj in objColors) {
						fColors.Add((Color) obj);
					}
					fieldListColors[field] = fColors;
				}
			}

			for (int i = 0; i < data.listCount(field); i++) {
				GUILayout.BeginHorizontal();
				if (edit) {
					if (xButton(null)) {
						if (data.listCount(field) == 1) {
							data.rm(field);
						}
						else {
							data.rm(field, i);
						}
						refresh();
						return;
					}
				}
				GUILayout.Label(field.id, GUILayout.Width(buttonWidth));
				GUILayout.Label("list", GUILayout.Width(40));
				Object value = data.data<Object>(field,i);
				string dataType = DataUtil.dataTypeString(field.dType);
				string storedData = "";
				Color storedColor = Color.white;
				if (field.dType == DataType.colorType) {
					storedColor = (Color) value;
				}
				else {
					storedData = DataUtil.dataString(field.dType, value);
				}

				// allow field to be selected for editing longer text
				if (dataType == "string" && mode != DataWindowMode.fields) {
					if (selectedField == field && selectedFieldInt == i) {
						GUI.backgroundColor = Color.yellow;
					}
					if (GUILayout.Button("string", GUILayout.Width(60))) {
						if (selectedField != field || selectedFieldInt != i) {
							selectedField = field;
							selectedFieldInt = i;
							editFieldText = data.data<string>(field,i);
						}
						else {
							selectedField = null;
							selectedFieldInt = -1;
							editFieldText = "";
						}
					}
					resetColor();
				}
				else {
					GUILayout.Label(dataType, GUILayout.Width(60));				
				}

				if (field.dType == DataType.colorType) {
					EditorGUILayout.ColorField(storedColor, GUILayout.Width(buttonWidth));
				}
				else {
					GUILayout.Label(storedData, GUILayout.Width(fieldWidth));
				}
				// value
				if (edit) {
					if (field.dType == DataType.colorType) {
						fColors[i] = EditorGUILayout.ColorField(fColors[i], GUILayout.Width(buttonWidth));				
					}
					else {
						fValues[i] = GUILayout.TextField(fValues[i], GUILayout.Width(fieldWidth));
					}
				
					if (GUILayout.Button("Edit", GUILayout.Width(50))) {
						if (value is int) {
							data.edit<int>(field, i, DataUtil.toInt(fValues[i]));
						}
						else if (value is float) {
							data.edit<float>(field, i, DataUtil.toFloat(fValues[i]));
						}
						else if (value is Color) {
							data.edit<Color>(field, i, fColors[i]);
						}
						else if (value is string) {
							data.edit<string>(field, i, fValues[i]);
						}
						else if (value is Entity) {
							data.edit<Entity>(field, i, data[fValues[i]]);
						}
						refresh();
					}
				}
				GUILayout.EndHorizontal();
			}

		}

		//------------------------------------------------------------
		private int editAddNodeToType = 0;
		private int editAddNodeToSet = 0;

		private Dictionary<Entity, List<Entity>> parentLinks = null;
		private Dictionary<Entity, List<Entity>> childLinks = null;
		private string rename = "";

		private void displayEntityNode(Entity ent) {
			// display rename
			GUILayout.BeginHorizontal();
			rename = GUILayout.TextField(rename, GUILayout.Width(buttonWidth));
			if (GUILayout.Button("Rename", GUILayout.Width(60))) {
				data.renameNode(selEnt, rename);
				selEnt = data[rename];
			}
			GUILayout.EndHorizontal();
			GUILayout.Space(10);
			
			// load data
			loadEntityTypeSetData();

			//--------------------
			// NodeTypes
			//--------------------
			GUILayout.Label("Node Types", EditorStyles.boldLabel);
			if (selNodeTypes != null) {
				foreach (Entity nodeType in selNodeTypes) {
					GUILayout.BeginHorizontal();
					if (edit) {
						if (xButton(nodeType)) {
							data.rmEntFromType(ent, nodeType);
							refresh();
							return;
						}
					}					
					if (entButton(nodeType)) {
						selectEnt(nodeType);
						return;
					}
					GUILayout.EndHorizontal();
				}
			}
			GUILayout.Space(10);

			// Add to NodeType
			if (edit && newNodeTypes != null && newNodeTypes.Length > 0) {
				GUILayout.Label("Add to NodeType");
				GUILayout.BeginHorizontal();
				editAddNodeToType = EditorGUILayout.Popup(editAddNodeToType, newNodeTypes, GUILayout.Width(buttonWidth));
				if (GUILayout.Button("Add to Type", GUILayout.Width(100))) {
					Entity nodeType = data[newNodeTypes[editAddNodeToType]];
					data.addEntToType(selEnt, nodeType);
					refresh();
				}
				GUILayout.EndHorizontal();
			}
			GUILayout.Space(10);


			//--------------------
			// NodeSets
			//--------------------
			GUILayout.Label("Node Sets", EditorStyles.boldLabel);
			if (selNodeSets != null) {
				foreach (Entity nodeSet in selNodeSets) {
					GUILayout.BeginHorizontal();
					if (edit) {
						if (xButton(nodeSet)) {
							data.rmEntFromSet(ent, nodeSet);
							refresh();
							return;
						}
					}					
					if (entButton(nodeSet)) {
						selectEnt(nodeSet);
						return;
					}
					GUILayout.EndHorizontal();
				}
			}
			// Add to NodeSet
			if (edit && newNodeSets != null && newNodeSets.Length > 0) {
				GUILayout.Label("Add to NodeSet");
				GUILayout.BeginHorizontal();
				editAddNodeToSet = EditorGUILayout.Popup(editAddNodeToSet, newNodeSets, GUILayout.Width(buttonWidth));
				if (GUILayout.Button("Add to Set", GUILayout.Width(100))) {
					Entity nodeSet = data[newNodeSets[editAddNodeToSet]];
					data.addEntToSet(selEnt, nodeSet);
					refresh();
				}
				GUILayout.EndHorizontal();
			}
			GUILayout.Space(10);

			//--------------------
			// Links
			//--------------------
			
			// load linkTypes
			if (parentLinks == null ||
				childLinks == null) {
				parentLinks = new Dictionary<Entity, List<Entity>>();
				childLinks = new Dictionary<Entity, List<Entity>>();
				foreach (Entity linkType in data.allLinkTypes()) {
					HashSet<Entity> pLinks = data.parents(selEnt, linkType);
					if (pLinks != null && pLinks.Count > 0) {
						List<Entity> pList = new List<Entity>(pLinks);
						pList.Sort();
						parentLinks.Add(linkType, pList);
					}
					HashSet<Entity> cLinks = data.children(selEnt, linkType);
					if (cLinks != null && cLinks.Count > 0) {
						List<Entity> cList;
						if (data.exists(linkType.id, "order")) {
							cList = DataUtil.sortedChildren(data, linkType, ent, "order");
						}
						else {
							cList = new List<Entity>(cLinks);
							cList.Sort();
						}
						cList.Sort();
						childLinks.Add(linkType, cList);
					}
				}
			}
			
			// Display parent links
			GUILayout.Label("Parent Links", EditorStyles.boldLabel);
			if (parentLinks != null) {
				foreach (Entity linkType in parentLinks.Keys) {
					GUILayout.BeginHorizontal();
					if (entButton(linkType)) {
						selectEnt(linkType);
						return;
					}
					List<Entity> parents;
					if (parentLinks.TryGetValue(linkType, out parents)) {
						foreach (Entity parent in parents) {
							Entity link = data.link(linkType, parent, selEnt);
							if (linkButton(link)) {
								selectEnt(link);
								return;
							}
							if (entButton(parent)) {
								selectEnt(parent);
								return;
							}
						}
						GUILayout.EndHorizontal();
					}
				}
			}
			GUILayout.Space(10);
			
			// display child links
			GUILayout.Label("Child Links", EditorStyles.boldLabel);
			if (childLinks != null) {
				foreach (Entity linkType in childLinks.Keys) {
					if (entButton(linkType)) {
						selectEnt(linkType);
						return;
					}
					GUILayout.BeginHorizontal();
					List<Entity> children;
					int linkIndex = 0;
					if (childLinks.TryGetValue(linkType, out children)) {
						foreach (Entity child in children) {
							if (linkIndex != 0 && linkIndex % NUM_LINK_COL == 0) {
								GUILayout.EndHorizontal();
								GUILayout.BeginHorizontal();
							}
							Entity link = data.link(linkType, selEnt, child);
							if (linkButton(link)) {
								selectEnt(link);
								return;
							}
							if (entButton(child)) {
								selectEnt(child);
								return;
							}
							linkIndex++;
						}
					}
					GUILayout.EndHorizontal();
				}
			}
			GUILayout.Space(10);
			
			// Add Link
			if (edit) {
				GUILayout.Label("Type:Parent:Child", GUILayout.MaxWidth(150));
				GUILayout.BeginHorizontal();					
				editLinkType = EditorGUILayout.Popup(editLinkType, editAddLinkTypeArray, GUILayout.Width(buttonWidth));
				for (int i = 0; i < editAddLinkNodeArray.Length; i++) {
					if (editAddLinkNodeArray[i] == ent.id) {
						editLinkParent = i;
						break;
					}
				}
				editLinkChild = EditorGUILayout.Popup(editLinkChild, editAddLinkNodeArray, GUILayout.Width(buttonWidth));
				GUILayout.EndHorizontal();
				GUILayout.Space(10);
				GUILayout.BeginHorizontal();					
				if(GUILayout.Button("Add Link", GUILayout.Width(buttonWidth))) {
					addEnt(EntType.link, "", editLinkType, editLinkParent, editLinkChild);
					refresh();
				}
				GUILayout.EndHorizontal();
				GUILayout.Space(10);
			}
		}
	
		//------------------------------------------------------------
		private int editAddLinkToSet = 0;

		private void displayEntityLink() {
			// load data
			loadEntityTypeSetData();

			// display linkType, parent, child
			GUILayout.BeginHorizontal();
			GUILayout.Label("LinkType", GUILayout.Width(buttonWidth));
			GUILayout.Label("Parent", GUILayout.Width(buttonWidth));
			GUILayout.Label("Child", GUILayout.Width(buttonWidth));
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			Entity linkType = data.linkType(selEnt);
			if (entButton(linkType)) {
				selectEnt(linkType);
				return;
			}
			Entity parent = data.linkParent(selEnt);
			if (entButton(parent)) {
				selectEnt(parent);
				return;
			}
			Entity child = data.linkChild(selEnt);
			if (entButton(child)) {
				selectEnt(child);
				return;
			}
			GUILayout.EndHorizontal();
			GUILayout.Space(10);

			//--------------------
			// LinkSets
			//--------------------
			GUILayout.Label("Link Sets", EditorStyles.boldLabel);
			if (selLinkSets != null) {
				foreach (Entity linkSet in selLinkSets) {
					GUILayout.BeginHorizontal();
					if (edit) {
						if (xButton(linkSet)) {
							data.rmEntFromSet(selEnt, linkSet);
							refresh();
							return;
						}
					}					
					if (entButton(linkSet)) {
						selectEnt(linkSet);
						return;
					}
					GUILayout.EndHorizontal();
				}
			}
			// Add to LinkSet
			if (edit && newLinkSets != null && newLinkSets.Length > 0) {
				GUILayout.Label("Add to LinkSet");
				GUILayout.BeginHorizontal();
				editAddLinkToSet = EditorGUILayout.Popup(editAddLinkToSet, newLinkSets, GUILayout.Width(buttonWidth));
				if (GUILayout.Button("Add to Set", GUILayout.Width(100))) {
					Entity linkSet = data[newLinkSets[editAddLinkToSet]];
					data.addEntToSet(selEnt, linkSet);
					refresh();
				}
				GUILayout.EndHorizontal();
			}
			GUILayout.Space(10);

		}

		//------------------------------------------------------------
		private List<Entity> selNodeSets = null;
		private List<Entity> selLinkSets = null;
		private List<Entity> selNodeTypes = null;
		private string[] newNodeTypes = null;
		private string[] newNodeSets = null;
		private string[] newLinkSets = null;

		private void loadEntityTypeSetData() {
			// nodeTypes
			if (selEnt.type == EntType.node || selEnt.type == EntType.nodeSet) {
				if (selNodeTypes == null) {
					selNodeTypes = new List<Entity>(data.entTypes(selEnt));
				}
				if (newNodeTypes == null) {
					List<Entity> newTypes = new List<Entity>(data.allNodeTypes());
					foreach(Entity rmEnt in selNodeTypes) {
						newTypes.Remove(rmEnt);
					}
					newNodeTypes = new string[newTypes.Count];
					for (int i = 0; i < newTypes.Count; i++) {
						newNodeTypes[i] = newTypes[i].id;
					}
				}
			}
			// nodeSets
			if (selEnt.type == EntType.node) {
				if (selNodeSets == null) {
					selNodeSets = new List<Entity>(data.entSets(selEnt));
				}
				if (newNodeSets == null) {
					List<Entity> newSets = new List<Entity>(data.allNodeSets());
					foreach(Entity rmEnt in selNodeSets) {
						newSets.Remove(rmEnt);
					}
					newNodeSets = new string[newSets.Count];
					for (int i = 0; i < newSets.Count; i++) {
						newNodeSets[i] = newSets[i].id;
					}
				}
			}
			// linkSets
			if (selEnt.type == EntType.link) {
				if (selLinkSets == null) {
					selLinkSets = new List<Entity>(data.entSets(selEnt));
				}
				if (newLinkSets == null) {
					List<Entity> newSets = new List<Entity>(data.allLinkSets());
					foreach(Entity rmEnt in selLinkSets) {
						newSets.Remove(rmEnt);
					}
					newLinkSets = new string[newSets.Count];
					for (int i = 0; i < newSets.Count; i++) {
						newLinkSets[i] = newSets[i].id;
					}
				}
			}
		}

		//------------------------------------------------------------
		private bool linkButton(Entity link) {
			setEntColor(EntType.link);
			string linkText = " ";
			if (data.exists(link.id, "num")) {
				linkText = data.data<int>(link, "num").ToString();
			}
			if (GUILayout.Button(linkText, GUILayout.Width(30))) {
				resetColor();
				return true;
			}
			else {
				resetColor();
				return false;
			}
		}	

		//------------------------------------------------------------
		public Entity linkType = null;
		public List<Entity> linkTypes = null;
		public List<LinkWindow> linkWindows = null;
		public bool splitLinkNodes = false;
		public bool lastSplitLinkNodes = false;
		public int maxXSize = 400;
		public int maxYSize = 600;

		public int xMargin = 10;
		public int yMargin = 10;
		public int winWidth = 140;
		public int winHeight = 30;
		public int winXSpacing = 40;
		public int winYSpacing = 20;
		public int winYStart = 260;

		public Entity currentGraphType;

		private void displayGraphs() {
			if (data != null) {

				if (linkWindows == null) {
					maxXSize = 400;
					maxYSize = 600;
				}

				GUILayout.Space(10);
				splitLinkNodes = GUILayout.Toggle(splitLinkNodes, "Split Multiple Links");
				if (lastSplitLinkNodes != splitLinkNodes) {
					lastSplitLinkNodes = splitLinkNodes;
					refresh();
				}      
				GUILayout.Space(10);

				var oldColor = GUI.backgroundColor;
				GUI.backgroundColor = Color.red;

				if (linkTypes == null) {
					linkTypes = new List<Entity>(data.allLinkTypes());
					linkTypes.Sort();
				}
      
				// display link types
				foreach (Entity key in linkTypes) {
					GUILayout.BeginHorizontal();
					if (entButton(key)) {
						currentGraphType = key;
						loadLinkData();
					}
					GUILayout.EndHorizontal();
				}


				GUILayout.Space(20);
				if (linkType != null) {
					GUILayout.Label(linkType.id, EditorStyles.boldLabel);
				}        

				if (linkWindows != null) {

					Handles.BeginGUI();
					foreach (LinkWindow win in linkWindows) {
						foreach (LinkWindow childWin in win.children) {
							if (win.rect.center.x < childWin.rect.center.x) {
								Vector2 start = new Vector2(win.rect.center.x, win.rect.center.y);
								Vector2 end = new Vector2(childWin.rect.center.x, childWin.rect.center.y);
								Handles.DrawBezier(start, end, new Vector2(win.rect.xMax + winXSpacing,win.rect.center.y), 
												   new Vector2(childWin.rect.xMin - winXSpacing, childWin.rect.center.y)
												   ,Color.red,null,5f);
							}
							else if (win.rect.center.x == childWin.rect.center.x) {
								Vector2 start = new Vector2(win.rect.xMax, win.rect.center.y);
								Vector2 end = new Vector2(childWin.rect.center.x, childWin.rect.center.y);
								Handles.DrawBezier(start, end, 
												   new Vector2(win.rect.xMax + winXSpacing, win.rect.center.y), 
												   new Vector2(childWin.rect.xMax, 
															   (childWin.rect.center.y + win.rect.center.y)/2.0f)
												   ,Color.red,null,5f);
							}
							else {
								Vector2 start = new Vector2(win.rect.xMax, win.rect.center.y);
								Vector2 end = new Vector2(childWin.rect.center.x, childWin.rect.center.y);
								Handles.DrawBezier(start, end, 
												   new Vector2(win.rect.xMax + winXSpacing, 
															   (win.rect.center.y + childWin.rect.center.y)/2.0f), 
												   new Vector2((childWin.rect.center.x + win.rect.center.x)/2.0f, 
															   (childWin.rect.center.y + win.rect.center.y)/2.0f)
												   ,Color.red,null,5f);
							}
						}
					}
					Handles.EndGUI();

					
					foreach (LinkWindow win in linkWindows) {
						string id = win.ent.id;
						if (data.exists(win.ent.id, "name") && useNames) {
							id = data.data<string>(win.ent.id, "name");
						}
						
						GUI.backgroundColor = Color.green;
						if (GUI.Button(win.rect, id)) {
							selectEnt(win.ent);
						}
					}
        
				}

				GUI.backgroundColor = oldColor;        
			} 
		}

		//------------------------------------------------------------
		private void setScrollPos() {
			scrollPos = Vector2.zero;
			if (selEnt != null && selEnt.type == EntType.node 
				&& linkWindows != null) {
				foreach (LinkWindow win in linkWindows) {
					if (win.ent == selEnt) {
						scrollPos = new Vector2(win.rect.center.x - position.center.x, 
												win.rect.center.y - position.center.y);
					}
				}
			}
		}

		//------------------------------------------------------------
		private void loadLinkData() {
			Entity type = currentGraphType;
			if (type == null || data == null) {
				clearLinksData();
				return;
			}

			linkType = type;

			linkTypes = new List<Entity>(data.allLinkTypes());
			linkTypes.Sort();

			List<Entity> roots = new List<Entity>();
			roots.Sort();

			HashSet<Entity> allNodes;
			if (useFilter && activeFilter != null) {
				allNodes = activeFilter.matches(data.allNodes(), data);
			}
			else {
				allNodes = data.allNodes();
			}
			foreach (Entity node in allNodes) {
				HashSet<Entity> parents = data.parents(node, linkType);
				if (useFilter && activeFilter != null) {
					parents = activeFilter.matches(parents, data);
				}
				HashSet<Entity> children = data.children(node, linkType);
				if (useFilter && activeFilter != null) {
					children = activeFilter.matches(children, data);
				}
				if ((parents == null || parents.Count == 0) 
					&& (children != null && children.Count > 0)) {
					roots.Add(node);
				}
			}
    
			// check for nodes already LinkWindow already added
			HashSet<Entity> addedNodes = new HashSet<Entity>();
			HashSet<Entity> duplicateParents = new HashSet<Entity>();

			// create stacks
			Stack<LinkWindow> stackWindows = new Stack<LinkWindow>();
			Stack<int> stackLayers = new Stack<int>();
			foreach (Entity root in roots) {
				stackWindows.Push(new LinkWindow(root));
				stackLayers.Push(0);
			}

			linkWindows = new List<LinkWindow>();
			int modYStart = winYStart;
			if (displayFilters) {
				modYStart += 200;
			}
			int yPos = modYStart + 20*linkTypes.Count;
			if (edit) {
				yPos += 40;
			}
			int maxLayer = 0;
			int lastLayer = -1;

			while (stackWindows.Count > 0) {
				// pop
				LinkWindow newWin = stackWindows.Pop();
				int layer = stackLayers.Pop();
      
				// update xPos if adding on same layer
				if (lastLayer >= layer) {
					yPos += winHeight + winYSpacing;
				}
				lastLayer = layer;
				if (maxLayer < layer) {
					maxLayer = layer;
				}

				// create LinkWindow
				linkWindows.Add(newWin);
				newWin.rect = new Rect(layer*(winWidth + winXSpacing) + xMargin,
									   yPos + yMargin, winWidth, winHeight);
      
				// add children to stack
				HashSet<Entity> children = data.children(newWin.ent, linkType);
				if (useFilter && activeFilter != null && children != null) {
					children = activeFilter.matches(children, data);
				}
				if (children != null && children.Count > 0) {
					foreach (Entity node in children) {
						if (splitLinkNodes || !addedNodes.Contains(node)) {
							LinkWindow childWin = new LinkWindow(node);
							stackWindows.Push(childWin);
							stackLayers.Push(layer+1);
							newWin.children.Add(childWin);
							addedNodes.Add(node);
						}
						else {
							duplicateParents.Add(newWin.ent);
						}
					}
				}            
			}

			// resolve duplicates
			foreach (Entity parent in duplicateParents) {
				// find parent win
				LinkWindow parentWin = null;
				foreach (LinkWindow win in linkWindows) {
					if (win.ent == parent) {
						parentWin = win;
					}
				}
				foreach (Entity child in data.children(parent, type)) {
					bool found = false;
					foreach (LinkWindow childWin in parentWin.children) {
						if (childWin.ent == child) {
							found = true;
						}
					}
					if (!found) {
						foreach (LinkWindow win in linkWindows) {
							if (win.ent == child) {
								parentWin.children.Add(win);
							}
						}
					}
				}
			}

			maxXSize = maxLayer * (2*winWidth + winXSpacing) + 2*xMargin;
			maxYSize = yPos + winHeight + 2 * yMargin;
		}

		//------------------------------------------------------------
		public void clearLinksData() {
			linkWindows = null;
			linkTypes = null;
			linkType = null;
		}
	}

}
