using System;
using UnityEngine;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/*
  DataMask is used to store mask layers for data.  There can be up to 
  64 different mask layers used.  DataMasks are essentially filters and
  and for data to be visible at least one layer must match of the viewing
  filter and the data filter.
  
  The DataWindow Editor can edit the names of the different mask layers.
  For each piece of data that has a mask value, you can check if the data
  is on that layer or give it a mask filter that it can check against to 
  see if the data is masked given that filter.
  
  Internally, mask values are stored as binary flags in a single long
  to save space.  Mask values can be set manually set or by adding layers 
  by their string names

*/

public class DataMask {
	/// Returns filter that has all layers set to true
	public const long ALL = -1;

	/// Returns filter that has all layers set to false
	public const long NONE = 0;

	// Number of max layers
	public const int MAX_NUM_LAYERS = 64;

	// used to store all maskNames 
	private static string[] maskLayerNames = new string[MAX_NUM_LAYERS];

	public readonly long mask;

	//------------------------------------------------------------
	/// Default constructor that sets all flags to false
	public DataMask() {
		mask = 0;
	}

	//------------------------------------------------------------
	public DataMask(long mask) {
		this.mask = mask;
	}

	//------------------------------------------------------------
	public DataMask(string maskString) {
		if (maskString == null || maskString.Length == 0 
			|| maskString.Length > MAX_NUM_LAYERS) {
			Debug.LogError("Invalid mask: " + maskString);
			mask = 0;
			return;
		}
		long newMask = 0;
		long screen = 1 << (maskString.Length - 1);
		for (int i = 0; i < maskString.Length; i++) {
			if (maskString[i] == '1') {
				newMask = newMask | screen;
				screen = screen >> 1;
			}
			else if (maskString[i] == '0') {
				screen = screen >> 1;
			}
			else {
				Debug.LogError("Invalid mask: " + maskString);
			}
		}
		mask = newMask;
	}

	//------------------------------------------------------------
	/// Constructor with list of strings for layers
	public DataMask(List<string> layers) {
		if (layers == null || layers.Count == 0) {
			mask = 0;
		}
		long maskLong = 0;
		foreach (string name in layers) {
			bool found = false;
			int layer = 0;
			for (int i = 0; i < MAX_NUM_LAYERS; i++) {
				if (maskLayerNames[i] != null && maskLayerNames[i] == name) {
					found = true;
					layer = i;
				}
			}
			if (found) {
				long screen = (long) (1 << layer);
				maskLong = maskLong | screen;
			}
			else {
				Debug.LogError("Can't use invalid layer: " + name);
			}
		}
		mask = maskLong;
	}

	//------------------------------------------------------------
	/// Constructor with list of ints for layers.  int can't
	/// exceen MAX_NUM_LAYERS
	public DataMask(List<int> layers) {
		if (layers == null || layers.Count == 0) {
			mask = 0;
		}
		long maskLong = 0;
		foreach (int layer in layers) {
			if (layer < MAX_NUM_LAYERS && layer >= 0) {
				long screen = (long) (1 << layer);
				maskLong = maskLong | screen;
			}
			else {
				Debug.LogError("Can't use invalid layer: " + layer);
			}
		}
		mask = maskLong;
	}

	//------------------------------------------------------------
	/// Returns true if the data is on layerToCheck
	public bool isLayer(string layerToCheck) {
		long screen = 1;
		for (int i = 0; i < MAX_NUM_LAYERS; i++) {
			if (maskLayerNames[i] != null && maskLayerNames[i] == layerToCheck) {
				if ((screen & mask) != 0) {
					return true;
				}
				else {
					return false;
				}
			}
			screen = screen << 1;
		}
		Debug.LogError("Can't find DataMaskLayer: " + layerToCheck);
		return false;
	}

	//------------------------------------------------------------
	/// Set name of layer to newName
	public static void setLayerName(int index, string newName) {
		if (index >= 0 &&  index < MAX_NUM_LAYERS) {
			maskLayerNames[index] = newName;
		}
		else {
			Debug.LogError("index out of range: " + index);
		}
	}

	//------------------------------------------------------------
	/// Return string name of layer at index
	public static string getLayerName(int index) {
		if (index >= 0 && index < MAX_NUM_LAYERS) {
			return maskLayerNames[index];
		}
		else {
			Debug.LogError("index out of range: " + index);
			return null;
		}
	}
  
	//------------------------------------------------------------
	/// Returns true if at least one layer is both true on filter
	/// and on mask layers
	public bool visible(DataMask filter) {
		if ((filter.mask & mask) == 0) {
			return false;
		}
		else {
			return true;
		}
	}

	//------------------------------------------------------------
	/// Used to quickly change multiple flags at once to newValue using
	/// layerMask as bit flags to mark flags to change.
	public DataMask combine(DataMask combineMask) {
		return new DataMask(combineMask.mask | mask);
	}

	//------------------------------------------------------------
	/// Returns true if two DataMasks have same flags set
	public override bool Equals(System.Object obj) {
		if (obj == null) {
			return false;
		}

		DataMask m = obj as DataMask;
		if ((System.Object)m == null) {
			return false;
		}

		return (mask == m.mask);
	}

	//------------------------------------------------------------
	/// Returns true if two DataMasks have same flags set  
	public bool Equals(DataMask m) {
		if ((object)m == null) {
			return false;
		}
		return (mask == m.mask);
	}

	//------------------------------------------------------------
	public override int GetHashCode() {
		return mask.GetHashCode();
	}

	//------------------------------------------------------------
	public override string ToString() {
		if (mask == 0) {
			return "mask:0";
		}
		long copy = mask;
		StringBuilder sb = new StringBuilder();
		while (copy != 0) {
			if ((copy & 1L) == 1L) {
				sb.Append("1");
			}
			else {
				sb.Append("0");
			}
			copy = copy >> 1;
		}
		StringBuilder sb2 = new StringBuilder("mask:");
		for (int i = sb.Length - 1; i >= 0; i--) {
			sb2.Append(sb[i]);
		}
		return sb2.ToString();
	}

	//------------------------------------------------------------
	public string binary() {
		StringBuilder sb = new StringBuilder();
		
		long newMask = mask;
		if (newMask == 0) {
			return "0";
		}
		while (newMask != 0) {
			if (newMask % 2 == 1) {
				sb.Append("1");
			}
			else 
				sb.Append("0");
			newMask /= 2;
		}
		StringBuilder reverse = new StringBuilder();
		for (int i = sb.Length-1; i >= 0; i--) {
			reverse.Append(sb[i]);
		}
		return reverse.ToString();
	}		
}
