using System;
using System.Text;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public static class FileUtil {
	public static string DATA_FILES_LOCATION = Application.dataPath + "/DataFiles/";

	//------------------------------------------------------------
	public static bool isValidFile(string filename, string ext=".csv") {
		try {
			StreamReader sr = File.OpenText(DATA_FILES_LOCATION + filename + ext);
			string contents = sr.ReadToEnd();
			sr.Close();
			if( contents == null || contents == "") {
				return false;
			}
		}
		catch {
			return false;
		}
		return true;
	}

	//------------------------------------------------------------
	public static string readFile(string filename, string ext=".csv") {
		
		StreamReader sr = File.OpenText(DATA_FILES_LOCATION + filename + ext);
		string contents = sr.ReadToEnd();
		sr.Close();
		
		if( contents == null || contents == "") {
			Debug.LogError("Couldn't read filename: " + filename);
			return null;
		}

		return contents;
	}

	//------------------------------------------------------------
	public static void writeFile(string filename, string contents, string ext=".csv") {
		StreamWriter sw = File.CreateText(DATA_FILES_LOCATION + filename + ext);
		sw.WriteLine(contents);
		sw.Close();
	}

	//------------------------------------------------------------
	public static void deleteFile(string filename) {
		if(File.Exists(filename)) {
			File.Delete(filename);
		}		
	}
		
}
