using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Field : IComparable {
	public readonly Entity entity;
	public readonly string id;
	public readonly FieldType fType;
	public readonly DataType dType;

	//------------------------------------------------------------
	public Field(Entity entity, string id, FieldType fType, DataType dType) {
		this.entity = entity;
		this.id = id;
		this.fType = fType;
		this.dType = dType;
	}

	//------------------------------------------------------------
	public override bool Equals(System.Object obj) {
		if (obj == null) {
			return false;
		}
    
		Field k = obj as Field;
		if ((System.Object)k == null) {
			return false;
		}
    
		return (entity == k.entity && id == k.id);
	}

	//------------------------------------------------------------
	public bool Equals(Field k) {
		if ((object)k == null) {
			return false;
		}

		return (entity == k.entity && id == k.id);
	}

	//------------------------------------------------------------
	public override int GetHashCode() {
		return entity.GetHashCode() * 13 
			+ id.GetHashCode() * 7;
	}
	
	//------------------------------------------------------------
	public override string ToString() {
		return "field:" + entity.ToString() + ":" + id + ":" + fType + ":" + dType;
	}

	//------------------------------------------------------------
	public int CompareTo(System.Object other) { 
        if (other == null) {
			return 1;
		}

        Field f = other as Field;
        if (f != null) {
			if (entity != f.entity) {
				return entity.CompareTo(f.entity);
			}
			else {
				return id.CompareTo(f.id);
			}
		}
        else {
           throw new ArgumentException("Object is not a Field");
		}
	}

	//------------------------------------------------------------
	public static bool operator ==(Field lhs, Field rhs) {
		if (object.ReferenceEquals(lhs, null)) {
			return object.ReferenceEquals(rhs, null);
		}

		return lhs.Equals(rhs);
	}

	//------------------------------------------------------------
	public static bool operator !=(Field lhs, Field rhs) {
		if (object.ReferenceEquals(lhs, null)) {
			return !object.ReferenceEquals(rhs, null);
		}
		return !lhs.Equals(rhs);
	}

}
