using System;
using System.Text;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace DS {
	public static class DataLoader {

		public const int NUM_COLUMNS = 8;

		//------------------------------------------------------------
		public static List<DataFilter> readFilterFile(string filename) {
			List<DataFilter> returnFilters = new List<DataFilter>();
			string contents = FileUtil.readFile(filename, ".csv");

			string[] lines = DataUtil.splitIntoLines(contents);

			// loop through lines
			DataFilter filter = null;
			foreach (string line in lines) {
				string[] cells = DataUtil.split(line);

				if (!hasContent(cells)) {
					filter = null;
					continue;
				}

				if (cells[0] == "DATAFILTER") {
					filter = new DataFilter(cells[1]);
					returnFilters.Add(filter);
					continue;
				}
				
				if (filter != null) {
					loadFilterItem(cells, filter);
				}
				else {
					Debug.LogError("Corrupt Data on line:\n" + line);
				}
			}
			return returnFilters;
		}

		//------------------------------------------------------------
		public static void writeFilterFile(List<DataFilter> filters, string filename) {
			StringBuilder sb = new StringBuilder();

			foreach (DataFilter filter in filters) {
				sb.Append("DATAFILTER," + filter.id + "\n");
				foreach (DataFilterItem filterItem in filter.filters) {
					sb.Append(filterItem.type.ToString() + ",");
					sb.Append(filterItem.mode.ToString() + ",");
					
					// Show list of ents in DataFilterItem
					List<string> ents = new List<string>(filterItem.ents);
					for (int i = 0; i < ents.Count; i++) {
						sb.Append(ents[i] + ",");
					}
					sb.Append("\n");
				}
				sb.Append("\n");
			}
			
			FileUtil.writeFile(filename, sb.ToString(), ".csv");			
		}
		
		//------------------------------------------------------------
		public static void readDataFile(string filename, DataSet data,
										List<DataFilter> readFilters=null) {

			string contents = FileUtil.readFile(filename);
			readDataString(contents, data, readFilters);
			
		}

		//------------------------------------------------------------
		public static void readDataString(string dataString, DataSet data,
										  List<DataFilter> readFilters=null) {

			HashSet<Entity> added = new HashSet<Entity>();
			
			ReadingMode mode = ReadingMode.unknown;
			string[] lines = DataUtil.splitIntoLines(dataString);
			Entity setEnt = null;

			// loop through lines
			foreach (string line in lines) {
				string[] cells = DataUtil.split(line);

				// First check for content
				if (!hasContent(cells)) {
					mode = ReadingMode.unknown;
					continue;
				}

				// Check for mode marker
				if (mode == ReadingMode.unknown) {
					mode = setReadingMode(cells, ref setEnt, data, added);
				}

				// nodeTypes
				else if (mode == ReadingMode.nodeType) {
					loadNodeType(cells, data, added);
				}
				// linkTypes
				else if (mode == ReadingMode.linkType) {
					loadLinkType(cells, data, added);
				}
				// nodes
				else if (mode == ReadingMode.node) {
					loadNode(cells, data, added);
				}
				// links
				else if (mode == ReadingMode.link) {
					loadLink(cells, data, added);
				}
				// nodeSets
				else if (mode == ReadingMode.nodeSets) {
					loadNodeSets(cells, data, added);
				}
				// nodeSet
				else if (mode == ReadingMode.nodeSet) {
					loadNodeSet(cells, setEnt, data, added);
				}
				// linkSets
				else if (mode == ReadingMode.linkSet) {
					loadLinkSet(cells, setEnt, data, added);
				}
				// Fields
				else if (mode == ReadingMode.field) {
					loadField(cells, data);
				}

			} // end loop through lines

			if (readFilters != null) {
				// Apply filters - start with added 
				HashSet<Entity> matched = new HashSet<Entity>(added);
				foreach (DataFilter readFilter in readFilters) {
					matched = readFilter.matches(matched, data);
				}
				// At end, remove anything that isn't matched
				added.ExceptWith(matched);
				foreach (Entity rmEnt in added) {
					if (data.exists(rmEnt.id)) {
						data.rm(rmEnt);
					}
				}
			}
		}
	
		//------------------------------------------------------------
		public static string writeDataString(DataSet data,
											 List<DataFilter> writeFilters=null) {
			StringBuilder sb = new StringBuilder();

			HashSet<Entity> matched = data.allEnts();
			if (writeFilters != null) {
				// Apply filters
				foreach (DataFilter writeFilter in writeFilters) {
					matched = writeFilter.matches(matched, data);
				}
			}
			
			// nodeTypes
			writeNodeTypes(ref sb, data, matched);

			// linkTypes
			writeLinkTypes(ref sb, data, matched);

			// nodes
			writeNodes(ref sb, data, matched);

			// links
			writeLinks(ref sb, data, matched);

			// nodeSets
			writeNodeSets(ref sb, data, matched);
		
			// linkSets
			writeLinkSets(ref sb, data, matched);

			// fields
			writeFields(ref sb, data, matched);

			return sb.ToString();
		}

		//------------------------------------------------------------
		public static void writeDataFile(string filename, DataSet data,
										 List<DataFilter> writeFilters=null) {
			FileUtil.writeFile(filename, writeDataString(data, writeFilters));
		}
	
		//************************************************************
		// Private methods
		//************************************************************

		//------------------------------------------------------------
		private static Entity readLink(string[] cells, DataSet data, HashSet<Entity> added) {
			// linkType
			Entity linkType;
			if (!data.exists(cells[0])) {
				linkType = data.addLinkType(cells[0]);
				added.Add(linkType);
			}
			else {
				linkType = data[cells[0]];
			}

			
			// parent
			Entity parent; 
			if (!data.exists(cells[1])) {
				parent = data.addNode(cells[1]);
				added.Add(parent);
			}
			else {
				parent = data[cells[1]];
			}
		
			// child
			Entity child; 
			if (!data.exists(cells[2])) {
				child = data.addNode(cells[2]);
				added.Add(child);
			}
			else {
				child = data[cells[2]];
			}
		
			Entity link;
			if (!data.exists(cells[0] + ":" + cells[1]  
							 + ":" + cells[2])) {
				link = data.addLink(linkType, parent, child);
				added.Add(link);
			}
			else {
				link = data.link(linkType, parent, child);
			}

			return link;
		}

		//------------------------------------------------------------
		private static bool hasContent(string[] cells) {
			if (cells.Length == 0) {
				return false;
			}
			foreach (string cell in cells) {
				if (cell != "") {
					return true;
				}
			}
			return false;
		}

		//------------------------------------------------------------
		private static void loadNodeType(string[] cells, DataSet data, HashSet<Entity> added) {
			if (cells[0] != null && cells[0] != "") {
				Entity newNodeType = null;
				if (!data.exists(cells[0])) {
					newNodeType = data.addNodeType(cells[0]);
					added.Add(newNodeType);
				}
				else {
					newNodeType = data[cells[0]];
				}
				for (int i = 1; i < cells.Length; i++) {
					if (cells[i] != null && cells[i] != "") {
						if (!data.exists(cells[i])) {
							data.addNodeType(cells[i]);
						}
						data.addEntToType(newNodeType, data[cells[i]]);

					}
				}
			}
		}

		//------------------------------------------------------------
		private static void loadLinkType(string[] cells, DataSet data, HashSet<Entity> added) {
			if (cells[0] != null && cells[0] != "") {
				Entity newLinkType = null;
				if (!data.exists(cells[0])) {
					newLinkType = data.addLinkType(cells[0]);
					added.Add(newLinkType);
				}
				for (int i = 1; i < cells.Length; i++) {
					if (cells[i] != null && cells[i] != "") {
						if (!data.exists(cells[i])) {
							data.addLinkType(cells[i]);
						}
						data.addEntToType(newLinkType, data[cells[i]]);
					}
				}
			}
		}

		//------------------------------------------------------------
		private static void loadNode(string[] cells, DataSet data, HashSet<Entity> added) {
			string id = cells[0];
			Entity node;
			if (!data.exists(id)) {
				node = data.addNode(id);
				added.Add(node);
			}
			else {
				node = data[id];
			}
			// mask
			if (cells.Length > 1) {
				if (cells[1] != null && cells[1] != "") {
					data.setMask(node, new DataMask(cells[1]));
				}
				else {
					data.setMask(node, null);
				}
			}
			for (int i = 2; i < cells.Length; i++) {
				if (cells[i] != null && cells[i] != "") {
					Entity nodeType;
					if (!data.exists(cells[i])) {
						nodeType = data.addNodeType(cells[i]);
						added.Add(nodeType);
					}
					else {
						nodeType = data[cells[i]];
					}
					if (!data.isEntType(node, nodeType, true)) {
						data.addEntToType(node, nodeType);
					}
				}
			}
		}

		//------------------------------------------------------------
		private static void loadLink(string[] cells, DataSet data, HashSet<Entity> added) {
			if (cells.Length < 3) {
				Debug.LogError("Link not loaded: " + string.Join(",", cells));
			}
		
			Entity link = readLink(cells, data, added);
			if (link == null) {
				return;
			}
			if (cells.Length > 4) {
				if (cells[3] != null && cells[3] != "") {
					data.setMask(link, new DataMask(cells[3]));
				}
				else {
					data.setMask(link, null);
				}
			}
		}

		//------------------------------------------------------------
		private static void loadNodeSets(string[] cells, DataSet data, HashSet<Entity> added) {
			if (cells[0] == null || cells[0] == "") {
				Debug.LogError("loadNodeSets called with blank cells[0]");
				return;
			}

			Entity nodeSet;
			if (!data.exists(cells[0])) {
				nodeSet = data.addNodeSet(cells[0]);
				added.Add(nodeSet);
			}
			else {
				nodeSet = data[cells[0]];
			}
			
			// Add nodeset types
			for (int i = 1; i < cells.Length; i++) {
				if (cells[i] != null && cells[i] != "") {
					Entity nodeType;
					if (!data.exists(cells[i])) {
						nodeType = data.addNodeType(cells[i]);
					}
					else {
						nodeType = data[cells[i]];
					}
					
					data.addEntToType(nodeSet, nodeType);
				}
			}
		}

		//------------------------------------------------------------
		private static void loadNodeSet(string[] cells, Entity setEnt, 
										DataSet data, HashSet<Entity> added) {
			for (int i = 0; i < cells.Length; i++) {
				if (cells[i] != null && cells[i] != "") {
					Entity node;
					if (!data.exists(cells[i])) {
						node = data.addNode(cells[i]);
						added.Add(node);
					}
					else {
						node = data[cells[i]];
					}
					if (!data.isEntSet(node, setEnt, true)) {
						data.addEntToSet(node, setEnt);
					}
				}
			}
		}

		//------------------------------------------------------------
		private static void loadLinkSet(string[] cells, Entity setEnt, 
										DataSet data, HashSet<Entity> added) {
			if (cells.Length < 3) {
				Debug.LogError("Couldn't add link to linkSet: " + string.Join(",", cells));
			}
		
			Entity link = readLink(cells, data, added);
			if (!data.isEntSet(link, setEnt, true)) {
				data.addEntToSet(link, setEnt);
			}
		}

		//------------------------------------------------------------
		private static void loadField(string[] cells, DataSet data) {
			if (cells.Length < 5) {
				Debug.LogError("Couldn't add field: " + string.Join(",", cells));
				return;
			}
			if (!data.exists(cells[0])) {
				return;
			}

			// single
			if (cells[2] == "single") {
				loadSingleValue(cells, data);
			}
			// list
			else if (cells[2] == "list") {
				loadListValue(cells, data);
			}
		}

		//------------------------------------------------------------
		private static void loadFilterItem(string[] cells, DataFilter filter) {
			DataFilterItem newItem = new DataFilterItem();

			if (cells.Length < 2) {
				Debug.LogError("Couldn't add filter: " + string.Join(",", cells));
				return;
			}

			// type
			if (cells[0] == "include") {
				newItem.type = FilterType.include;
			}
			else {
				newItem.type = FilterType.exclude;
			}

			// mode
			if (cells[1] == "all") {
				newItem.mode = FilterMode.all;
			}
			else if (cells[1] == "allNodeTypes") {
				newItem.mode = FilterMode.allNodeTypes;
			}
			else if (cells[1] == "allNodeSets") {
				newItem.mode = FilterMode.allNodeSets;
			}
			else if (cells[1] == "allNodes") {
				newItem.mode = FilterMode.allNodes;
			}
			else if (cells[1] == "allLinkTypes") {
				newItem.mode = FilterMode.allLinkTypes;
			}
			else if (cells[1] == "allLinkSets") {
				newItem.mode = FilterMode.allLinkSets;
			}
			else if (cells[1] == "allLinks") {
				newItem.mode = FilterMode.allLinks;
			}
			else if (cells[1] == "ent") {
				newItem.mode = FilterMode.ent;
			}
			else if (cells[1] == "group") {
				newItem.mode = FilterMode.group;
			}

			// ents
			HashSet<string> ents = new HashSet<string>();
			for (int i = 2; i < cells.Length; i++) {
				if (cells[i] != null && cells[i] != "") {
					ents.Add(cells[i]);
				}
			}
			newItem.ents = ents;

			filter.filters.Add(newItem);
			
		}

		//------------------------------------------------------------
		private static void loadSingleValue(string[] cells, DataSet data) {
			Entity ent = data[cells[0]];

			switch (cells[3]) {
			case "int":
				if (!data.exists(cells[0], cells[1])) {
					data.addField<int>(ent, cells[1], DataUtil.toInt(cells[4]));
				}
				else {
					Field field = data.field(ent, cells[1]);
					data.edit<int>(field, DataUtil.toInt(cells[4]));
				}
				break;
			case "float":
				if (!data.exists(cells[0], cells[1])) {
					data.addField<float>(ent, cells[1], DataUtil.toFloat(cells[4]));
				}
				else {
					Field field = data.field(ent, cells[1]);
					data.edit<float>(field, DataUtil.toFloat(cells[4]));
				}
				break;
			case "color":
				if (!data.exists(cells[0], cells[1])) {
					data.addField<Color>(ent, cells[1], DataUtil.toColor(cells[4]));
				}
				else {
					Field field = data.field(ent, cells[1]);
					data.edit<Color>(field, DataUtil.toColor(cells[4]));
				}
				break;
			case "string":
				if (!data.exists(cells[0], cells[1])) {
					data.addField<string>(ent, cells[1], cells[4]);
				}
				else {
					Field field = data.field(ent, cells[1]);
					data.edit<string>(field, cells[4]);
				}
				break;
			case "entity":
				Entity valEnt;
				if (cells[4] == "") {
					valEnt = null;
				}
				else {
					valEnt = data[cells[4]];
				}
				if (!data.exists(cells[0], cells[1])) {
					data.addField<Entity>(ent, cells[1], valEnt);
				}
				else {
					Field field = data.field(ent, cells[1]);
					data.edit<Entity>(field, valEnt);
				}
				break;
			case "dataType":
				DataObject dataO = DataList.getNewDataObject(cells[4]);
				if (!data.exists(cells[0], cells[1])) {
					data.addField<DataObject>(ent, cells[1], dataO);
				}
				else {
					Field field = data.field(ent, cells[1]);
					data.edit<DataObject>(field, dataO);
				}
				break;
			}
		}

		//------------------------------------------------------------
		private static void loadListValue(string[] cells, DataSet data) {
			Entity ent = data[cells[0]];

			switch (cells[3]) {
			case "int":
				if (!data.exists(cells[0], cells[1])) {
					Field newField = data.addFieldList<int>(ent, cells[1]);
					if (cells[4] != "") {
						data.addToDataList(newField, DataUtil.toInt(cells[4]));
					}
				}
				else {
					Field listField = data.field(ent, cells[1]);
					// Don't add if already present
					if (cells.Length > 6 && cells[6] != null 
						&& cells[6] != "") {
						int index = DataUtil.toInt(cells[6]);
						if (data.listCount(listField) == index) {
							data.addToDataList<int>(listField, DataUtil.toInt(cells[4]));
						}
					}
				}
				break;
			case "float":
				if (!data.exists(cells[0], cells[1])) {
					Field newField = data.addFieldList<float>(ent, cells[1]);
					if (cells[4] != "") {
						data.addToDataList<float>(newField, DataUtil.toFloat(cells[4]));
					}
				}
				else {
					Field listField = data.field(ent, cells[1]);
					// Don't add if already present
					if (cells.Length > 6 && cells[6] != null 
						&& cells[6] != "") {
						int index = DataUtil.toInt(cells[6]);
						if (data.listCount(listField) == index) {
							data.addToDataList<float>(listField, DataUtil.toFloat(cells[4]));
						}
					}
				}
				break;
			case "color":
				if (!data.exists(cells[0], cells[1])) {
					Field newField = data.addFieldList<Color>(ent, cells[1]);
					if (cells[4] != "") {
						data.addToDataList<Color>(newField, DataUtil.toColor(cells[4]));
					}
				}
				else {
					Field listField = data.field(ent, cells[1]);
					// Don't add if already present
					if (cells.Length > 6 && cells[6] != null 
						&& cells[6] != "") {
						int index = DataUtil.toInt(cells[6]);
						if (data.listCount(listField) == index) {
							data.addToDataList<Color>(listField, DataUtil.toColor(cells[4]));
						}
					}
				}
				break;
			case "string":
				if (!data.exists(cells[0], cells[1])) {
					Field newField = data.addFieldList<string>(ent, cells[1]);
					if (cells[4] != "") {
						data.addToDataList<string>(newField, cells[4]);
					}
				}
				else {
					Field listField = data.field(ent, cells[1]);
					// Don't add if already present
					if (cells.Length > 6 && cells[6] != null 
						&& cells[6] != "") {
						int index = DataUtil.toInt(cells[6]);
						int listCount = data.listCount(listField);
						if (listCount == index) {
							data.addToDataList<string>(listField, cells[4]);
						}
					}
				}
				break;
			case "entity":
				if (!data.exists(cells[0], cells[1])) {
					Field newField = data.addFieldList<Entity>(ent, cells[1]);
					if (cells[4] != "") {
						data.addToDataList<Entity>(newField, data[cells[4]]);
					}
				}
				else {
					Field listField = data.field(ent, cells[1]);
					// Don't add if already present
					if (cells.Length > 6 && cells[6] != null 
						&& cells[6] != "") {
						int index = DataUtil.toInt(cells[6]);
						if (data.listCount(listField) == index) {
							data.addToDataList<Entity>(listField, data[cells[4]]);
						}
					}
				}
				break;
			case "dataType":
				if (!data.exists(cells[0], cells[1])) {
					Field newField = data.addFieldList<DataObject>(ent, cells[1]);
					if (cells[4] != "") {
						data.addToDataList<DataObject>(newField,
													   DataList.getNewDataObject(cells[4]));
					}
				}
				else {
					Field listField = data.field(ent, cells[1]);
					// Don't add if already present
					if (cells.Length > 6 && cells[6] != null 
						&& cells[6] != "") {
						int index = DataUtil.toInt(cells[6]);
						if (data.listCount(listField) == index) {
							DataObject dataO = DataList.getNewDataObject(cells[4]);
							data.addToDataList<DataObject>(listField, dataO);
						}
					}
				}
				break;
			}
		}

		//------------------------------------------------------------
		private static ReadingMode setReadingMode(string[] cells, 
												  ref Entity setEnt,
												  DataSet data,
												  HashSet<Entity> added) {
			ReadingMode mode = ReadingMode.unknown;
			if (cells[0] == "NODETYPES") {
				mode = ReadingMode.nodeType;
			}
			else if (cells[0] == "LINKTYPES") {
				mode = ReadingMode.linkType;
			}
			else if (cells[0] == "NODES") {
				mode = ReadingMode.node;
			}
			else if (cells[0] == "LINKS") {
				mode = ReadingMode.link;
			}
			else if (cells[0] == "FIELDS") {
				mode = ReadingMode.field;
			}
			else if (cells[0] == "NODESETS") {
				mode = ReadingMode.nodeSets;
			}
			else if (cells[0] == "NODESET") {
				mode = ReadingMode.nodeSet;
				if (!data.exists(cells[1])) {
					setEnt = data.addNodeSet(cells[1]);
					added.Add(setEnt);
				}
				else {
					setEnt = data[cells[1]];
				}
				if (cells.Length > 2 && cells[2] != null
					&& cells[2] != "") {
					data.setMask(setEnt, new DataMask(cells[2]));
				}
				else {
					data.setMask(setEnt, null);
				}
				if (cells.Length > 3) {
					for (int i = 3; i < cells.Length; i++) {
						if (cells[i] != "") {
							data.addEntToType(setEnt, data[cells[i]]);
						}
					}
				}
			}
			else if (cells[0] == "LINKSET") {
				mode = ReadingMode.linkSet;
				if (!data.exists(cells[1])) {
					setEnt = data.addLinkSet(cells[1]);
					added.Add(setEnt);
				}
				else {
					setEnt = data[cells[1]];
				}
				if (cells.Length > 2 && cells[2] != null
					&& cells[2] != "") {
					data.setMask(setEnt, new DataMask(cells[2]));
				}
				if (cells.Length > 3) {
					for (int i = 3; i < cells.Length; i++) {
						if (cells[i] != "") {
							data.addEntToType(setEnt, data[cells[i]]);
						}
					}
				}
				else {
					data.setMask(setEnt, null);
				}
			}

			return mode;
		}

		//------------------------------------------------------------
		private static void writeNodeTypes(ref StringBuilder sb, DataSet data,
										   HashSet<Entity> matched) {
			sb.Append("NODETYPES\n");
			List<Entity> allNodeTypesList = new List<Entity>(data.allNodeTypes());
			allNodeTypesList.Sort();
			foreach (Entity nodeType in allNodeTypesList) {
				bool include = false;
				if (matched.Contains(nodeType)) {
					include = true;
				}
				else {
					foreach (Entity ent in data.typeEnts(nodeType, true)) {
						if (matched.Contains(ent)) {
							include = true;
							break;
						}
					}
				}
				if (include) {
					sb.Append(nodeType.id + ",");
					foreach (Entity typeParent in data.entTypes(nodeType,true)) {
						sb.Append(typeParent.id + ",");
					}
					sb.Append("\n");
				}
			}
			sb.Append("\n");
		}

		//------------------------------------------------------------
		private static void writeLinkTypes(ref StringBuilder sb, DataSet data,
										   HashSet<Entity> matched) {
			sb.Append("LINKTYPES\n");
			List<Entity> allLinkTypesList = new List<Entity>(data.allLinkTypes());
			allLinkTypesList.Sort();
			foreach (Entity linkType in allLinkTypesList) {
				bool include = false;
				if (matched.Contains(linkType)) {
					include = true;
				}
				else {
					foreach (Entity ent in data.typeEnts(linkType, true)) {
						if (matched.Contains(ent)) {
							include = true;
							break;
						}
					}
				}
				if (include) {
					sb.Append(linkType.id + ",");
					foreach (Entity typeParent in data.entTypes(linkType,true)) {
						sb.Append(typeParent.id + ",");
					}
					sb.Append("\n");
				}
			}
			sb.Append("\n");
		}

		//------------------------------------------------------------
		private static void writeNodes(ref StringBuilder sb, DataSet data,
									   HashSet<Entity> matched) {
			sb.Append("NODES\n");
			List<Entity> allNodesList = new List<Entity>(data.allNodes());
			allNodesList.Sort();
			foreach (Entity node in allNodesList) {
				if (matched.Contains(node)) {
					sb.Append(node.id + ",");
					// mask
					if (data.mask(node,true) != null) {
						sb.Append(data.mask(node,true).binary());
					}
					foreach (Entity nodeType in data.entTypes(node,true)) {
						sb.Append("," + nodeType.id);
					}
					sb.Append("\n");
				}
			}
			sb.Append("\n");
		}

		//------------------------------------------------------------
		private static void writeLinks(ref StringBuilder sb, DataSet data,
									   HashSet<Entity> matched) {
			sb.Append("LINKS\n");
			List<Entity> allLinksList = new List<Entity>(data.allLinks());
			allLinksList.Sort();
			foreach (Entity link in allLinksList) {
				if (matched.Contains(link)) {
					sb.Append(data.linkType(link).id
							  + "," + data.linkParent(link).id 
							  + "," + data.linkChild(link).id);
					if (data.mask(link, true) != null) {
						sb.Append("," + data.mask(link).binary());
					}
					sb.Append("\n");
				}
			}
			sb.Append("\n");
		}

		//------------------------------------------------------------
		private static void writeNodeSets(ref StringBuilder sb, DataSet data,
										  HashSet<Entity> matched) {
			
			List<Entity> allNodeSetsList = new List<Entity>(data.allNodeSets());
			allNodeSetsList.Sort();


			// create all nodesets
			sb.Append("NODESETS\n");
			foreach (Entity nodeSet in allNodeSetsList) {
				bool include = false;
				if (matched.Contains(nodeSet))  {
					include = true;
				}
				else {
					foreach (Entity ent in data.setEnts(nodeSet, true)) {
						if (matched.Contains(ent)) {
							include = true;
							break;
						}
					}
				}
			  
				if (include) {
					sb.Append(nodeSet.id + ",");
					if (data.mask(nodeSet,true) != null) {
						sb.Append(data.mask(nodeSet,true).binary());
					}
					foreach (Entity nodeType in data.entTypes(nodeSet, true)) {
						sb.Append("," + nodeType.id);
					}
					sb.Append("\n");
				}
			} // end NODESETS
			sb.Append("\n");

			// NODESET
			foreach (Entity nodeSet in allNodeSetsList) {
				bool include = false;
				if (matched.Contains(nodeSet)) {
					include = true;
				}
				else {
					foreach (Entity ent in data.setEnts(nodeSet, true)) {
						if (matched.Contains(ent)) {
							include = true;
							break;
						}
					}
				}
				
				if (include) {
					sb.Append("NODESET," + nodeSet.id + "\n");
					
					// Show list of nodes in nodeSet
					List<Entity> nodes = new List<Entity>(data.setEnts(nodeSet,true));
					nodes.Sort();
					int j = 0;
					foreach (Entity childSet in data.childSets(nodeSet, true)) {
						if (matched.Contains(childSet)) {
							if (j != 0 && j % NUM_COLUMNS == 0) {
								sb.Append("\n");
							}
							sb.Append(childSet.id + ",");
							j++;
						}
					}
					foreach (Entity node in nodes) {
						if (matched.Contains(node)) {
							if (j != 0 && j % NUM_COLUMNS == 0) {
								sb.Append("\n");
							}
							sb.Append(node.id + ",");
							j++;
						}
					}
					sb.Append("\n\n");
				}
			}
		}

		//------------------------------------------------------------
		// TODO save linkSet hierarchy
		private static void writeLinkSets(ref StringBuilder sb, DataSet data,
										 HashSet<Entity> matched) {
			List<Entity> linkSetsList = new List<Entity>(data.allLinkSets());
			linkSetsList.Sort();
			foreach (Entity linkSet in linkSetsList) {
				if (matched.Contains(linkSet)) {
					sb.Append("LINKSET," + linkSet.id + ",");
					if (data.mask(linkSet,true) != null) {
						sb.Append(data.mask(linkSet,true).binary());
					}
					foreach (Entity linkType in data.entTypes(linkSet, true)) {
						sb.Append("," + linkType.id);
					}

					sb.Append("\n");
					
					// Show list of links in linkSet
					List<Entity> links = new List<Entity>(data.setEnts(linkSet,true));
					links.Sort();
					foreach (Entity link in links) {
						if (matched.Contains(link)) {
						
							sb.Append(data.linkType(link).id + ","
									  + data.linkParent(link).id + "," 
									  + data.linkChild(link).id);
							if (data.mask(link,true) != null) {
								sb.Append("," + data.mask(link,true).binary());
							}
							sb.Append("\n");
						}
					}
					sb.Append("\n");
				}
			}
		}

		//------------------------------------------------------------
		private static void writeFields(ref StringBuilder sb, DataSet data,
										HashSet<Entity> matched) {

			List<Field> fields = new List<Field>(data.allFields());
			fields.Sort();

			sb.Append("FIELDS\n");
			foreach (Field field in fields) {

				// skip if doesn't match filter
				if (!matched.Contains(field.entity)) {
					continue;
				}
				
				// single
				if (field.fType == FieldType.single) {
					sb.Append(field.entity.id + "," 
							  + field.id + "," 
							  + field.fType + ",");
					// dataType
					sb.Append(DataUtil.dataTypeString(field.dType) + ",");
					// data value
					object val = data.data<object>(field);
					sb.Append(DataUtil.escapeString(DataUtil.dataString(field.dType,
																		val)) + ",");
					// mask
					if (data.mask(field,true) != null) {
						sb.Append(data.mask(field,true).binary());
					}
					sb.Append("\n");
				}
				// list
				else if (field.fType == FieldType.list) {
					for (int i = 0; i < data.listCount(field); i++) {
						sb.Append(field.entity.id + "," 
								  + field.id + "," 
								  + field.fType + ",");

						// dataType
						sb.Append(DataUtil.dataTypeString(field.dType) + ",");
						// data value
						object val = data.data<object>(field, i);
						sb.Append(DataUtil.escapeString(DataUtil.dataString(field.dType,
																			val)) + ",");
						// mask
						if (data.mask(field,true) != null) {
							sb.Append(data.mask(field,true).binary());
						}
						sb.Append(",");
						sb.Append(i + "\n");
					}
				}
			}
			sb.Append("\n");
		}

		//------------------------------------------------------------
		// used to remove ents as they are added if filtered from read
		private static bool rmIfFiltered(Entity rmEnt, List<DataFilter> readFilters, DataSet data) {
			if (readFilters == null) {
				return false;
			}

			foreach (DataFilter filter in readFilters) {
				if (filter.matches(rmEnt, data)) {
					data.rm(rmEnt);
					return true;
				}
			}
			return false;
		}
	}
}
