using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Object = System.Object;

namespace DS {

	public partial class DataStore : DataSet {

		//========================================
		// Fields
		//========================================

		//------------------------------------------------------------
		public T data<T>(Field key) {
			Field correctKey = getTrueField(key);
			if (correctKey == null) {
				string keyS = (key == null) ? "null" : "key";
				Debug.LogError("Can't call data<"+ typeof(T).Name +"> on invalid field: " + keyS);
				return default(T);
			}
			if (key.fType != FieldType.single) {
				Debug.LogError("Can't call data(field) on non-single FieldType" + key);
				return default(T);
			}
			System.Object data;

			fieldData.TryGetValue(correctKey, out data);
			if (data == null) {
				// entityTypes can be null
				if (key.dType == DataType.entityType) {
					return default(T);
				}
				else {
					Debug.LogError("field is incorrect type ("+typeof(T).Name+"): " + data);
					return default(T);
				}
			}
			else if (data is T) {
				return (T) data;
			}
			else {
				Debug.LogError(key.entity + ":" + key.id + " field is incorrect type ("+typeof(T).Name+"): " + data);
				return default(T);
			}
		}

		//------------------------------------------------------------
		public T data<T>(string entId, string fieldId) {
			Entity ent = this[entId];
			if (ent == null) {
				Debug.LogError("Can't call data<"+typeof(T).Name+"> on invalid entity: " + entId);
				return default(T);
			}
			Field fieldKey = getTrueField(field(ent, fieldId));
			return data<T>(fieldKey);
		}

	
		//------------------------------------------------------------
		public T data<T>(Entity entKey, string fieldId) {
			Field key = field(entKey, fieldId);
			return data<T>(key);
		}

		//------------------------------------------------------------
		public T data<T>(Field key, int index) {
			Field correctKey = getTrueField(key);
			if (correctKey == null) {
				Debug.LogError("Can't call data<"+ typeof(T).Name +"> on invalid field: " + key);
				return default(T);
			}
			if (key.fType != FieldType.list) {
				Debug.LogError("Can't call data(field, index) on non-list FieldType" + key);
				return default(T);
			}
			List<System.Object> dataList;

			// if not found, field might belong to type
			fieldListData.TryGetValue(correctKey, out dataList);
		
			if (index < 0 || index >= dataList.Count) {
				Debug.LogError("index is out of range: " + index);
				return default(T);
			}
			else {
				System.Object data = dataList[index];
				if (data is T) {
					return (T) data;
				}
				else {
					Debug.LogError("field is incorrect type ("+typeof(T).Name+"): " + data);
					return default(T);
				}
			}
		}

		//------------------------------------------------------------
		public T data<T>(string entId, string fieldId, int index) {
			Entity ent = this[entId];
			if (ent == null) {
				Debug.LogError("Can't call data<"+typeof(T).Name+"> on invalid entity: " + entId);
				return default(T);
			}
			Field fieldKey = getTrueField(field(ent, fieldId));
			return data<T>(fieldKey, index);
		}
	
		//------------------------------------------------------------
		public T data<T>(Entity entKey, string fieldId, int index) {
			Field key = field(entKey, fieldId);
			return data<T>(key, index);
		}

		//------------------------------------------------------------
		public List<T> dataList<T>(Field key) {
			Field correctField = getTrueField(key);
			if (correctField == null) {
				Debug.LogError("Can't call data<"+ typeof(T).Name +"> on invalid field: " + key);
				return null;
			}
			if (key.fType != FieldType.list) {
				Debug.LogError("Can't call dataList(field) on non-list FieldType" + key);
				return null;
			}

			List<System.Object> dataList;
			fieldListData.TryGetValue(correctField, out dataList);

			List<T> tList = new List<T>();
			foreach (System.Object data in dataList) {
				if (data is T) {
					tList.Add((T)data);
				}
				else {
					Debug.LogError("field is incorrect type ("+typeof(T).Name+"): " + data);
					return null;
				}
			}
			return tList;
		}

		//------------------------------------------------------------
		public void insertDataList<T>(Field key, int index, T value) {
			if (!exists(key)) {
				Debug.LogError("Can't call data<"+ typeof(T).Name +"> on invalid field: " + key);
				return;
			}
			if (key.fType != FieldType.list) {
				Debug.LogError("Can't call insertDataList(key, index, value) on non-list field" + key);
				return;
			}

			List<System.Object> newList = createSpecificDataList<T>(key);

			if (newList != null) {
				if (index < 0 || index > newList.Count) {
					Debug.LogError("index is out of range: " + index);
					return;
				}
				// Check that list is of type T
				newList.Insert(index, (System.Object)value);
			}
			else {
				Debug.LogError("insertDataList failed");
			}
		}
	
		//------------------------------------------------------------
		public void addToDataList<T>(Field key, T value) {
			if (!exists(key)) {
				Debug.LogError("Can't call data<"+ typeof(T).Name +"> on invalid field: " + key);
				return;
			}
			if (key.fType != FieldType.list) {
				Debug.LogError("Can't call insertDataList(key, index, value) on non-list field" + key);
				return;
			}

			List<System.Object> newList = createSpecificDataList<T>(key);

			if (newList != null) {
				newList.Add((System.Object)value);
			}
			else {
				Debug.LogError("addToDataList failed");
			}
		}

		//------------------------------------------------------------
		public int listCount(Field field) {
			Field correct = getTrueField(field);
			if (correct == null || correct.fType != FieldType.list) {
				Debug.LogError("Can't call listCount on invalid field:" + field);
				return -1;
			}
		
			List<object> fieldList;
			fieldListData.TryGetValue(correct, out fieldList);
			return fieldList.Count;
		}

		//------------------------------------------------------------
		public Field field(Entity key, string fieldId) {
			if (!exists(key)) {
				string keyString = (key == null) ? "null" : key.ToString();
				Debug.LogError("Can't call field on invalid entity: " + keyString);
				return null;
			}
			// First check entity
			Dictionary<string, Field> eFields;
			if (entityFields.TryGetValue(key, out eFields)) {
				Field foundField;
				if (eFields.TryGetValue(fieldId, out foundField)) {
					return foundField;
				}
			}

			foreach (Entity entType in entTypes(key)) {
				if (entityFields.TryGetValue(entType, out eFields)) {
					Field foundField;
					if (eFields.TryGetValue(fieldId, out foundField)) {
						// Return field that matches entity in params
						return new Field (key, fieldId, foundField.fType, foundField.dType);
					}
				}
			}

			// Reaching here means not found
			Debug.LogError("could not find field(key="+ key + ", fieldId=" + fieldId + ")");
			return null;
		}

		//------------------------------------------------------------
		public Field field(string entId, string fieldId) {
			Entity ent = this[entId];
			return field(ent, fieldId);
		}

		//------------------------------------------------------------
		public HashSet<Field> fields(Entity key, bool specific=false) {
			if (!exists(key)) {
				Debug.LogError("Can't call fields on invalid entity: " + key);
				return null;
			}

			Dictionary <string, Field> returnFields;
			HashSet<Field> returnSet;
			// first grab entity fields
			if (entityFields.TryGetValue(key, out returnFields)) {
				returnSet = new HashSet<Field>(returnFields.Values);
			}
			else {
				returnSet = new HashSet<Field>();
			}
			// add type fields
			if ((key.type == EntType.node || key.type == EntType.nodeSet || key.type == EntType.link)
				&& !specific) {

				HashSet<Entity> eTypes;
				eTypes = entTypes(key);

				foreach (Entity eType in eTypes) {
					HashSet<Field> tFields = fields(eType);
					foreach (Field field in tFields) {
						Field addField = new Field(key, field.id, field.fType, field.dType);
						returnSet.Add(addField);
					}
				}
			}
			return returnSet;
		}

		//------------------------------------------------------------
		public bool exists(Field key, bool specific=false) {
			return exists(key.entity.id, key.id, specific);
		}

		//------------------------------------------------------------
		public bool exists(string entId, string fieldId, bool specific=false) {
			if (entId == null || entId == ""
				|| !exists(entId)) {
				return false;
			}
			if (fieldId == null || fieldId == "" ) {
				return false;
			}
		
			// First check entity
			Dictionary<string, Field> eFields;
			Entity ent = this[entId];
			if (entityFields.TryGetValue(ent, out eFields)) {
				Field foundField;
				if (eFields.TryGetValue(fieldId, out foundField)) {
					return true;
				}
			}
			if (specific) {
				return false;
			}

			// Field may belong to type
			foreach (Entity entType in entTypes(ent)) {
				if (entityFields.TryGetValue(entType, out eFields)) {
					Field foundField;
					if (eFields.TryGetValue(fieldId, out foundField)) {
						return true;
					}
				}
			}
			return false;
		}

		//------------------------------------------------------------
		public DataMask mask(Field key, bool specific=false) {
			if (!exists(key)) {
				Debug.LogError("Can't call mask on invalid field: " + key);
				return null;
			}

			DataMask fieldMask;
			fieldMasks.TryGetValue(key, out fieldMask);

			if (fieldMask != null || specific) {
				return fieldMask;
			}

			// return mask of entity
			return mask(key.entity);
		}

		//------------------------------------------------------------
		public Field addField<T>(Entity key, string fieldId, T value) {
			if (!exists(key)) {
				Debug.LogError("Can't call addField with invalid entity: " + key);
				return null;
			}
			if (fieldId == null || fieldId == "") {
				Debug.LogError("Can't call addField with invalid field name: " + fieldId);
				return null;
			}
			Field newField;

			DataType newDType = DataUtil.dataType(value);
			newField = new Field(key, fieldId, FieldType.single, newDType);

			if (fieldData.ContainsKey(newField) 
				|| fieldListData.ContainsKey(newField)) {
				Debug.LogError("Can't call addField with already existing field: " + key + ":" + fieldId);
				return null;
			}
			fieldData.Add(newField, value);
		
			// add to entityFields
			Dictionary<string, Field> fields;
			if (!entityFields.TryGetValue(key, out fields)) {
				Dictionary<string, Field> newDict = new Dictionary<string, Field>();
				entityFields.Add(key, newDict);
				entityFields.TryGetValue(key, out fields);
			}
			fields.Add(fieldId, newField);
		
			return newField;
		}

		//------------------------------------------------------------d
		public Field addFieldList<T>(Entity key, string fieldId) {
			if (!exists(key)) {
				Debug.LogError("Can't call addField with invalid entity: " + key);
				return null;
			}
			if (fieldId == null || fieldId == "") {
				Debug.LogError("Can't call addField with invalid field name: " + fieldId);
				return null;
			}
			Field newField;

			DataType newDType = DataUtil.listType<T>();
			newField = new Field(key, fieldId, FieldType.list, newDType);

			if (fieldData.ContainsKey(newField) 
				|| fieldListData.ContainsKey(newField)) {
				Debug.LogError("Can't call addField with already existing field: " + fieldId);
				return null;
			}
			List<System.Object> objList = new List<System.Object>();
			fieldListData.Add(newField, objList);			
		
			// Add to entFields
			Dictionary<string, Field> fields;
			if (!entityFields.TryGetValue(key, out fields)) {
				Dictionary<string, Field> newDict = new Dictionary<string, Field>();
				entityFields.Add(key, newDict);
				entityFields.TryGetValue(key, out fields);
			}
			fields.Add(fieldId, newField);

			return newField;
		}
	
		//------------------------------------------------------------
		public void edit<T>(Field key, T newValue) {
			Field correctField = getTrueField(key);
			if (correctField == null) {
				Debug.LogError("Can't call edit on invalid field: " + key);
			}
			if (key.fType != FieldType.single) {
				Debug.LogError("Can't call edit(key, value) on non-single field" + key);
				return;
			}

			if (correctField != key) {
				addField(key.entity, key.id, newValue);
			}
			else {
				fieldData[key] = newValue;
			}
		}

		//------------------------------------------------------------
		public void edit<T>(string entId, string fieldId, T newValue) {
			Field key = field(entId, fieldId);
			edit<T>(key, newValue);
		}

		//------------------------------------------------------------
		public void edit<T>(Entity ent, string fieldId, T newValue) {
			Field key = field(ent, fieldId);
			edit<T>(key, newValue);
		}
	
		//------------------------------------------------------------
		public void edit<T>(Field key, int index, T newValue) {
			if (!exists(key)) {
				Debug.LogError("Can't call edit on invalid field: " + key);
			}
			if (key.fType != FieldType.list) {
				Debug.LogError("Can't call edit(key, list, value) on non-list field" + key);
				return;
			}

			List<System.Object> objList = createSpecificDataList<T>(key);
			if (index < 0 || index >= objList.Count) {
				Debug.LogError("Index out of range: " + index);
				return;
			}

			if (objList != null) {
				objList[index] = (System.Object) newValue;
			}
		}
	
		//------------------------------------------------------------
		public void edit<T>(string entId, string fieldId, int index, T newValue) {
			Field key = field(entId, fieldId);
			edit<T>(key, index, newValue);
		}

		//------------------------------------------------------------
		public void edit<T>(Entity ent, string fieldId, int index, T newValue) {
			Field key = field(ent, fieldId);
			edit<T>(key, index, newValue);
		}

		//------------------------------------------------------------
		public void rm(Field key) {
			if (!exists(key, true)) {
				if (!exists(key)) {
					Debug.LogError("Can't call rm on invalid field: " + key);
				}
				else {
					Debug.LogError("Can't rm Field that is inherited by type");
				}
			}

			if (key.fType == FieldType.single) {
				fieldData.Remove(key);
			}
			else if (key.fType == FieldType.list) {
				fieldListData.Remove(key);
			}
			fieldMasks.Remove(key);
		
			Dictionary<string, Field> fields;
			entityFields.TryGetValue(key.entity, out fields);
			fields.Remove(key.id);
			if (fields.Count == 0) {
				entityFields.Remove(key.entity);
			}
		}

		//------------------------------------------------------------
		public void rm(Entity key, string fieldId) {
			Field rmField = field(key, fieldId);
			rm(rmField);
		}

		//------------------------------------------------------------
		public void rm(Field key, int index) {
			if (!exists(key, true)) {
				if (!exists(key)) {
					Debug.LogError("Can't call rm on invalid field: " + key);
				}
				else {
					Debug.LogError("Can't rm Field that is inherited by type");
				}
			}
			if (key.fType != FieldType.list) {
				Debug.LogError("Can't call edit(key, list, value) on non-list field" + key);
				return;
			}

			List<System.Object> objList;
			fieldListData.TryGetValue(key, out objList);
			if (index < 0 || index >= objList.Count) {
				Debug.LogError("Index out of range: " + index);
				return;
			}
			objList.RemoveAt(index);
		}

		//------------------------------------------------------------
		public void setMask(Field key, DataMask newMask) {
			if (!exists(key)) {
				Debug.LogError("Can't call setMask on invalid field: " + key);
			}
			DataMask oldMask;
			if (fieldMasks.TryGetValue(key, out oldMask)) {
				fieldMasks[key] = newMask;
			}
			else {
				fieldMasks.Add(key, newMask);
			}
		}

		//------------------------------------------------------------
		public HashSet<Field> allFields() {
			HashSet<Field> singleFields = new HashSet<Field>(fieldData.Keys);
			HashSet<Field> listFields = new HashSet<Field>(fieldListData.Keys);
		
			singleFields.UnionWith(listFields);
			return singleFields;
		}

		//------------------------------------------------------------
		private void cloneFields(Entity newEnt, Entity oldEnt) {
			HashSet<Field> oldFields = fields(oldEnt, true);
			foreach (Field oldField in oldFields) {
				if (oldField.fType == FieldType.list) {
					Field newField;
					switch(oldField.dType) {
					case DataType.intType:
						newField = addFieldList<int>(newEnt, oldField.id);
						foreach (int val in dataList<int>(oldField)) {
							addToDataList<int>(newField, val);
						}
						break;
					case DataType.floatType:
						newField = addFieldList<float>(newEnt, oldField.id);
						foreach (float val in dataList<float>(oldField)) {
							addToDataList<float>(newField, val);
						}
						break;
					case DataType.stringType:
						newField = addFieldList<string>(newEnt, oldField.id);
						foreach (string val in dataList<string>(oldField)) {
							addToDataList<string>(newField, val);
						}
						break;
					case DataType.entityType:
						newField = addFieldList<Entity>(newEnt, oldField.id);
						foreach (Entity val in dataList<Entity>(oldField)) {
							addToDataList<Entity>(newField, val);
						}
						break;
					case DataType.colorType:
						newField = addFieldList<Color>(newEnt, oldField.id);
						foreach (Color val in dataList<Color>(oldField)) {
							addToDataList<Color>(newField, val);
						}
						break;
					case DataType.dataType:
						newField = addFieldList<DataObject>(newEnt, oldField.id);
						foreach (DataObject val in dataList<DataObject>(oldField)) {
							addToDataList<DataObject>(newField, val);
						}
						break;
					case DataType.objectType:
						newField = addFieldList<object>(newEnt, oldField.id);
						foreach (object val in dataList<object>(oldField)) {
							addToDataList<object>(newField, val);
						}
						break;
					}
				}
				else
					switch(oldField.dType) {
					case DataType.intType:
						addField<int>(newEnt, oldField.id, data<int>(oldField));
						break;
					case DataType.floatType:
						addField<float>(newEnt, oldField.id, data<float>(oldField));
						break;
					case DataType.stringType:
						addField<string>(newEnt, oldField.id, data<string>(oldField));
						break;
					case DataType.entityType:
						addField<Entity>(newEnt, oldField.id, data<Entity>(oldField));
						break;
					case DataType.colorType:
						addField<Color>(newEnt, oldField.id, data<Color>(oldField));
						break;
					case DataType.dataType:
						addField<DataObject>(newEnt, oldField.id, data<DataObject>(oldField));
						break;
					case DataType.objectType:
						addField<Object>(newEnt, oldField.id, data<Object>(oldField));
						break;
					}
			}
		}
	}
}
