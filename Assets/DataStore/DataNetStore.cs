﻿using System;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	// Contains portion of Class that is not in DataSet
	public partial class DataNetStore : Bolt.GlobalEventListener, DataSet {
		public DataStore ds;
		public DataStringListener stringListener = null;

		public const int MAX_STRING_SIZE = 140;
		
		[HideInInspector]
		bool initialized = false;

		// used to store DataString messages until they are completed
		Dictionary<int, List<string>> receivedDataStrings
		= new Dictionary<int, List<string>>();
		// used to store DataString required lengths
		Dictionary<int, int> dataStringCounts
		= new Dictionary<int, int>();
		
		// used to receive changes by client when it is receiving changes
		// and not yet ready to apply them
		LinkedList<DataChange> receivedChanges =
			new LinkedList<DataChange>();

		// used to store changes that are being made and bolt not ready
		// to transmit them.
		LinkedList<DataChange> queuedChanges =
			new LinkedList<DataChange>();

		//========================================
		// DataListeners
		//========================================
		// used to store hash to make datachanges faster
		Dictionary<Entity, HashSet<DataListener>> entListeners;
		Dictionary<DataListener, DataFilter> listeners;

		//========================================
		// Settings
		//========================================

		bool receiveStrings = true;
		
		//When set to true, changes made to server will be transmitted
		bool syncChanges = false;

		//------------------------------------------------------------
		public void OnBoltStartDone() {
			if (syncChanges && queuedChanges.Count > 0) {
				foreach (DataChange dc in queuedChanges) {
					transmitChange(dc);
				}
				queuedChanges.Clear();
			}
		}
		
		//------------------------------------------------------------
		public void init() {
			initialized = true;
			ds = new DataStore();

			entListeners = new Dictionary<Entity, HashSet<DataListener>>();
			listeners = new Dictionary<DataListener, DataFilter>();
			receivedChanges = new LinkedList<DataChange>();
			queuedChanges = new LinkedList<DataChange>();
		}

		//------------------------------------------------------------
		public void startSyncing() {
			if (!initialized) {
				Debug.LogError("Can't sync when not initialized!");
				return;
			}
			syncChanges = true;
		}
		
		//------------------------------------------------------------
		public void addListener(DataListener listener, DataFilter filter) {
			listeners.Add(listener, filter);

			// add entities to entListeners that match
			HashSet<Entity> apply = ds.allNodes();
			apply = filter.matches(apply, ds);
			foreach (Entity ent in apply) {
				addEntToListener(ent, listener);
			}
		}

		//------------------------------------------------------------
		public void rmListener(DataListener listener) {
			listeners.Remove(listener);

			foreach (Entity ent in entListeners.Keys) {
				entListeners[ent].Remove(listener);
			}
		}

		//------------------------------------------------------------
		public void addEntToListener(Entity ent, DataListener listener) {
			if (!entListeners.ContainsKey(ent)) {
				entListeners.Add(ent, new HashSet<DataListener>());
			}
			entListeners[ent].Add(listener);
		}

		//------------------------------------------------------------
		public void updateListenersNewEnt(Entity newEnt, DataChange dc) {
			foreach (DataListener listener in listeners.Keys) {
				if (listeners[listener].matches(newEnt, ds)) {
					addEntToListener(newEnt, listener);
					listener.notify(dc);
				}
			}
		}
		
		//------------------------------------------------------------
		public void notifyListeners(DataChange dc, params Entity[] ents) {

			// grab listeners that match
			foreach (Entity ent in ents) {
				HashSet<DataListener> dls;
				if (entListeners.TryGetValue(ent, out dls)) {
					if (dls != null && dls.Count > 0) {
						foreach (DataListener listener in dls) {
							listener.notify(dc);
						}
					}
				}
			}
		}

		//------------------------------------------------------------
		public void applyChange(DataChange change) {
			
			switch(change.type) {
			case ChangeType.clearAll:
				clearAll();
				break;
			case ChangeType.addEnt:
				addEnt(change);
				break;
			case ChangeType.addLink:
				addLink(ds[change.linkType], ds[change.parent], ds[change.child]);
				break;
			case ChangeType.addField:
			case ChangeType.addFieldIndex:
				addField(change);
				break;
			case ChangeType.addFieldList:
				addFieldList(change);
				break;
			case ChangeType.rmEnt:
				rm(ds[change.ent]);
				break;
			case ChangeType.rmField:
				rm(ds.field(ds[change.ent], change.field));
				break;
			case ChangeType.rmFieldIndex:				
				rm(ds.field(ds[change.ent], change.field), change.index);
				break;
			case ChangeType.edit:
			case ChangeType.editIndex:
				edit(change);
				break;
			case ChangeType.addToGroup:
				addToGroup(change);
				break;
			case ChangeType.rmFromGroup:
				rmFromGroup(change);
				break;
			case ChangeType.rename:
				renameNode(ds[change.ent], change.dataString);
				break;
			case ChangeType.clone:
				bool parentLinks = false;
				if (change.index == 1) {
					parentLinks = true;
				}
				cloneNode(ds[change.ent], change.dataString, parentLinks);
				break;
			default:
				Debug.LogError("applyChange case not caught! " + change.type);
				break;
			}
		}

		//------------------------------------------------------------
		public void sendDataStringToServer(string dataString) {
			if (!BoltNetwork.isClient) {
				return;
			}
			int msgCount = dataString.Length / MAX_STRING_SIZE;
			if (dataString.Length % MAX_STRING_SIZE > 0) {
				msgCount++;
			}
			
			var dataStringStart
				= DataStringStartEvent.Create(BoltNetwork.server,
											  Bolt.ReliabilityModes.ReliableOrdered);
			dataStringStart.id = Random.Range(0, Int32.MaxValue);
			dataStringStart.msgCount = msgCount;
			dataStringStart.Send();

			for (int i = 0; i < msgCount; i++) {
				int start = i * MAX_STRING_SIZE;
				var dataStringEvent
					= DataStringEvent.Create(BoltNetwork.server,
											 Bolt.ReliabilityModes.ReliableOrdered);
				dataStringEvent.id = dataStringStart.id;

				int size = MAX_STRING_SIZE;
				if (start + size > dataString.Length) {
					size = dataString.Length - start;
				}
				
				dataStringEvent.data = dataString.Substring(start, size);
				dataStringEvent.Send();
			}

		}

		//--------------------------------------------------
		public void stopReceivingDataStrings() {
			receivedChanges.Clear();
			receiveStrings = false;
		}

		//========================================
		// OnEvent processing
		//========================================

		//------------------------------------------------------------
		public override void OnEvent(DataStringStartEvent e) {
			if (!receiveStrings) {
				return;
			}
			if (stringListener == null) {
				Debug.LogError("Need stringListener for sent DataStrings");
			}
			receivedDataStrings.Add(e.id, new List<string>());
			dataStringCounts.Add(e.id, e.msgCount);
		}

		//------------------------------------------------------------
		public override void OnEvent(DataStringEvent e) {
			if (!receiveStrings) {
				return;
			}

			if (stringListener == null) {
				Debug.LogError("Need stringListener for sent DataStrings");
			}
			List<string> stringList;
			if (receivedDataStrings.TryGetValue(e.id, out stringList)) {
				stringList.Add(e.data);
				if (stringList.Count == dataStringCounts[e.id]) {
					StringBuilder sb = new StringBuilder();
					foreach (string s in stringList) {
						sb.Append(s);
					}
					stringListener.stringReceived(sb.ToString(), e.RaisedBy);
					receivedDataStrings.Remove(e.id);
					dataStringCounts.Remove(e.id);
				}
			}
		}
		
		//------------------------------------------------------------
		public override void OnEvent(ClearEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.clearAll;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(AddEntEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addEnt;
			dc.entType = (EntType) e.entType;
			dc.ent = e.ent;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(AddLinkEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addLink;
			dc.parent = e.parent;
			dc.child = e.child;
			dc.linkType = e.type;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(AddFieldEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addField;
			dc.ent = e.ent;
			dc.field = e.field;
			dc.dType = (DataType) e.dType;
			dc.dataString = e.data;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(AddFieldIEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addFieldIndex;
			dc.ent = e.ent;
			dc.field = e.field;
			dc.dType = (DataType) e.dType;
			dc.dataString = e.data;
			dc.index = e.index;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(AddFieldListEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addFieldList;
			dc.ent = e.ent;
			dc.field = e.field;
			dc.dType = (DataType) e.dType;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(RmEntEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.rmEnt;
			dc.ent = e.ent;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(RmFieldEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.rmField;
			dc.ent = e.ent;
			dc.field = e.field;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(RmFieldIEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.rmFieldIndex;
			dc.ent = e.ent;
			dc.field = e.field;
			dc.index = e.index;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(EditEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.edit;
			dc.ent = e.ent;
			dc.field = e.field;
			dc.dType = (DataType) e.dType;
			dc.dataString = e.data;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(EditIEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.editIndex;
			dc.ent = e.ent;
			dc.field = e.field;
			dc.dType = (DataType) e.dType;
			dc.dataString = e.data;
			dc.index = e.index;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(AddGroupEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addToGroup;
			dc.ent = e.ent;
			dc.group = e.group;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		
		//------------------------------------------------------------
		public override void OnEvent(RmGroupEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.rmFromGroup;
			dc.ent = e.ent;
			dc.group = e.group;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(RenameNodeEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.rename;
			dc.dataString = e.name;
			dc.ent = e.ent;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}

		//------------------------------------------------------------
		public override void OnEvent(CloneNodeEvent e) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.clone;
			dc.dataString = e.name;
			dc.index = e.index;
			dc.ent = e.ent;
			if (syncChanges) {
				applyChange(dc);
			}
			else {
				receivedChanges.AddLast(dc);
			}
		}


		//************************************************************
		// Private methods
		//************************************************************
		
		//------------------------------------------------------------
		private void transmitChange(DataChange change) { 
			if (!syncChanges || 
                (BoltNetwork.isConnected && BoltNetwork.isClient)) {
				return;
			}
			if (!BoltNetwork.isRunning) {
				queuedChanges.AddLast(change);
			}
			else {
				switch(change.type) {
				case ChangeType.clearAll:
                    var clearEvent
						= ClearEvent.Create(Bolt.GlobalTargets.AllClients,
											Bolt.ReliabilityModes.ReliableOrdered);
                    clearEvent.Send();
					break;
				case ChangeType.addEnt:
					var addEntEvent
						= AddEntEvent.Create(Bolt.GlobalTargets.AllClients,
											 Bolt.ReliabilityModes.ReliableOrdered);
					addEntEvent.entType = (int) change.entType;
					addEntEvent.ent = change.ent;
					addEntEvent.Send();
					break;
				case ChangeType.addLink:
					var addLinkEvent
						= AddLinkEvent.Create(Bolt.GlobalTargets.AllClients,
											  Bolt.ReliabilityModes.ReliableOrdered);
					addLinkEvent.parent = change.parent;
					addLinkEvent.child = change.child;
					addLinkEvent.type = change.linkType;
					addLinkEvent.Send();
					break;
				case ChangeType.addField:
					var addFieldEvent
						= AddFieldEvent.Create(Bolt.GlobalTargets.AllClients,
											   Bolt.ReliabilityModes.ReliableOrdered);
					addFieldEvent.ent = change.ent;
					addFieldEvent.field = change.field;
					addFieldEvent.dType = (int) change.dType;
					addFieldEvent.data = change.dataString;
					addFieldEvent.Send();
					break;
				case ChangeType.addFieldIndex:
					var addFieldIEvent
						= AddFieldIEvent.Create(Bolt.GlobalTargets.AllClients,
												Bolt.ReliabilityModes.ReliableOrdered);
					addFieldIEvent.ent = change.ent;
					addFieldIEvent.field = change.field;
					addFieldIEvent.dType = (int) change.dType;
					addFieldIEvent.data = change.dataString;
					addFieldIEvent.index = change.index;
					addFieldIEvent.Send();
					break;
				case ChangeType.addFieldList:
					var addFieldListEvent
						= AddFieldListEvent.Create(Bolt.GlobalTargets.AllClients,
												   Bolt.ReliabilityModes.ReliableOrdered);
					addFieldListEvent.ent = change.ent;
					addFieldListEvent.field = change.field;
					addFieldListEvent.dType = (int) change.dType;
					addFieldListEvent.Send();
					break;
				case ChangeType.rmEnt:
					var rmEntEvent
						= RmEntEvent.Create(Bolt.GlobalTargets.AllClients,
											Bolt.ReliabilityModes.ReliableOrdered);
					rmEntEvent.ent = change.ent;
					rmEntEvent.Send();
					break;
				case ChangeType.rmField:
					var rmFieldEvent
						= RmFieldEvent.Create(Bolt.GlobalTargets.AllClients,
											  Bolt.ReliabilityModes.ReliableOrdered);
					rmFieldEvent.ent = change.ent;
					rmFieldEvent.field = change.field;
					rmFieldEvent.Send();
					break;
				case ChangeType.rmFieldIndex:
					var rmFieldIEvent
						= RmFieldIEvent.Create(Bolt.GlobalTargets.AllClients,
											   Bolt.ReliabilityModes.ReliableOrdered);
					rmFieldIEvent.ent = change.ent;
					rmFieldIEvent.field = change.field;
					rmFieldIEvent.index = change.index;
					rmFieldIEvent.Send();
					break;
				case ChangeType.edit:
					var editEvent
						= EditEvent.Create(Bolt.GlobalTargets.AllClients,
										   Bolt.ReliabilityModes.ReliableOrdered);
					editEvent.ent = change.ent;
					editEvent.field = change.field;
					editEvent.dType = (int) change.dType;
					editEvent.data = change.dataString;
					editEvent.Send();
					break;
				case ChangeType.editIndex:
					var editIEvent
						= EditIEvent.Create(Bolt.GlobalTargets.AllClients,
											Bolt.ReliabilityModes.ReliableOrdered);
					editIEvent.ent = change.ent;
					editIEvent.field = change.field;
					editIEvent.dType = (int) change.dType;
					editIEvent.data = change.dataString;
					editIEvent.index = change.index;
					editIEvent.Send();
					break;
				case ChangeType.addToGroup:
					var addGroupEvent =
						AddGroupEvent.Create(Bolt.GlobalTargets.AllClients,
											 Bolt.ReliabilityModes.ReliableOrdered);
					addGroupEvent.ent = change.ent;
					addGroupEvent.group = change.group;
					addGroupEvent.Send();
					break;
				case ChangeType.rmFromGroup:
					var rmGroupEvent =
						RmGroupEvent.Create(Bolt.GlobalTargets.AllClients,
											Bolt.ReliabilityModes.ReliableOrdered);
					rmGroupEvent.ent = change.ent;
					rmGroupEvent.group = change.group;
					rmGroupEvent.Send();
					break;
				case ChangeType.rename:
					var renameEvent
						= RenameNodeEvent.Create(Bolt.GlobalTargets.AllClients,
												 Bolt.ReliabilityModes.ReliableOrdered);
					renameEvent.entType = (int) change.entType;
					renameEvent.ent = change.ent;
					renameEvent.name = change.dataString;
					renameEvent.Send();
					break;
				case ChangeType.clone:
					var cloneEvent
						= CloneNodeEvent.Create(Bolt.GlobalTargets.AllClients,
												Bolt.ReliabilityModes.ReliableOrdered);
					cloneEvent.entType = (int) change.entType;
					cloneEvent.ent = change.ent;
					cloneEvent.name = change.dataString;
					cloneEvent.index = change.index;
					cloneEvent.Send();
					break;
				default:
					Debug.LogError("Change not caught: " + change.type);
					break;
				}
			}
		}

		//------------------------------------------------------------
		private void addEnt(DataChange change) {
			switch(change.entType) {
			case EntType.node:
				addNode(change.ent);
				break;
			case EntType.nodeType:
				addNodeType(change.ent);
				break;
			case EntType.nodeSet:
				addNodeSet(change.ent);
				break;
			case EntType.linkType:
				addLinkType(change.ent);
				break;
			case EntType.linkSet:
				addLinkSet(change.ent);
				break;
			case EntType.link:
				string[] ents = change.ent.Split(':');
				addLink(ds[ents[0]], ds[ents[1]], ds[ents[2]]);
				break;
			}
		}

		//------------------------------------------------------------
		private void addField(DataChange change) {
			Entity ent = ds[change.ent];
			// add single field
			if (change.type == ChangeType.addField) {
				switch(change.dType) {
				case DataType.intType:
					addField<int>(ent, change.field,
									 DataUtil.toInt(change.dataString));
					break;
				case DataType.floatType:
					addField<float>(ent, change.field,
									   DataUtil.toFloat(change.dataString));
					break;
				case DataType.stringType:
					addField<string>(ent, change.field,
										change.dataString);
					break;
				case DataType.entityType:
					if (change.dataString == null || change.dataString == "") {
						addField<Entity>(ent, change.field, null);
					}
					else {
						addField<Entity>(ent, change.field,
											ds[change.dataString]);
					}
					break;
				case DataType.colorType:
					addField<Color>(ent, change.field,
									   DataUtil.toColor(change.dataString));
					break;
				case DataType.dataType:
					addField<DataObject>(ent, change.field,
											DataList.getNewDataObject(change.dataString));
					break;
				case DataType.objectType:
					Debug.LogError("Networking objects in field not supported!");
					break;
				}
			}
			// addFieldIndex
			else {
				Field listField = ds.field(ent, change.field); 
				switch(change.dType) {
				case DataType.intType:
					insertDataList<int>(listField, change.index,
										   DataUtil.toInt(change.dataString));
					break;
				case DataType.floatType:
					insertDataList<float>(listField, change.index,
											 DataUtil.toFloat(change.dataString));
					break;
				case DataType.stringType:
					insertDataList<string>(listField, change.index,
											  change.dataString);
					break;
				case DataType.entityType:
					if (change.dataString == null || change.dataString == "") {
						insertDataList<Entity>(listField, change.index, null);
					}
					else {
						insertDataList<Entity>(listField, change.index,
												  ds[change.dataString]);
					}
					break;
				case DataType.colorType:
					insertDataList<Color>(listField, change.index,
											 DataUtil.toColor(change.dataString));
					break;
				case DataType.dataType:
					insertDataList<DataObject>(listField, change.index,
												  DataList.getNewDataObject(change.dataString));
					break;
				case DataType.objectType:
					Debug.LogError("Networking objects in field not supported!");
					break;
				}
			}
		}
		
		//------------------------------------------------------------
		private void addFieldList(DataChange change) {
			Entity ent = ds[change.ent];
					
			switch(change.dType) {
			case DataType.intType:
				addFieldList<int>(ent, change.field);
				break;
			case DataType.floatType:
				addFieldList<float>(ent, change.field);
				break;
			case DataType.stringType:
				addFieldList<string>(ent, change.field);
				break;
			case DataType.entityType:
				addFieldList<Entity>(ent, change.field);
				break;
			case DataType.colorType:
				addFieldList<Color>(ent, change.field);
				break;
			case DataType.dataType:
				addFieldList<DataObject>(ent, change.field);
				break;
			case DataType.objectType:
				Debug.LogError("Networking objectType not supported!");
				break;
			}
		}

		//------------------------------------------------------------
		private void edit(DataChange change) {
			Entity ent = ds[change.ent];
			// add single field
			if (change.type == ChangeType.edit) {
				switch(change.dType) {
				case DataType.intType:
					int newValue = DataUtil.toInt(change.dataString);
					edit<int>(ent, change.field, newValue);
					break;
				case DataType.floatType:
					edit<float>(ent, change.field,
								   DataUtil.toFloat(change.dataString));
					break;
				case DataType.stringType:
					edit<string>(ent, change.field,
									change.dataString);
					break;
				case DataType.entityType:
					if (change.dataString == null || change.dataString == "") {
						edit<Entity>(ent, change.field, null);
					}
					else {
						edit<Entity>(ent, change.field,
										ds[change.dataString]);
					}
					break;
				case DataType.colorType:
					edit<Color>(ent, change.field,
								   DataUtil.toColor(change.dataString));
					break;
				case DataType.dataType:
					edit<DataObject>(ent, change.field,
										DataList.getNewDataObject(change.dataString));
					break;
				case DataType.objectType:
					Debug.LogError("Networking objects in field not supported!");
					break;
				}
			}
			// editIndex
			else {
				Field listField = ds.field(ent, change.field); 
				switch(change.dType) {
				case DataType.intType:
					edit<int>(listField, change.index,
								 DataUtil.toInt(change.dataString));
					break;
				case DataType.floatType:
					edit<float>(listField, change.index,
								   DataUtil.toFloat(change.dataString));
					break;
				case DataType.stringType:
					edit<string>(listField, change.index,
									change.dataString);
					break;
				case DataType.entityType:
					if (change.dataString == null || change.dataString == "") {
						edit<Entity>(listField, change.index, null);
					}
					else {
						edit<Entity>(listField, change.index,
										ds[change.dataString]);
					}
					break;
				case DataType.colorType:
					edit<Color>(listField, change.index,
								   DataUtil.toColor(change.dataString));
					break;
				case DataType.dataType:
					edit<DataObject>(listField, change.index,
										DataList.getNewDataObject(change.dataString));
					break;
				case DataType.objectType:
					Debug.LogError("Networking objects in field not supported!");
					break;
				}
			}
		}

		//------------------------------------------------------------
		private void addToGroup(DataChange change) {
			Entity ent = ds[change.ent];
			Entity group = ds[change.group];
			switch (group.type) {
			case EntType.nodeSet:
			case EntType.linkSet:
				addEntToSet(ent, group);
				break;
			case EntType.linkType:
			case EntType.nodeType:
				addEntToType(ent, group);
				break;
			}
		}

		//------------------------------------------------------------
		private void rmFromGroup(DataChange change) {
			Entity ent = ds[change.ent];
			Entity group = ds[change.group];
			switch (group.type) {
			case EntType.nodeSet:
			case EntType.linkSet:
				rmEntFromSet(ent, group);
				break;
			case EntType.nodeType:
			case EntType.linkType:
				rmEntFromType(ent, group);
				break;
			}
		}
	}
}
