using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public class DataFilter {
		public readonly string id;

		// filters earlier in list have higher precedence
		public List<DataFilterItem> filters;

		//------------------------------------------------------------
		public DataFilter(string id) {
			this.id = id;

			filters = new List<DataFilterItem>();
		}

		//------------------------------------------------------------
		public bool matches(Entity checkEnt, DataSet data) {
			HashSet<Entity> startEnts = new HashSet<Entity>();
			startEnts.Add(checkEnt);

			HashSet<Entity> matchSet = matches(startEnts, data);
			if (matchSet.Count > 0) {
				return true;
			}
			return false;
		}

		//------------------------------------------------------------
		public HashSet<Entity> matches(HashSet<Entity> startEnts, DataSet data) {

			if (startEnts == null) {
				return null;
			}
			
			HashSet<Entity> includeSet = new HashSet<Entity>();
			HashSet<Entity> remaining = new HashSet<Entity>(startEnts);

			foreach (DataFilterItem filter in filters) {

				HashSet<Entity> filterEnts;

				// all, put remaining into includeSet
				if (filter.type == FilterType.include && filter.mode == FilterMode.all) {
					includeSet.UnionWith(remaining);
					break;
				}
				else if (filter.type == FilterType.include && filter.mode == FilterMode.all) {
					// return remaining
					break;
				}
				else if (filter.mode == FilterMode.allNodeTypes) {
					filterEnts = data.allNodeTypes();
				}
				else if (filter.mode == FilterMode.allNodeTypes) {
					filterEnts = data.allNodeTypes();
				}
				else if (filter.mode == FilterMode.allNodeSets) {
					filterEnts = data.allNodeSets();
				}
				else if (filter.mode == FilterMode.allNodes) {
					filterEnts = data.allNodes();
				}
				else if (filter.mode == FilterMode.allLinkTypes) {
					filterEnts = data.allLinkTypes();
				}
				else if (filter.mode ==  FilterMode.allLinkSets) {
					filterEnts = data.allLinkTypes();
				}
				else if (filter.mode == FilterMode.allLinks) {
					filterEnts = data.allLinks();
				}
				else if (filter.mode == FilterMode.group) {
					filterEnts = new HashSet<Entity>();
					foreach (string groupEntS in filter.ents) {
						if (!data.exists(groupEntS)) {
							continue;
						}
						Entity groupEnt = data[groupEntS];
						switch(groupEnt.type) {
						case EntType.nodeType:
						case EntType.linkType:
							filterEnts.UnionWith(data.typeEnts(groupEnt));
							break;
						case EntType.nodeSet:
						case EntType.linkSet:
							filterEnts.UnionWith(data.setEnts(groupEnt));
							break;
						}
					}
				}
				// mode = ent
				else {
					filterEnts = new HashSet<Entity>();
					foreach (string entS in filter.ents) {
						if (data.exists(entS)) {
							filterEnts.Add(data[entS]);
						}
					}
				}

				// new Intersect are ents that will be affected
				HashSet<Entity> newIntersect = new HashSet<Entity>(remaining);
				newIntersect.IntersectWith(filterEnts);
				remaining.ExceptWith(newIntersect);

				if (filter.type == FilterType.include) {
					includeSet.UnionWith(newIntersect);
				}

				if (remaining.Count == 0) {
					break;
				}
			}

			return includeSet;
		}

		//------------------------------------------------------------
		public override bool Equals(System.Object obj) {
			if (obj == null) {
				return false;
			}
    
			DataFilter df = obj as DataFilter;
			if ((System.Object) df == null) {
				return false;
			}
    
			return (id == df.id);
		}
  
		//------------------------------------------------------------
		public bool Equals(DataFilter df) {
			if ((object)df == null) {
				return false;
			}

			return (id == df.id);
		}

		//------------------------------------------------------------
		public override int GetHashCode() {
			return id.GetHashCode();
		}

		//------------------------------------------------------------
		public override string ToString() {
			return "DataFilter:" + id;
		}

	}
}
