public enum EntType {
	nodeType,
	nodeSet,
	linkType,
	linkSet,
	node,
	link
}

public enum FieldType {
	single,
	list
}

public enum ReadingMode {
	unknown,
	node,
	link,
	nodeType,
	linkType,
	nodeSet,
	nodeSets,
	linkSet,
	field,
	script
}

public enum DataType {
	intType,
	floatType,
	stringType,
	entityType,
	colorType,
	dataType,
	objectType
}

public enum FilterType {
	include,
	exclude
}

public enum FilterMode {
	all,
	allNodeTypes,
	allNodeSets,
	allNodes,
	allLinkTypes,
	allLinkSets,
	allLinks,
	ent,
	group
}

public enum ChangeType {
	clearAll,
	addEnt,
	addLink,
	addField,
	addFieldIndex,
	addFieldList,
	rmEnt,
	rmField,
	rmFieldIndex,
	edit,
	editIndex,
	addToGroup,
	rmFromGroup,
	clone,
	rename
}

