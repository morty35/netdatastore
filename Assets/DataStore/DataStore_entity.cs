using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public partial class DataStore : DataSet {

		//------------------------------------------------------------
		public DataStore() {
			entityByString	= new Dictionary<string, Entity>();
			fieldData		= new Dictionary<Field, object>();
			fieldListData	= new Dictionary<Field, List<object>>();
			fieldMasks		= new Dictionary<Field, DataMask>();
			entityFields	= new Dictionary<Entity, Dictionary<string, Field>>();
			entities		= new HashSet<Entity>();
			entityMasks		= new Dictionary<Entity, DataMask>();
			setEntLists		= new Dictionary<Entity, HashSet<Entity>>();
			entSetLists		= new Dictionary<Entity, HashSet<Entity>>();
			setParentLists  = new Dictionary<Entity, HashSet<Entity>>();
			setChildLists  = new Dictionary<Entity, HashSet<Entity>>();
			typeEntLists	= new Dictionary<Entity, HashSet<Entity>>();
			entTypeLists	= new Dictionary<Entity, HashSet<Entity>>();
			typeParentLists = new Dictionary<Entity, HashSet<Entity>>();
			typeChildLists = new Dictionary<Entity, HashSet<Entity>>();
			nodeChildLinks	= new Dictionary<Entity, Dictionary<Entity, HashSet<Entity>>>();
			nodeParentLinks = new Dictionary<Entity, Dictionary<Entity, HashSet<Entity>>>();
			linkChildren	= new Dictionary<Entity, Entity>();
			linkParents		= new Dictionary<Entity, Entity>();
		}

		//------------------------------------------------------------
		public void clearAll() {
			entityByString.Clear();
			fieldData.Clear();
			fieldListData.Clear();
			fieldMasks.Clear();
			entityFields.Clear();
			entities.Clear();
			entityMasks.Clear();
			setEntLists.Clear();
			entSetLists.Clear();
			setParentLists.Clear();
			setChildLists.Clear();
			typeEntLists.Clear();
			entTypeLists.Clear();
			typeParentLists.Clear();
			typeChildLists.Clear();
			nodeChildLinks.Clear();
			nodeParentLinks.Clear();
			linkChildren.Clear();
			linkParents.Clear();
		}
	
		//------------------------------------------------------------
		public Entity this[string id] {
			get {
				if (id == null) {
					Debug.LogError("Can't call data[null]");
					return null;
				}
				else if (id == "") {
					Debug.LogError("Can't call data[\"\"]");
					return null;
				}
				Entity key;
				if (!entityByString.TryGetValue(id, out key)) {
					Debug.LogError("DataStore[" + id + "] not valid key");
					return null;
				}
				else {
					return key;
				}
			}
			set {}
		}

		//========================================
		// Entity
		//========================================

		//------------------------------------------------------------
		public Entity ent(string id) {
			return this[id];
		}

		//------------------------------------------------------------
		public HashSet<Entity> allEnts() {
			return new HashSet<Entity>(entities);
		}

		//------------------------------------------------------------
		public DataMask mask(Entity key, bool specific=false) {
			if (!exists(key)) {
				Debug.LogError("Can't call mask on invalid entity: " + key);
				return null;
			}

			// Types don't have masks
			if (key.type == EntType.nodeType 
				|| key.type == EntType.linkType) {
				return null;
			}
			DataMask entMask;
			entityMasks.TryGetValue(key, out entMask);

			// Return mask of entity if set or specific
			if (key.type == EntType.nodeSet 
				|| key.type == EntType.linkSet
				|| specific) {
				return entMask;
			}

			if (entMask == null) {
				entMask = new DataMask(DataMask.NONE);
			}

			// Combine entMask with all set masks
			HashSet<Entity> sets = entSets(key);
			if (sets != null) {
				foreach (Entity set in sets) {
					DataMask dm;
					if (entityMasks.TryGetValue(set, out dm)) {
						entMask = entMask.combine(dm);
					}
				}
			}
			
			return entMask;
		}
	
		//------------------------------------------------------------
		public bool exists(Entity key) {
			if (key == null || key.id == null || key.id == "") {
				return false;
			}
			return entities.Contains(key);
		}

		//------------------------------------------------------------
		public bool exists(string id) {
			if (id == null || id == "") {
				return false;
			}
			return entityByString.ContainsKey(id);
		}

		//------------------------------------------------------------
		public Entity addNode(string id) {
			if (id == null || id == "") {
				Debug.LogError("Can't addNode with id: " + id);
				return null;
			}
			return addEntity(id, EntType.node);
		}


		//------------------------------------------------------------
		public Entity addNodeSet(string id) {
			if (id == null || id == "") {
				Debug.LogError("Can't addNodeSet with id: " + id);
				return null;
			}
			Entity newEnt = addEntity(id, EntType.nodeSet);
			setEntLists.Add(newEnt, new HashSet<Entity>());
			setParentLists.Add(newEnt, new HashSet<Entity>());
			return newEnt;
		}
	
		//------------------------------------------------------------
		public Entity addLinkSet(string id) {
			if (id == null || id == "") {
				Debug.LogError("Can't addLinkSet with id: " + id);
				return null;
			}
			Entity newEnt = addEntity(id, EntType.linkSet);
			setEntLists.Add(newEnt, new HashSet<Entity>());
			setParentLists.Add(newEnt, new HashSet<Entity>());
			return newEnt;
		}

		//------------------------------------------------------------
		public Entity addNodeType(string id) {
			if (id == null || id == "") {
				Debug.LogError("Can't addNodeType with id: " + id);
				return null;
			}
			Entity newEnt = addEntity(id, EntType.nodeType);
			typeEntLists.Add(newEnt, new HashSet<Entity>());
			typeParentLists.Add(newEnt, new HashSet<Entity>());
			return newEnt;
		}

		//------------------------------------------------------------
		public Entity addLinkType(string id) {
			if (id == null || id == "") {
				Debug.LogError("Can't addLinkType with id: " + id);
				return null;
			}
			Entity newEnt = addEntity(id, EntType.linkType);
			typeEntLists.Add(newEnt, new HashSet<Entity>());
			typeParentLists.Add(newEnt, new HashSet<Entity>());
			return newEnt;
		}

		//------------------------------------------------------------
		public Entity addLink(Entity type, Entity parent, Entity child) {
			// check params
			if (!exists(type) || !exists(parent) || !exists(child)) {
				Debug.LogError("Can't call addLink on invalid entities: " 
							   + type + "\n" + parent + "\n" + child);
				return null;
			}	
			if (type.type != EntType.linkType || parent.type != EntType.node
				|| child.type != EntType.node) {
				Debug.LogError("Must call addLink(type, node, node): " + 
							   type + "\n" + parent + "\n" + child);
				return null;
			}

			// check if link already exists
			string id = type.id + ":" + parent.id + ":" + child.id;
			Entity newLink;
			if (entityByString.TryGetValue(id, out newLink)) {
				Debug.LogError("Can't add link because already exists: " + id);
				return null;
			}

			newLink = new Entity(id, EntType.link);
			entityByString.Add(id, newLink);
			entities.Add(newLink);

			// add to linkTypeLists and typeEntLists
			entTypeLists.Add(newLink, new HashSet<Entity>{type});
			// add to typeEntLists
			HashSet<Entity> ents;
			if (!typeEntLists.TryGetValue(type, out ents)) {
				typeEntLists.Add(type, new HashSet<Entity>());
				typeEntLists.TryGetValue(type, out ents);
			}
			ents.Add(newLink);

			// Add new LinkType if necessary to nodeChildLinks, nodeParentLinks
			Dictionary<Entity, HashSet<Entity>> typeLinks;
			if (!nodeChildLinks.TryGetValue(type, out typeLinks)) {
				nodeChildLinks.Add(type, new Dictionary<Entity, HashSet<Entity>>());
				nodeParentLinks.Add(type, new Dictionary<Entity, HashSet<Entity>>());			
				nodeChildLinks.TryGetValue(type, out typeLinks);
			}

			// Add to nodeChildLinks
			HashSet<Entity> children;
			if (!typeLinks.TryGetValue(parent, out children)) {
				typeLinks.Add(parent, new HashSet<Entity>());
				typeLinks.TryGetValue(parent, out children);
			}
			children.Add(newLink);

			// Add to nodeParentLinks
			nodeParentLinks.TryGetValue(type, out typeLinks);
			HashSet<Entity> parents;
			if (!typeLinks.TryGetValue(child, out parents)) {
				typeLinks.Add(child, new HashSet<Entity>());
				typeLinks.TryGetValue(child, out parents);
			}
			parents.Add(newLink);

			// add parent and child
			linkChildren.Add(newLink, child);
			linkParents.Add(newLink, parent);

			return newLink;
		}

		//------------------------------------------------------------
		public void rm(Entity key) {
			if (!exists(key)) {
				Debug.LogError("Can't call rm on invalid entity: " + key);
				return;
			}
		
			// First remove fields
			Dictionary<string, Field> entFields;
			if (entityFields.TryGetValue(key, out entFields)) {
				List<Field> rmFields = new List<Field>();
				foreach (Field field in entFields.Values) {
					rmFields.Add(field);
				}
				foreach (Field rmField in rmFields) {
					rm(rmField);
				}
			}

			// Remove entities from set if set
			if (key.type == EntType.linkSet 
				|| key.type == EntType.nodeSet) {
				HashSet<Entity> entsInSet = setEnts(key);
				if (entsInSet != null) {
					foreach (Entity ent in entsInSet) {
						rmEntFromSet(ent, key);
					}
				}
				setEntLists.Remove(key);
			}
			// remove node from type
			else if (key.type == EntType.nodeType) {
				HashSet<Entity> entsInType = typeEnts(key);
				if (entsInType != null) {
					foreach (Entity ent in entsInType) {
						rmEntFromType(ent, key);
					}
				}
				typeEntLists.Remove(key);
			}
			// Links can only have one type so remove links in linkType
			else if (key.type == EntType.linkType) {
				HashSet<Entity> linksInType = typeEnts(key);
				if (linksInType != null) {
					foreach (Entity link in linksInType) {
						rm(link);
					}
				}
				typeEntLists.Remove(key);
			}

			// Remove from nodeType
			if (key.type == EntType.node || key.type == EntType.nodeSet) {
				HashSet<Entity> rmEntTypes = entTypes(key);
				if (rmEntTypes != null) {
					foreach (Entity rmEntType in rmEntTypes) {
						rmEntFromType(key, rmEntType);
					}
				}
			}

			// remove from sets
			if (key.type == EntType.node 
				|| key.type == EntType.link) {

				// Remove from sets
				HashSet<Entity> rmEntSets = entSets(key);
				if (rmEntSets != null) {
					foreach (Entity rmEntSet in rmEntSets) {
						rmEntFromSet(key, rmEntSet);
					}
				}
			}

			// Nodes
			if (key.type == EntType.node) {
				// Remove all links
				HashSet<Entity> childLinks = allChildLinks(key);
				if (childLinks != null) {
					foreach (Entity link in childLinks) {
						rm(link);
					}
				}
				HashSet<Entity> parentLinks = allParentLinks(key);
				if (parentLinks != null) {
					foreach (Entity link in parentLinks) {
						rm(link);
					}
				}
			}

			// Links
			if (key.type == EntType.link) {
				Entity type = linkType(key);
				Entity parent = linkParent(key);
				Entity child   = linkChild(key);

				// remove from nodeChildLinks
				Dictionary<Entity, HashSet<Entity>> typeLinks;
				nodeChildLinks.TryGetValue(type, out typeLinks);
				HashSet<Entity> children;
				typeLinks.TryGetValue(parent, out children);
				children.Remove(key);
				// remove empty containers
				if (children.Count == 0) {
					typeLinks.Remove(parent);
					if (typeLinks.Count == 0) {
						nodeChildLinks.Remove(type);
					}
				}
			
				// remove from nodeParentLinks
				nodeParentLinks.TryGetValue(type, out typeLinks);
				HashSet<Entity> parents;
				typeLinks.TryGetValue(child, out parents);
				parents.Remove(key);
				// remove empty containers
				if (parents.Count == 0) {
					typeLinks.Remove(child);
					if (typeLinks.Count == 0) {
						nodeParentLinks.Remove(type);
					}
				}

				// remove from linkParents, linkChildren
				linkChildren.Remove(key);
				linkParents.Remove(key);			

				// Remove from linkType
				entTypeLists.Remove(key);
				HashSet<Entity> ents;
				typeEntLists.TryGetValue(type, out ents);
				ents.Remove(key);
			}


			// Remove entity
			entityByString.Remove(key.id);
			entities.Remove(key);
			entityMasks.Remove(key);

		}

		//------------------------------------------------------------
		public void setMask(Entity key, DataMask newMask) {
			if (!exists(key)) {
				Debug.LogError("Can't call setMask on invalid entity: " + key);
				return;
			}
			if (newMask == null) {
				if (entityMasks.ContainsKey(key)) {
					entityMasks.Remove(key);
				}
			}
			else {
				if (!entityMasks.ContainsKey(key)) {
					entityMasks.Add(key, newMask);
				}
				else {
					entityMasks[key] = newMask;
				}
			}
		}
	}
}
