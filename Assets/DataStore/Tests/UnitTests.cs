using System;
using System.Text;
using System.Linq;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public class UnitTests : MonoBehaviour {
		public bool verbose;

		//------------------------------------------------------------
		public void runTests() {
			bool allTests = true;

			var gos = GetComponentsInChildren<MonoBehaviour>();
			UnitTest[] testScripts = (from a in gos where a.GetType().GetInterfaces().Any(k => k == typeof(UnitTest)) select (UnitTest)a).ToArray();

			foreach (UnitTest test in testScripts) {
				if (!test.runTests(verbose)) {
					allTests = false;
				}
			}
			
			if (allTests) {
				Debug.Log("All Tests passed!");
			}
			else {
				Debug.LogError("Not all tests passed!");
			}
		}
	}
}
