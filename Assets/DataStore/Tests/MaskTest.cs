using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public class MaskTest : MonoBehaviour, UnitTest {
		bool result = true;

		//------------------------------------------------------------
		public bool runTests(bool verbose) {
			// First create some layers and check they are created
			DataMask.setLayerName(0, "Test0");
			DataMask.setLayerName(1, "Test1");
			DataMask.setLayerName(2, "Test2");
			DataMask.setLayerName(3, "Test3");

			Tests.check("set/get layerName", "Test0", DataMask.getLayerName(0), ref result);
			Tests.check("set/get layerName", "Test1", DataMask.getLayerName(1), ref result);
			Tests.check("set/get layerName", "Test2", DataMask.getLayerName(2), ref result);
			Tests.check("set/get layerName", "Test3", DataMask.getLayerName(3), ref result);

			// Test different constructors
			DataMask dataMask = new DataMask();
			Tests.check("DataMask()", false, dataMask.isLayer("Test0"), ref result);
			Tests.check("DataMask()", false, dataMask.isLayer("Test1"), ref result);
			Tests.check("DataMask()", false, dataMask.isLayer("Test2"), ref result);
			Tests.check("DataMask()", false, dataMask.isLayer("Test3"), ref result);

			dataMask = new DataMask(Convert.ToInt64("0101", 2));
			Tests.check("DataMask(long)", true, dataMask.isLayer("Test0"), ref result);
			Tests.check("DataMask(long)", false, dataMask.isLayer("Test1"), ref result);
			Tests.check("DataMask(long)", true, dataMask.isLayer("Test2"), ref result);
			Tests.check("DataMask(long)", false, dataMask.isLayer("Test3"), ref result);

			dataMask = new DataMask("0011");
			Tests.check("DataMask(string)", true, dataMask.isLayer("Test0"), ref result);
			Tests.check("DataMask(string)", true, dataMask.isLayer("Test1"), ref result);
			Tests.check("DataMask(string)", false, dataMask.isLayer("Test2"), ref result);
			Tests.check("DataMask(string)", false, dataMask.isLayer("Test3"), ref result);

			dataMask = new DataMask(new List<int>(){0,0,1,1});
			Tests.check("DataMask(List<int>)", true, dataMask.isLayer("Test0"), ref result);
			Tests.check("DataMask(List<int>)", true, dataMask.isLayer("Test1"), ref result);
			Tests.check("DataMask(List<int>)", false, dataMask.isLayer("Test2"), ref result);
			Tests.check("DataMask(List<int>)", false, dataMask.isLayer("Test3"), ref result);

			dataMask = new DataMask(new List<string>(){"Test1", "Test3"});
			Tests.check("DataMask(List<string>)", false, dataMask.isLayer("Test0"), ref result);
			Tests.check("DataMask(List<string>)", true, dataMask.isLayer("Test1"), ref result);
			Tests.check("DataMask(List<string>)", false, dataMask.isLayer("Test2"), ref result);
			Tests.check("DataMask(List<string>)", true, dataMask.isLayer("Test3"), ref result);

			// check visible
			Tests.check("visible", true,  dataMask.visible(new DataMask("1111")), ref result);
			Tests.check("visible", false, dataMask.visible(new DataMask("0000")), ref result);
			Tests.check("visible", false,  dataMask.visible(new DataMask("0001")), ref result);
			Tests.check("visible", true,  dataMask.visible(new DataMask("0010")), ref result);
			Tests.check("visible", false,  dataMask.visible(new DataMask("0100")), ref result);
			Tests.check("visible", true,  dataMask.visible(new DataMask("1000")), ref result);
			Tests.check("visible", false,  dataMask.visible(new DataMask("0101")), ref result);
			Tests.check("visible", true, dataMask.visible(new DataMask("1010")), ref result);

			// test combine
			DataMask mask2 = dataMask.combine(new DataMask(Convert.ToInt64("1100", 2)));
			Tests.check("combine", false, mask2.isLayer("Test0"), ref result);
			Tests.check("combine", true, mask2.isLayer("Test1"), ref result);
			Tests.check("combine", true, mask2.isLayer("Test2"), ref result);
			Tests.check("combine", true, mask2.isLayer("Test3"), ref result);
		
			// test binary string
			mask2 = new DataMask("01010");
			Tests.check("binary", mask2.binary(), "1010", ref result);
		
			return result;
		}
                        
	}
}
