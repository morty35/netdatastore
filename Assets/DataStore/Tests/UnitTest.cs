﻿using UnityEngine;
using System.Collections;

namespace DS {

	public interface UnitTest  {

		bool runTests(bool verbose);
	}
}
