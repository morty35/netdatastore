﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {
	public static class Tests {

		//------------------------------------------------------------
		public static void check<T>(string testName, T testValue, T correctValue, ref bool result) {
			bool testIsNull = EqualityComparer<T>.Default.Equals(testValue, default(T));
			bool correctIsNull = EqualityComparer<T>.Default.Equals(correctValue, default(T));

			if (testIsNull && correctIsNull) {
				return;
			}
			if ((correctIsNull && !testIsNull) 
				|| (!correctIsNull && testIsNull) 
				|| !correctValue.Equals(testValue)) {
				Debug.LogError(testName + " failed - test:" + testValue 
							   + " not equal to correct:" + correctValue);
				result = false;
			}
		}

		//------------------------------------------------------------
		public static void compareLists<T> (string testName, List<T> test, List<T> correct, ref bool result) {
			if (test == null && correct == null) {
				return;
			}
			if ((test == null && correct != null) 
				|| (test != null && correct == null)) {
				Debug.LogError(testName + "failed - one list is null and the other isn't: " + correct + " " + test);
				result = false;
				return;
			}
			if (correct.Count != test.Count) {
				Debug.LogError(testName + " failed - lists are not same length!");
				result = false;
			}
			for (int i = 0; i < correct.Count; i++) {
				if (!correct[i].Equals(test[i])) {
					Debug.LogError(testName + " failed - " + test[i] + " not equal to " + correct[i]);
					result = false;
				}
			}
		}
		//------------------------------------------------------------
		public static void compareSets<T>(string testName, ICollection<T> test, ICollection<T> correct, ref bool result) {
			if (test == null && correct == null) {
				return;
			}
			if ((test == null && correct != null) 
				|| (test != null && correct == null)) {
				Debug.LogError(testName + "failed - one set is null and the other isn't: " + correct + " " + test);
				result = false;
				return;
			}
			if (correct.Count != test.Count) {
				Debug.LogError(testName + " failed - lists are not same length! " + correct.Count + " " + test.Count );
				result = false;
				return;
			}
			foreach (T correctItem in correct) {
				bool found = false;
				foreach (T testItem in test) {
					if (correctItem.Equals(testItem)) {
						found = true;
					}
				}
				if (!found) {
					Debug.LogError(testName + " failed - " + correctItem + " not found! ");
					result = false;
					return;
				}
			}
		}
	}
}
