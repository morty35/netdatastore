using UnityEngine;
using System.Collections;

namespace DS {

    public class BoltStart : Bolt.GlobalEventListener {

		public string sceneName;
		public string endPoint = "127.0.0.1:27000";

		//------------------------------------------------------------
		public void StartServer() {
			// START SERVER
			BoltLauncher.StartServer(UdpKit.UdpEndPoint.Parse(endPoint));
		}

		//------------------------------------------------------------
		public void StartClient() {
			BoltLauncher.StartClient();
		}

		//------------------------------------------------------------
		public override void BoltStartDone() {
			if (BoltNetwork.isServer) {
				BoltNetwork.LoadScene(sceneName);
			}
			else {
				BoltNetwork.Connect(UdpKit.UdpEndPoint.Parse(endPoint));
			}
		}
		
	}

}
