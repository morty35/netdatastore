﻿using UnityEngine;
using System.Collections;

namespace DS {
	public class DataStoreTest : MonoBehaviour, UnitTest {
		bool result = true;
		
		public DataStore data;
		
		//------------------------------------------------------------
		public bool runTests(bool verbose) {
			data = new DataStore();
			Data.master = data;
			result = DataSetTest.testDataSet(verbose, data);
			return result;
		}
	}
}

