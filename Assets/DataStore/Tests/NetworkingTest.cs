﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* To test need to connect multiple machines running same Test
   Make sure to run test on master server first, adn then wait long
   enough to give time to network the data.  Then run tests on each 
   client machine to make sure all pass.
   */

namespace DS {
	public class NetworkingTest : MonoBehaviour, UnitTest,
		DataStringListener, DataListener {

		[HideInInspector]
		public bool result = true;
		public string startData;

		public DataNetStore ds;

		//------------------------------------------------------------
		public void Awake() {
			ds.init();
			Data.master = ds;

			// read DataFile
			DataLoader.readDataFile("Tests/" + startData, ds);
			
			ds.startSyncing();
			BoltConsole.Write("start Syncing!", Color.green);
		}

		//------------------------------------------------------------
		// On server it will receive client data and then transmit changes to client
		public void stringReceived(string dataString, BoltConnection connection) {
			DataLoader.readDataString(dataString, ds);
			BoltConsole.Write("string received", Color.green);
		}

		//------------------------------------------------------------
		public void notify(DataChange change) {
			Debug.Log("received change: " + change.type);
			if (change.type == ChangeType.addEnt
				&& change.ent == "commander") {
				Debug.Log("Added commander!");
			}
		}
	
		//------------------------------------------------------------
		public bool runTests(bool verbose) {
			
			// if server run DataSet core test to do dataset operations
			// and transmit them
			if (BoltNetwork.isServer) {
				ds.stringListener = this;
				Entity ship  			= ds["ship"];
				ds.addField<int>(ship, "damage", 0);
				Entity fleet1			= ds["fleet1"];
				Entity fleet2			= ds["fleet2"];
								
				// Create some ents and assign types
				Entity ship01			= ds.addNode("ship01");
				ds.addEntToType(ship01, ship);
				ds.edit<int>(ship01, "damage", 1);
				Entity ship02			= ds.addNode("ship02");
				ds.addEntToType(ship02, ship);
				ds.edit<int>(ship02, "damage", 2);
				Entity ship03			= ds.addNode("ship03");
				ds.addEntToType(ship03, ship);
				ds.edit<int>(ship03, "damage", 3);
				Entity ship04			= ds.addNode("ship04");
				ds.addEntToType(ship04, ship);
				ds.edit<int>(ship04, "damage", 4);

				ds.addEntToSet(ship01, fleet1);
				ds.addEntToSet(ship02, fleet1);
				ds.addEntToSet(ship03, fleet2);
				ds.addEntToSet(ship04, fleet2);

				return result;
			}
			// Run tests that data correctly synced
			else {
				string group = "Networking test ";
				int i = 0;
				Tests.check(group + i++, true, ds.exists("ship01"), ref result);
				Tests.check(group + i++, true, ds.exists("ship02"), ref result);
				Tests.check(group + i++, true, ds.exists("ship03"), ref result);
				Tests.check(group + i++, true, ds.exists("ship04"), ref result);
				Tests.check(group + i++, ds.data<int>("ship", "damage"), 0, ref result);
				Tests.check(group + i++, ds.data<int>("ship01", "damage"), 1, ref result);
				Tests.check(group + i++, ds.data<int>("ship02", "damage"), 2, ref result);
				Tests.check(group + i++, ds.data<int>("ship03", "damage"), 3, ref result);
				Tests.check(group + i++, ds.data<int>("ship04", "damage"), 4, ref result);				

				Tests.compareSets<Entity>(group+i++, ds.typeEnts(ds["ship"]), 
										  new List<Entity>{ds["ship01"], ds["ship02"], ds["ship03"], ds["ship04"]}, ref result);
				Tests.compareSets<Entity>(group+i++, ds.setEnts(ds["fleet1"]), 
										  new List<Entity>{ds["ship01"], ds["ship02"]}, ref result);
				Tests.compareSets<Entity>(group+i++, ds.setEnts(ds["fleet2"]), 
										  new List<Entity>{ds["ship03"], ds["ship04"]}, ref result);


				// Add self as listener
				DataFilter filter = new DataFilter("networkTest");
				DataFilterItem item = new DataFilterItem();
				item.type = FilterType.include;
				item.mode = FilterMode.all;
				filter.filters.Add(item);
				ds.addListener(this, filter);

				// send up data file
				BoltConsole.Write("writeDataString", Color.green);
				DataSet ds2 = new DataStore();
				ds2.addNode("commander");
				string dataString = DataLoader.writeDataString(ds2);
				ds.sendDataStringToServer(dataString);
				
				return result;
			}
		}
	}
}
