using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public static class DataSetTest {

		//------------------------------------------------------------
		public static bool testDataSet(bool verbose, DataSet data, bool maskTests=true) {
			bool result = true;
		
			// test entity functionality
			Entity ship01			= data.addNode("ship01");
			Entity laser01			= data.addNode("laser01");
			Entity ship  			= data.addNodeType("ship");
			Entity weapon    		= data.addNodeType("weapon");
			Entity fleet1			= data.addNodeSet("fleet1");
			Entity energyLink		= data.addLinkSet("energyLink");
			Entity weaponLink   	= data.addLinkType("weaponLink");
			Entity shipLaser		= data.addLink(weaponLink, ship01, laser01);
			Entity cloneNode        = data.cloneNode(ship01, "cloneShip");
		
			// test entities exist as expected
			string group = "add entity ";
			int i = 0;
			Tests.check(group + i++, EntType.node, data["ship01"].type, ref result);
			Tests.check(group + i++, ship01, data["ship01"], ref result);
			Tests.check(group + i++, true, data.exists(ship01), ref result);

			Tests.check(group + i++, EntType.node, data["laser01"].type, ref result);
			Tests.check(group + i++, laser01, data.ent("laser01"), ref result);
			Tests.check(group + i++, true, data.exists(laser01), ref result);

			Tests.check(group + i++, EntType.nodeType, data["ship"].type, ref result);
			Tests.check(group + i++, ship, data["ship"], ref result);
			Tests.check(group + i++, true, data.exists("ship"), ref result);

			Tests.check(group + i++, EntType.nodeType, data["weapon"].type, ref result);
			Tests.check(group + i++, weapon, data["weapon"], ref result);
			Tests.check(group + i++, true, data.exists(weapon), ref result);

			Tests.check(group + i++, EntType.nodeSet, data["fleet1"].type, ref result);
			Tests.check(group + i++, fleet1, data["fleet1"], ref result);
			Tests.check(group + i++, true, data.exists(fleet1), ref result);

			Tests.check(group + i++, EntType.linkSet, data["energyLink"].type, ref result);
			Tests.check(group + i++, energyLink, data["energyLink"], ref result);
			Tests.check(group + i++, true, data.exists(energyLink), ref result);

			Tests.check(group + i++, EntType.linkType, data["weaponLink"].type, ref result);
			Tests.check(group + i++, weaponLink, data["weaponLink"], ref result);
			Tests.check(group + i++, true, data.exists(weaponLink), ref result);

			Tests.check(group + i++, EntType.link, data["weaponLink:ship01:laser01"].type, ref result);
			Tests.check(group + i++, shipLaser, data["weaponLink:ship01:laser01"], ref result);
			Tests.check(group + i++, true, data.exists(shipLaser), ref result);
			Tests.check(group + i++, true, data.exists(cloneNode), ref result);
			Tests.check(group + i++, "cloneShip", cloneNode.id, ref result);

			Tests.check(group + i++, false, data.exists(new Entity("nonexistent", EntType.node)), ref result);
			Tests.check(group + i++, false, data.exists(new Entity("nonexistent", EntType.link)), ref result);
			Tests.check(group + i++, false, data.exists(new Entity("nonexistent", EntType.nodeType)), ref result);
			Tests.check(group + i++, false, data.exists(new Entity("nonexistent", EntType.nodeSet)), ref result);
			Tests.check(group + i++, false, data.exists(new Entity("nonexistent", EntType.linkType)), ref result);
			Tests.check(group + i++, false, data.exists(new Entity("nonexistent", EntType.linkSet)), ref result);

			// test link methods
			group = "link methods ";
			i = 0;
			data.rm(cloneNode);
			Entity ship02 = data.addNode("ship02");
			Entity ship03 = data.addNode("ship03");
			Entity commandLink = data.addLinkType("commandLink");
			Entity ship1CommandLink = data.addLink(commandLink, ship01, ship02);
			data.addLink(commandLink, ship02, ship03);
			Entity ship2Laser = data.addLink(weaponLink, ship02, laser01);

			Tests.check(group + i++, shipLaser, data.link(weaponLink,ship01,laser01), ref result);
			Tests.check(group + i++, ship01, data.linkParent(shipLaser), ref result);
			Tests.check(group + i++, laser01, data.linkChild(shipLaser), ref result);
			Tests.compareSets<Entity>(group + i++, data.childLinks(ship01, commandLink), 
									 new List<Entity>{ship1CommandLink}, ref result);
			Tests.compareSets<Entity>(group + i++, data.allChildLinks(ship01), 
									 new List<Entity>{ship1CommandLink, shipLaser}, ref result);
			Tests.compareSets<Entity>(group + i++, data.parentLinks(ship02, commandLink), 
									 new List<Entity>{ship1CommandLink}, ref result);
			Tests.compareSets<Entity>(group + i++, data.allParentLinks(laser01), 
									 new List<Entity>{shipLaser, ship2Laser}, ref result);
		
			// test node methods
			group = "node methods ";
			i = 0;
			Tests.compareSets<Entity>(group, data.children(ship01, commandLink), 
									 new List<Entity>{ship02}, ref result);
			Tests.compareSets<Entity>(group+i++, data.allChildren(ship01), 
									 new List<Entity>{ship02, laser01}, ref result);
			Tests.compareSets<Entity>(group+i++, data.parents(ship02, commandLink), 
									 new List<Entity>{ship01}, ref result);
			Tests.compareSets<Entity>(group+i++, data.allParents(laser01), 
									 new List<Entity>{ship01, ship02}, ref result);
			Tests.compareSets<Entity>(group+i++, data.descendents(ship01, commandLink), 
									 new List<Entity>{ship02, ship03}, ref result);
			Tests.compareSets<Entity>(group+i++, data.ancestors(ship03, commandLink), 
									 new List<Entity>{ship02, ship01}, ref result);
			Tests.compareSets<Entity>(group+i++, data.allNodes(),
									 new List<Entity>{ship01, ship02, ship03, laser01}, ref result);

			// test type methods
			data.addEntToType(ship01, ship);
			data.addEntToType(ship02, ship);
			Entity cruiser = data.addNodeType("cruiser");
			data.addEntToType(ship01, cruiser);

			group = "type methods ";
			i = 0;

			Tests.check(group + i++, data.isEntType(shipLaser, weaponLink), true, ref result);
			Tests.check(group + i++, data.isEntType(shipLaser, commandLink), false, ref result);
			Tests.check(group + i++, data.isEntType(ship01, ship), true, ref result);
			Tests.compareSets<Entity>(group+i++, data.entTypes(ship01), 
									 new List<Entity>{cruiser, ship}, ref result);
			Tests.check(group + i++, data.linkType(shipLaser), weaponLink, ref result);
			Tests.compareSets<Entity>(group+i++, data.typeEnts(ship), 
									 new List<Entity>{ship01, ship02}, ref result);
			Tests.compareSets<Entity>(group+i++, data.typeEnts(weaponLink), 
									 new List<Entity>{shipLaser, ship2Laser}, ref result);
			Tests.compareSets<Entity>(group+i++, data.allLinkTypes(), 
									 new List<Entity>{weaponLink, commandLink}, ref result);
			Tests.compareSets<Entity>(group+i++, data.allNodeTypes(), 
									 new List<Entity>{weapon, ship, cruiser}, ref result);
			data.rmEntFromType(ship03, ship);
			Tests.compareSets<Entity>(group+i++, data.entTypes(ship03), 
									 new List<Entity>(), ref result);
			// test set methods
			Entity fleet2 = data.addNodeSet("fleet2");		
			Entity energyWeapons = data.addNodeSet("energyWeapons");
			Entity missileLink = data.addLinkSet("missileLink");
			data.addEntToSet(shipLaser, energyLink);
			data.addEntToSet(ship01, fleet1);
			data.addEntToSet(ship01, fleet2);
			data.addEntToSet(ship02, fleet2);
			data.addEntToSet(ship03, fleet2);
			data.addEntToSet(laser01, energyWeapons);

			group = "set methods ";
			i = 0;
			Tests.check(group + i++, data.isEntSet(shipLaser, energyLink), true, ref result);
			Tests.check(group + i++, data.isEntSet(shipLaser, missileLink), false, ref result);
			Tests.check(group + i++, data.isEntSet(ship01, fleet1), true, ref result);
			Tests.compareSets<Entity>(group+i++, data.entSets(ship01), 
									 new List<Entity>{fleet1, fleet2}, ref result);
			Tests.compareSets<Entity>(group+i++, data.entSets(shipLaser), 
									 new List<Entity>{energyLink, }, ref result);
			Tests.compareSets<Entity>(group+i++, data.setEnts(fleet2), 
									 new List<Entity>{ship01, ship02, ship03}, ref result);
			Tests.compareSets<Entity>(group+i++, data.setEnts(energyLink), 
									 new List<Entity>{shipLaser}, ref result);
			Tests.compareSets<Entity>(group+i++, data.allLinkSets(), 
									 new List<Entity>{energyLink, missileLink}, ref result);
			Tests.compareSets<Entity>(group+i++, data.allNodeSets(), 
									 new List<Entity>{fleet1, fleet2, energyWeapons}, ref result);
			data.rmEntFromSet(ship01, fleet2);
			Tests.compareSets<Entity>(group+i++, data.entSets(ship01), 
									 new List<Entity>{fleet1}, ref result);

			// test fields
			Field damage = data.addField<int>(ship01, "damage", 0);
			Field name = data.addField<string>(ship01, "name", "Invincible");
			data.addField<int>(ship, "speed", 30);
			Field speed = new Field(ship01, "speed", FieldType.single, DataType.intType);

			group = "single field methods ";
			i = 0;
			// single 
			Tests.check(group + i++, data.data<int>(damage), 0, ref result);
			Tests.check(group + i++, data.data<int>(speed), 30, ref result);
			Tests.check(group + i++, data.data<int>("ship01", "damage"), 0, ref result);
			Tests.check(group + i++, data.data<int>("ship01", "speed"), 30, ref result);
			Tests.check(group + i++, data.data<string>("ship01", "name"), "Invincible", ref result);
			Tests.check(group + i++, data.field(ship01, "damage"), damage, ref result);

			Tests.check(group + i++, data.field(ship01, "speed"), speed, ref result);
			Tests.compareSets<Field>(group+i++, data.fields(ship01), 
									new List<Field>{damage, name, speed}, ref result);
			Tests.check(group + i++, data.exists(new Field(ship01, "range", FieldType.single, DataType.intType)), false, ref result);
			Tests.check(group + i++, data.exists("ship01", "name"), true, ref result);
			Tests.check(group + i++, data.exists(speed), true, ref result);
			Tests.check(group + i++, data.exists(speed, true), false, ref result);
			Tests.compareSets<Field>(group+i++, data.fields(ship01, true), 
									new List<Field>{name, damage}, ref result);
			data.edit<int>(damage, 3);
			data.edit<int>(speed, 20);
			Tests.check(group + i++, data.data<int>("ship01", "speed"), 20, ref result);
			Tests.check(group + i++, data.exists(speed, true), true, ref result);
			Tests.check(group + i++, data.data<int>(damage), 3, ref result);		
			data.rm(damage);
			data.rm(speed);
			Tests.check(group + i++, data.exists(speed), true, ref result);
			Tests.check(group + i++, data.exists(speed, true), false, ref result);
			Tests.compareSets<Field>(group+i++, data.fields(ship01), 
									new List<Field>{name, speed}, ref result);

			// list field methods
			Field commandList = data.addFieldList<Entity>(ship01, "commandList");
			data.addToDataList<Entity>(commandList, ship02);
			data.addToDataList<Entity>(commandList, ship03);
			Field shipFleet = data.addFieldList<Entity>(ship, "fleetList");
			data.addToDataList<Entity>(shipFleet, ship01);
			data.addToDataList<Entity>(shipFleet, ship02);
			data.addToDataList<Entity>(shipFleet, ship03);
			Field fleetList = new Field(ship01, "fleetList", FieldType.list, DataType.entityType);

			group = "list field methods ";
			i = 0;
			Tests.check(group + i++, data.data<Entity>(commandList, 0), ship02, ref result);
			Tests.check(group + i++, data.data<Entity>(fleetList, 1), ship02, ref result);
			Tests.check(group + i++, data.data<Entity>("ship01", "commandList", 1), ship03, ref result);
			Tests.check(group + i++, data.data<Entity>("ship01", "fleetList", 2), ship03, ref result);
			Tests.compareLists(group + i++, data.dataList<Entity>(commandList), 
							  new List<Entity>{ship02, ship03}, ref result);
			Tests.compareLists(group + i++, data.dataList<Entity>(fleetList), 
							  new List<Entity>{ship01, ship02, ship03}, ref result);
			Tests.check(group + i++, data.exists(new Field(ship01, "range", FieldType.list, DataType.intType)), false, ref result);
			Tests.check(group + i++, data.exists(fleetList), true, ref result);
			Tests.check(group + i++, data.exists(fleetList, true), false, ref result);
			Tests.compareSets<Field>(group+i++, data.fields(ship01), 
									new List<Field>{name, fleetList, commandList, speed}, ref result);
			Tests.compareSets<Field>(group+i++, data.fields(ship01,true), 
									new List<Field>{name, commandList}, ref result);
			data.insertDataList<Entity>(commandList, 0, ship01);
			data.insertDataList<Entity>(fleetList, 0, ship01);
			Tests.check(group + i++, data.exists(fleetList, true), true, ref result);		
			Tests.check(group + i++, data.data<Entity>(commandList,1), ship02, ref result);
			Tests.check(group + i++, data.data<Entity>(fleetList,1), ship01, ref result);
			data.addToDataList<Entity>(commandList, ship01);
			Tests.compareLists(group + i++, data.dataList<Entity>(commandList), 
							  new List<Entity>{ship01, ship02, ship03, ship01}, ref result);
			Tests.check(group + i++, data.listCount(commandList), 4, ref result);
			data.rm(commandList, 3);
			Tests.compareLists(group + i++, data.dataList<Entity>(commandList), 
							  new List<Entity>{ship01, ship02, ship03}, ref result);
			Tests.check(group + i++, data.field(ship01, "commandList"), commandList, ref result);
			Tests.compareSets<Field>(group+i++, data.fields(ship01), 
									new List<Field>{name, commandList,speed, fleetList}, ref result);
			Tests.check(group + i++, data.exists(new Field(ship01, "range", FieldType.list, DataType.intType)), false, ref result);
			Tests.check(group + i++, data.exists(commandList), true, ref result);
			data.edit<Entity>(commandList, 2, ship01);
			Tests.compareLists(group + i++, data.dataList<Entity>(commandList), 
							  new List<Entity>{ship01, ship02, ship01}, ref result);
			data.rm(commandList);
			data.rm(fleetList);
			Tests.compareSets<Field>(group+i++, data.fields(ship01,true), 
									new List<Field>{name}, ref result);

			if (maskTests) {
				// test DataMask
				group = "mask methods ";
				i = 0;
				Tests.check(group + i++, data.mask(laser01), new DataMask(), ref result);
				data.setMask(energyWeapons, new DataMask("0001"));
				Tests.check(group + i++, data.mask(laser01), new DataMask("0001"), ref result);
				Tests.check(group + i++, data.mask(laser01, true), null, ref result);
				data.setMask(fleet1, new DataMask("0010"));
				Tests.check(group + i++, data.mask(name), new DataMask("0010"), ref result);		
				data.setMask(ship01, new DataMask("0001"));
				Tests.check(group + i++, data.mask(ship01), new DataMask("0011"), ref result);		
				Tests.check(group + i++, data.mask(name, true), null, ref result);		
				data.setMask(name, new DataMask("1000"));
				Tests.check(group + i++, data.mask(name), new DataMask("1000"), ref result);
			}

			// test remove
			group = "rm method ";
			i = 0;
			// node
			data.rm(ship01);
			Tests.check(group + i++, data.exists(ship01), false, ref result);
			Tests.check(group + i++, data.exists(shipLaser), false, ref result);
			Tests.check(group + i++, data.exists(ship1CommandLink), false, ref result);
			Tests.check(group + i++, data.exists(ship1CommandLink), false, ref result);
			Tests.compareSets<Entity>(group+i++, data.setEnts(fleet1), 
									 new List<Entity>(), ref result);
			Tests.compareSets<Entity>(group+i++, data.typeEnts(ship), 
									 new List<Entity>{ship02}, ref result);
			Tests.compareSets<Entity>(group+i++, data.typeEnts(cruiser), 
									 new List<Entity>(), ref result);
			Tests.check(group + i++, data.exists(name), false, ref result);
			// nodeType
			data.rm(ship);
			Tests.compareSets<Entity>(group+i++, data.entTypes(ship02), 
									 new List<Entity>{}, ref result);
			// nodeSet
			data.rm(fleet2);
			Tests.compareSets<Entity>(group+i++, data.entSets(ship02),
									 new List<Entity>{}, ref result);
			// link
			data.rm(ship2Laser);
			Tests.compareSets<Entity>(group+i++, data.allChildren(ship02),
									 new List<Entity>{ship03}, ref result);
			Tests.compareSets<Entity>(group+i++, data.allParents(laser01),
									 new List<Entity>{}, ref result);
			Tests.compareSets<Entity>(group+i++, data.typeEnts(weaponLink),
									 new List<Entity>{}, ref result);
			// linkType
			data.rm(commandLink);
			Tests.compareSets<Entity>(group+i++, data.allLinkTypes(),
									 new List<Entity>{weaponLink}, ref result);
			// linkSet
			data.rm(energyLink);
			Tests.compareSets<Entity>(group+i++, data.allLinkSets(),
									 new List<Entity>{missileLink}, ref result);

			Data.master = data;

			return result;
		}
	}
}
