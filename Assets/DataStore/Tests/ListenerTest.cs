﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public class ListenerTest : MonoBehaviour, UnitTest {
		private bool result = true;
		public DataNetStore data;
		
		//****************************************
		public class Listener1 : DataListener {
			public DataSet data;
			public int numShips = 0;
			public int totalDamage = 0;

			public Listener1(DataSet data) {
				this.data = data;
			}
			
			public void notify(DataChange dc) {
				switch(dc.type) {
				case ChangeType.addToGroup:
					numShips++;
					totalDamage += data.data<int>(dc.ent, "damage");
					break;
				case ChangeType.rmEnt:
					numShips--;
					totalDamage -= data.data<int>(dc.ent, "damage");
					break;
				case ChangeType.edit:
				case ChangeType.addField:
					if (data.exists(dc.ent, "damage")) {
						totalDamage -= data.data<int>(dc.ent, "damage");
					}
					totalDamage += DataUtil.toInt(dc.dataString);
					break;
				case ChangeType.rmField:
					totalDamage -= data.data<int>(dc.ent, "damage");
					if (data.exists("ship", "damage")) {
						totalDamage += data.data<int>("ship", "damage");
					}
					break;
				}
			}
		}
		//****************************************

		//------------------------------------------------------------
		public bool runTests(bool verbose) {

			data.init();
			Data.master = data;

			Listener1 shipListener = new Listener1(data);
			DataFilter shipFilter = new DataFilter("ships");
			DataFilterItem newItem = new DataFilterItem();
			newItem.type = FilterType.include;
			newItem.mode = FilterMode.group;
			newItem.ents.Add("ship");
			shipFilter.filters.Add(newItem);


			// Add ships before filter
			Entity ship  			= data.addNodeType("ship");
			data.addField<int>(ship, "damage", 3);
			data.addListener(shipListener, shipFilter);
			Entity ship01			= data.addNode("ship01");
			data.addEntToType(ship01, ship);
			Entity ship02			= data.addNode("ship02");
			data.addField<int>(ship02, "damage", 2);
			data.addEntToType(ship02, ship);

			// Check number of ships is 2 and damage is 5
			string group = "shipListener";
			int i = 0;
			Tests.check(group + i++, shipListener.numShips, 2, ref result);
			Tests.check(group + i++, shipListener.totalDamage, 5, ref result);

			// remove ship
			data.rm(ship01);
			Tests.check(group + i++, shipListener.numShips, 1, ref result);
			Tests.check(group + i++, shipListener.totalDamage, 2, ref result);

			// edit damage
			data.edit<int>(ship02, "damage", 8);
			Tests.check(group + i++, shipListener.numShips, 1, ref result);
			Tests.check(group + i++, shipListener.totalDamage, 8, ref result);
			
			// remove field
			data.rm(ship02, "damage");
			Tests.check(group + i++, shipListener.numShips, 1, ref result);
			Tests.check(group + i++, shipListener.totalDamage, 3, ref result);
			
			return result;
		}
                        
	}
}

