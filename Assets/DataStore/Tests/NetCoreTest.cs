using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public class NetCoreTest : MonoBehaviour, UnitTest {
		bool result = true;

		public DataNetStore data;

		//------------------------------------------------------------
		public bool runTests(bool verbose) {
			data.init();
			Data.master = data;
			result = DataSetTest.testDataSet(verbose, data, false);
			return result;
		}
	}
}
