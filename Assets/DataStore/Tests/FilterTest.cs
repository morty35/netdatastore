using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public class FilterTest : MonoBehaviour, UnitTest {
		bool result = true;

		//------------------------------------------------------------
		public bool runTests(bool verbose) {

			DataStore data = new DataStore();
			DataLoader.readDataFile("Tests/test1", data);

			// Create new DataFilter
			DataFilter newFilter = new DataFilter("filter1");

			// nodeTypes
			DataFilterItem newItem = new DataFilterItem();
			newItem.type = FilterType.include;
			newItem.mode = FilterMode.allNodeTypes;
			newFilter.filters.Add(newItem);

			// remove nodeSets
			newItem = new DataFilterItem();
			newItem.type = FilterType.exclude;
			newItem.mode = FilterMode.allNodeSets;
			newFilter.filters.Add(newItem);

			// add nodes
			newItem = new DataFilterItem();
			newItem.type = FilterType.include;
			newItem.mode = FilterMode.allNodes;
			newFilter.filters.Add(newItem);

			// add commandLink
			newItem = new DataFilterItem();
			newItem.type = FilterType.include;
			newItem.mode = FilterMode.ent;
			newItem.ents.Add("commandLink");
			newFilter.filters.Add(newItem);

			// remove link
			newItem = new DataFilterItem();
			newItem.type = FilterType.exclude;
			newItem.mode = FilterMode.ent;
			newItem.ents.Add("commandLink:ship02:ship03");
			newFilter.filters.Add(newItem);

			// add links
			newItem = new DataFilterItem();
			newItem.type = FilterType.include;
			newItem.mode = FilterMode.allLinks;
			newFilter.filters.Add(newItem);

			DataLoader.writeDataFile("Tests/testFilter", data);

			DataStore data2 = new DataStore();
			DataLoader.readDataFile("Tests/testFilter", data2);

			// Test results of filter
			HashSet<Entity> startList = data2.allEnts();
			startList.Remove(data2["fleet2"]);
			startList.Remove(data2["captainLinks"]);
			startList.Remove(data2["ship"]);		
			
			HashSet<Entity> matches = newFilter.matches(startList, data2);
			HashSet<Entity> correct = new HashSet<Entity> {
				data2["capital"], data2["cruiser"],
				data2["ship01"], data2["ship02"],
				data2["ship03"], data2["commandLink"],
				data2["commandLink:ship01:ship02"],
				data2["commandLink:ship01:ship03"]
			};
			Tests.compareSets<Entity>("DataFilter matches", matches, correct, ref result);

			return result;
		}
                        
	}
}
