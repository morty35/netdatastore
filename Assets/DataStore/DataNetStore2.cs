﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public partial class DataNetStore : Bolt.GlobalEventListener {

		//========================================
		// Entity
		//========================================

		//------------------------------------------------------------
		public Entity this[string id] {
			get {
				return ds[id];
			}
		}

		//------------------------------------------------------------
		public void clearAll() {
			DataChange dc = new DataChange();
			dc.type = ChangeType.clearAll;
			transmitChange(dc);

			ds.clearAll();
			foreach (DataListener listener in listeners.Keys) {
				listener.notify(dc);
			}
		}

		//------------------------------------------------------------
		public HashSet<Entity> allEnts() {
			return ds.allEnts();
		}

		//------------------------------------------------------------
		public Entity ent(string id) {
			return ds.ent(id);
		}

		//------------------------------------------------------------
		public DataMask mask(Entity key, bool specific=false) {
			return ds.mask(key, specific);
		}

		//------------------------------------------------------------
		public bool exists(Entity key) {
			return ds.exists(key);
		}

		//------------------------------------------------------------
		public bool exists(string id) {
			return ds.exists(id);
		}

		//------------------------------------------------------------
		public Entity addNode(string id) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addEnt;
			dc.entType = EntType.node;
			dc.ent = id;
			transmitChange(dc);

			Entity newEnt = ds.addNode(id);
			updateListenersNewEnt(newEnt, dc);
			return newEnt;
		}

		//------------------------------------------------------------
		public Entity addLink(Entity type, Entity parent, Entity child) {
			if (type == null || parent == null || child == null) {
				Debug.LogError("addLink invalid params: \n" +
							   type + "\n" + parent + "\n" + child);
				return null;
			}
			DataChange dc = new DataChange();
			dc.type = ChangeType.addLink;
			dc.parent = parent.id;
			dc.child = child.id;
			dc.linkType = type.id;
			transmitChange(dc);

			Entity newEnt = ds.addLink(type, parent, child);
			updateListenersNewEnt(newEnt, dc);
			return newEnt;
		}

		//------------------------------------------------------------
		public Entity addNodeSet(string id) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addEnt;
			dc.entType = EntType.nodeSet;
			dc.ent = id;
			transmitChange(dc);

			Entity newEnt = ds.addNodeSet(id);
			updateListenersNewEnt(newEnt, dc);
			return newEnt;
		}

		//------------------------------------------------------------
		public Entity addLinkSet(string id) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addEnt;
			dc.entType = EntType.linkSet;
			dc.ent = id;
			transmitChange(dc);

			Entity newEnt = ds.addLinkSet(id);
			updateListenersNewEnt(newEnt, dc);
			return newEnt;
		}

		//------------------------------------------------------------
		public Entity addNodeType(string id) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addEnt;
			dc.entType = EntType.nodeType;
			dc.ent = id;
			transmitChange(dc);

			Entity newEnt = ds.addNodeType(id);
			updateListenersNewEnt(newEnt, dc);
			return newEnt;
		}

		//------------------------------------------------------------
		public Entity addLinkType(string id) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addEnt;
			dc.entType = EntType.linkType;
			dc.ent = id;
			transmitChange(dc);

			Entity newEnt = ds.addLinkType(id);
			updateListenersNewEnt(newEnt, dc);
			return newEnt;
		}

		//------------------------------------------------------------
		public void rm(Entity key) {
			if (key == null) {
				Debug.LogError("Rm called with invalid params: " + key);
				return;
			}
			DataChange dc = new DataChange();
			dc.type = ChangeType.rmEnt;
			dc.ent = key.id;
			transmitChange(dc);

			notifyListeners(dc, key);
			ds.rm(key);
		}

		//------------------------------------------------------------
		public void setMask(Entity key, DataMask newMask) {
			if (newMask != null) {
				Debug.LogError("networking masks not implemented");
			}
			ds.setMask(key, newMask);
		}
 		
		//========================================
		// Fields
		//========================================

		//------------------------------------------------------------
		public T data<T>(Field key) {
			return ds.data<T>(key);
		}

		//------------------------------------------------------------
		public T data<T>(Entity entKey, string fieldId) {
			return ds.data<T>(entKey, fieldId);
		}

		//------------------------------------------------------------
		public T data<T>(Field key, int index) {
			return ds.data<T>(key, index);
		}

		//------------------------------------------------------------
		public T data<T>(Entity entKey, string fieldId, int index) {
			return ds.data<T>(entKey, fieldId, index);
		}

		//------------------------------------------------------------
		public T data<T>(string entId, string fieldId) {
			return ds.data<T>(entId, fieldId);
		}

		//------------------------------------------------------------
		public T data<T>(string entId, string fieldId, int index) {
			return ds.data<T>(entId, fieldId, index);
		}

		//------------------------------------------------------------
		public List<T> dataList<T>(Field key) {
			return ds.dataList<T>(key);
		}

		//------------------------------------------------------------
		public void insertDataList<T>(Field key, int index, T value) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addFieldIndex;
			dc.ent = key.entity.id;
			dc.field = key.id;
			dc.dataString = DataUtil.dataString(key.dType, value);
			dc.index = index;
			transmitChange(dc);

			notifyListeners(dc, key.entity);
			ds.insertDataList<T>(key, index, value);
		}

		//------------------------------------------------------------
		public void addToDataList<T>(Field key, T value) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addFieldIndex;
			dc.ent = key.entity.id;
			dc.field = key.id;
			dc.dataString = DataUtil.dataString(key.dType, value);
			dc.index = ds.listCount(key);
			transmitChange(dc);

			notifyListeners(dc, key.entity);			
			ds.addToDataList<T>(key, value);
		}

		//------------------------------------------------------------
		public int listCount(Field field) {
			return ds.listCount(field);
		}

		//------------------------------------------------------------
		public Field field(Entity entity, string fieldId) {
			return ds.field(entity, fieldId);
		}

		//------------------------------------------------------------
		public Field field(string entId, string fieldId) {
			return ds.field(entId, fieldId);
		}

		//------------------------------------------------------------
		public HashSet<Field> fields(Entity key, bool specific=false) {
			return ds.fields(key, specific);
		}

		//------------------------------------------------------------
		public bool exists(Field key, bool specific=false) {
			return ds.exists(key, specific);
		}

		//------------------------------------------------------------
		public bool exists(string entId, string fieldId, bool specific=false) {
			return ds.exists(entId, fieldId, specific);
		}

		//------------------------------------------------------------
		public DataMask mask(Field key, bool specific=false) {
			return ds.mask(key, specific);
		}

		//------------------------------------------------------------
		public Field addField<T>(Entity key, string fieldId, T value) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addField;
			dc.ent = key.id;
			dc.field = fieldId;
			dc.dType = DataUtil.dataType(value);
			dc.dataString = DataUtil.dataString(dc.dType, value);
			transmitChange(dc);

			Field newField = ds.addField<T>(key, fieldId, value);
			notifyListeners(dc, key);
			return newField;
		}

		//------------------------------------------------------------
		public Field addFieldList<T>(Entity key, string fieldId) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addFieldList;
			dc.ent = key.id;
			dc.field = fieldId;
			dc.dType = DataUtil.listType<T>();
			transmitChange(dc);

			Field newField = ds.addFieldList<T>(key, fieldId);
			notifyListeners(dc, key);
			return newField;
		}

		//------------------------------------------------------------
		public void edit<T>(string entId, string fieldId, T newValue) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.edit;
			dc.ent = entId;
			dc.field = fieldId;
			dc.dType = DataUtil.dataType(newValue);
			dc.dataString = DataUtil.dataString(dc.dType, newValue);
			transmitChange(dc);

			notifyListeners(dc, this[entId]);
			ds.edit<T>(entId, fieldId, newValue);
		}

		//------------------------------------------------------------
		public void edit<T>(Entity ent, string fieldId, T newValue) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.edit;
			dc.ent = ent.id;
			dc.field = fieldId;
			dc.dType = DataUtil.dataType(newValue);
			dc.dataString = DataUtil.dataString(dc.dType, newValue);
			transmitChange(dc);

			notifyListeners(dc, ent);
			ds.edit<T>(ent, fieldId, newValue);
		}

		//------------------------------------------------------------
		public void edit<T>(Field key, T newValue) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.edit;
			dc.ent = key.entity.id;
			dc.field = key.id;
			dc.dType = DataUtil.dataType(newValue);
			dc.dataString = DataUtil.dataString(dc.dType, newValue);
			transmitChange(dc);

			notifyListeners(dc, key.entity);
			ds.edit<T>(key, newValue);
		}

		//------------------------------------------------------------
		public void edit<T>(string entId, string fieldId, int index, T newValue) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.editIndex;
			dc.ent = entId;
			dc.field = fieldId;
			dc.index = index;
			dc.dType = DataUtil.dataType(newValue);
			dc.dataString = DataUtil.dataString(dc.dType, newValue);
			transmitChange(dc);

			notifyListeners(dc, this[entId]);
			ds.edit<T>(entId, fieldId, index, newValue);
		}

		//------------------------------------------------------------
		public void edit<T>(Entity ent, string fieldId, int index, T newValue) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.editIndex;
			dc.ent = ent.id;
			dc.field = fieldId;
			dc.index = index;
			dc.dType = DataUtil.dataType(newValue);
			dc.dataString = DataUtil.dataString(dc.dType, newValue);
			transmitChange(dc);

			notifyListeners(dc, ent);
			ds.edit<T>(ent, fieldId, index, newValue);
		}

		//------------------------------------------------------------
		public void edit<T>(Field key, int index, T newValue) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.editIndex;
			dc.ent = key.entity.id;
			dc.field = key.id;
			dc.index = index;
			dc.dType = DataUtil.dataType(newValue);
			dc.dataString = DataUtil.dataString(dc.dType, newValue);
			transmitChange(dc);

			notifyListeners(dc, key.entity);
			ds.edit<T>(key, index, newValue);
		}

		//------------------------------------------------------------
		public void rm(Field key) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.rmField;
			dc.ent = key.entity.id;
			dc.field = key.id;
			transmitChange(dc);

			notifyListeners(dc, key.entity);
			ds.rm(key);
		}

		//------------------------------------------------------------
		public void rm(Entity key, string field) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.rmField;
			dc.ent = key.id;
			dc.field = field;
			transmitChange(dc);

			notifyListeners(dc, key);
			ds.rm(key, field);
		}
		
		//------------------------------------------------------------
		public void rm(Field key, int index) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.rmFieldIndex;
			dc.ent = key.entity.id;
			dc.field = key.id;
			dc.index = index;
			transmitChange(dc);

			notifyListeners(dc, key.entity);
			ds.rm(key, index);
		}

		//------------------------------------------------------------
		public void setMask(Field key, DataMask newMask) {
			Debug.LogError("networking masks not implemented");
			ds.setMask(key, newMask);
		}

		//------------------------------------------------------------
		public HashSet<Field> allFields() {
			return ds.allFields();
		}

		//========================================
		// Node methods
		//========================================

		//------------------------------------------------------------
		public HashSet<Entity> children(Entity nodeKey, Entity linkType) {
			return ds.children(nodeKey, linkType);
		}

		//------------------------------------------------------------
		public Entity child(Entity nodeKey, Entity linkType) {
			return ds.child(nodeKey, linkType);
		}

		//------------------------------------------------------------
		public HashSet<Entity> allChildren(Entity nodeKey) {
			return ds.allChildren(nodeKey);
		}

		//------------------------------------------------------------
		public Entity parent(Entity nodeKey, Entity linkType) {
			return ds.parent(nodeKey, linkType);
		}

		//------------------------------------------------------------
		public HashSet<Entity> parents(Entity nodeKey, Entity linkType) {
			return ds.parents(nodeKey, linkType);
		}

		//------------------------------------------------------------
		public HashSet<Entity> allParents(Entity nodeKey) {
			return ds.allParents(nodeKey);
		}

		//------------------------------------------------------------
		public HashSet<Entity> descendents(Entity nodeKey, Entity linkType) {
			return ds.descendents(nodeKey, linkType);
		}

		//------------------------------------------------------------
		public HashSet<Entity> ancestors(Entity nodeKey, Entity linkType) {
			return ds.ancestors(nodeKey, linkType);
		}

		//------------------------------------------------------------
		public HashSet<Entity> allNodes() {
			return ds.allNodes();
		}

		//------------------------------------------------------------
		public Entity renameNode(Entity node, string newName) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.rename;
			dc.ent = node.id;
			dc.dataString = newName;
			transmitChange(dc);

			Entity newNode = ds.renameNode(node, newName);
			updateListenersNewEnt(newNode, dc);
			return newNode;
		}

		//------------------------------------------------------------
		public Entity cloneNode(Entity node, string newName, bool parentLinks=false) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.clone;
			dc.ent = node.id;
			dc.dataString = newName;
			if (parentLinks) {
				dc.index = 1;
			}
			transmitChange(dc);

			Entity newNode = ds.cloneNode(node, newName, parentLinks);
			updateListenersNewEnt(newNode, dc);
			return newNode;
		}

		
		//========================================
		// DataLink Methods
		//========================================

		//------------------------------------------------------------
		public bool linkExists(Entity linkType, Entity parent, Entity child) {
			return ds.linkExists(linkType, parent, child);
		}
		
		//------------------------------------------------------------
		public Entity link(Entity linkType, Entity parent, Entity child) {
			return ds.link(linkType, parent, child);
		}

		//------------------------------------------------------------
		public Entity linkParent(Entity linkKey) {
			return ds.linkParent(linkKey);
		}

		//------------------------------------------------------------
		public Entity linkChild(Entity linkKey) {
			return ds.linkChild(linkKey);
		}

		//------------------------------------------------------------
		public HashSet<Entity> childLinks(Entity nodeKey, Entity linkType) {
			return ds.childLinks(nodeKey, linkType);
		}

		//------------------------------------------------------------
		public HashSet<Entity> allChildLinks(Entity nodeKey) {
			return ds.allChildLinks(nodeKey);
		}

		//------------------------------------------------------------
		public HashSet<Entity> parentLinks(Entity nodeKey, Entity linkType) {
			return ds.parentLinks(nodeKey, linkType);
		}

		//------------------------------------------------------------
		public HashSet<Entity> allParentLinks(Entity nodeKey) {
			return ds.allParentLinks(nodeKey);
		}

		//------------------------------------------------------------
		public HashSet<Entity> allLinks() {
			return ds.allLinks();
		}
		
		//========================================
		// Type methods
		//========================================

		//------------------------------------------------------------
		public bool isType(Entity key) {
			return ds.isType(key);
		}

		//------------------------------------------------------------
		public bool isEntType(Entity key, Entity typeKey, bool specific=false) {
			return ds.isEntType(key, typeKey, specific);
		}

		//------------------------------------------------------------
		public HashSet<Entity> entTypes(Entity key, bool specific=false) {
			return ds.entTypes(key, specific);
		}

		//------------------------------------------------------------
		public Entity linkType(Entity key) {
			return ds.linkType(key);
		}

		//------------------------------------------------------------
		public HashSet<Entity> typeEnts(Entity typekey, bool specific=false) {
			return ds.typeEnts(typekey, specific);
		}

		//------------------------------------------------------------
		public HashSet<Entity> childTypes(Entity typekey, bool specific=false) {
			return ds.childTypes(typekey, specific);
		}

		//------------------------------------------------------------
		public void addEntToType(Entity key, Entity typeKey) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.addToGroup;
			dc.ent = key.id;
			dc.group = typeKey.id;
			transmitChange(dc);

			ds.addEntToType(key, typeKey);
			updateListenersNewEnt(key, dc);
		}

		//------------------------------------------------------------
		public void rmEntFromType(Entity key, Entity typeKey) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.rmFromGroup;
			dc.ent = key.id;
			dc.group = typeKey.id;
			transmitChange(dc);

			ds.rmEntFromType(key, typeKey);
			notifyListeners(dc, key, typeKey);
		}

		//------------------------------------------------------------
		public HashSet<Entity> allLinkTypes() {
			return ds.allLinkTypes();
		}

		//------------------------------------------------------------
		public HashSet<Entity> allNodeTypes() {
			return ds.allNodeTypes();
		}
		
		//========================================
		// Set methods
		//========================================

		//------------------------------------------------------------
		public bool isSet(Entity key) {
			return ds.isSet(key);
		}

		//------------------------------------------------------------
		public bool isEntSet(Entity key, Entity typeKey, bool specific=false) {
			return ds.isEntSet(key, typeKey, specific);
		}

		//------------------------------------------------------------
		public HashSet<Entity> entSets(Entity linkKey, bool specific=false) {
			return ds.entSets(linkKey, specific);
		}

		//------------------------------------------------------------
		public HashSet<Entity> setEnts(Entity setKey, bool specific=false) {
			return ds.setEnts(setKey, specific);
		}

		//------------------------------------------------------------
		public HashSet<Entity> childSets(Entity setkey, bool specific=false) {
			return ds.childSets(setkey, specific);
		}

		//------------------------------------------------------------
		public void addEntToSet(Entity key, Entity setKey) {
			if (key == null || setKey == null) {
				Debug.LogError("key or setKey null" + "\n" + key + "\n" + setKey);
				return;
			}
			DataChange dc = new DataChange();
			dc.type = ChangeType.addToGroup;
			dc.ent = key.id;
			dc.group = setKey.id;
			transmitChange(dc);

			ds.addEntToSet(key, setKey);
			updateListenersNewEnt(key, dc);
		}

		//------------------------------------------------------------
		public void rmEntFromSet(Entity key, Entity setKey) {
			DataChange dc = new DataChange();
			dc.type = ChangeType.rmFromGroup;
			dc.ent = key.id;
			dc.group = setKey.id;
			transmitChange(dc);

			ds.rmEntFromSet(key, setKey);
			notifyListeners(dc, key, setKey);
		}

		//------------------------------------------------------------
		public HashSet<Entity> allLinkSets() {
			return ds.allLinkSets();
		}

		//------------------------------------------------------------
		public HashSet<Entity> allNodeSets() {
			return ds.allNodeSets();
		}

	}
}
