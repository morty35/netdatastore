﻿using UnityEngine;
using System.Collections;

namespace DS {

	public interface DataListener {
		void notify(DataChange change);
	}

}
