using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public partial class DataStore : DataSet {

		// links string names to entities
		private Dictionary<string, Entity> entityByString;

		//========================================
		// Fields Data
		//========================================

		// Store field data for all the differnet entities
		private Dictionary<Field, object> fieldData;

		// Stores field data List for all different entities
		private Dictionary<Field, List<object>> fieldListData;

		// Store masks for Fields that don't follow node mask
		private Dictionary<Field, DataMask> fieldMasks;

		// Stores list of fields in each entity
		private Dictionary<Entity, Dictionary<string, Field>> entityFields;

		//========================================
		// Entity Data
		//========================================

		// Stores all entities
		private HashSet<Entity> entities;

		// Store entity masks that are different from default
		private Dictionary<Entity, DataMask> entityMasks;

		//========================================
		// Entity Relationship data
		//========================================

		// lists all entities belonging to a set
		private Dictionary<Entity, HashSet<Entity>> setEntLists;
		// lists all sets that a entity belongs to
		private Dictionary<Entity, HashSet<Entity>> entSetLists;
		// lists all parent sets of a set
		private Dictionary<Entity, HashSet<Entity>> setParentLists;
		// lists all child sets of a set
		private Dictionary<Entity, HashSet<Entity>> setChildLists;
		
		// lists all entities that have a type
		private Dictionary<Entity, HashSet<Entity>> typeEntLists;
		// lists all types that a entity belongs to
		private Dictionary<Entity, HashSet<Entity>> entTypeLists;
		// lists all parent types of a type
		private Dictionary<Entity, HashSet<Entity>> typeParentLists;
		// lists all child sets of a set
		private Dictionary<Entity, HashSet<Entity>> typeChildLists;
		
		// stores links by type, then parent
		private Dictionary<Entity, Dictionary<Entity, HashSet<Entity>>> nodeChildLinks;
		// stores links by type, then child
		private Dictionary<Entity, Dictionary<Entity, HashSet<Entity>>> nodeParentLinks;
		// stores link children
		private Dictionary<Entity, Entity> linkChildren;
		// stores link parents
		private Dictionary<Entity, Entity> linkParents;

		//************************************************************
		// Private methods
		//************************************************************

		//------------------------------------------------------------
		private Entity addEntity(string id, EntType type) {
			Entity key;
			if (entityByString.TryGetValue(id, out key)) {
				Debug.LogError("Can't add Entity because id exists: " + id);
				return null;
			}

			Entity newEnt = new Entity(id, type);
			entityByString.Add(id, newEnt);
			entities.Add(newEnt);

			return newEnt;
		}

		//------------------------------------------------------------
		private Field getTrueField(Field key) {
			if (key == null || !exists(key.entity) 
				|| key.id == null || key.id == "") {
				return null;
			}
			// First check entity
			Dictionary<string, Field> eFields;
			if (entityFields.TryGetValue(key.entity, out eFields)) {
				Field foundField;
				if (eFields.TryGetValue(key.id, out foundField)) {
					if (foundField.fType == key.fType 
						&& foundField.dType == key.dType) {
						return foundField;
					}
				}
			}

			foreach (Entity entType in entTypes(key.entity)) {
				if (entityFields.TryGetValue(entType, out eFields)) {
					Field foundField;
					if (eFields.TryGetValue(key.id, out foundField)) {
						if (foundField.fType == key.fType 
							&& foundField.dType == key.dType ) {
							return foundField;
						}
					}
				}
			}
			return null;
		}

		//------------------------------------------------------------
		private List<System.Object> createSpecificDataList<T>(Field key) {
			List<System.Object> oldList;
			List<System.Object> newList = null;

			if (fieldListData.TryGetValue(key, out oldList)) {
				return oldList;
			}
			else {
				foreach (Entity entType in entTypes(key.entity)) {
					if (fieldListData.TryGetValue(new Field(entType, key.id, key.fType, key.dType), out oldList)) {
						List<T> createList = new List<T>();
						foreach (System.Object obj in oldList) {
							createList.Add((T) obj);
						}
						Field newField = addFieldList<T>(key.entity, key.id);
						foreach (T val in createList) {
							addToDataList<T>(newField, val);
						}
						fieldListData.TryGetValue(newField, out newList);
						return newList;
					}
				}
			}

			Debug.LogError("Couldn't find oldList to recreate");
			return null;
		}

	}
}
