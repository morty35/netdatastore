using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public partial class DataStore : DataSet {

		//========================================
		// Node methods
		//========================================

		//------------------------------------------------------------
		public Entity child(Entity nodeKey, Entity linkType) {
			return DataUtil.toSingle(children(nodeKey, linkType));
		}
		
		//------------------------------------------------------------
		public HashSet<Entity> children(Entity nodeKey, Entity linkType) {
			if (!exists(nodeKey) || nodeKey.type != EntType.node) {
				Debug.LogError("Can't call children on invalid nodeKey: " + nodeKey);
			}
			if (!exists(linkType) || linkType.type != EntType.linkType) {
				Debug.LogError("Can't call children on invalid linkType: " + linkType);
			}
		
			Dictionary<Entity, HashSet<Entity>> typeLinks;
			if (!nodeChildLinks.TryGetValue(linkType, out typeLinks)) {
				return new HashSet<Entity>();
			}
			HashSet<Entity> childLinks;
			if (typeLinks.TryGetValue(nodeKey, out childLinks)) {
				HashSet<Entity> children = new HashSet<Entity>();
				foreach (Entity link in childLinks) {
					children.Add(linkChild(link));
				}
				return children;
			}
			else {
				return new HashSet<Entity>();
			}
		}

		//------------------------------------------------------------
		public HashSet<Entity> allChildren(Entity nodeKey) {
			if (!exists(nodeKey) || nodeKey.type != EntType.node) {
				Debug.LogError("Can't call allChildren on invalid nodeKey: " + nodeKey);
			}

			HashSet<Entity> children = new HashSet<Entity>();
			// look over all types
			foreach (Entity typeLink in allLinkTypes()) {
				Dictionary<Entity, HashSet<Entity>> typeLinks;
				if (nodeChildLinks.TryGetValue(typeLink, out typeLinks)) {
					HashSet<Entity> childLinks;
					if (typeLinks.TryGetValue(nodeKey, out childLinks)) {
						// check children not already in list before adding
						foreach (Entity link in childLinks) {
							Entity child = linkChild(link);
							if (!children.Contains(child)) {
								children.Add(child);
							}
						}
					}
				}
			}
			return children;
		}

		//------------------------------------------------------------
		public Entity parent(Entity nodeKey, Entity linkType) {
			return DataUtil.toSingle(parents(nodeKey, linkType));
		}
		
		//------------------------------------------------------------
		public HashSet<Entity> parents(Entity nodeKey, Entity linkType) {
			if (!exists(nodeKey) || nodeKey.type != EntType.node) {
				Debug.LogError("Can't call parents on invalid nodeKey: "
							   + ((nodeKey == null) ? "null" : nodeKey.ToString()));
			}
			if (!exists(linkType) || linkType.type != EntType.linkType) {
				Debug.LogError("Can't call parents on invalid linkType: "
							   + ((linkType == null) ? "null" : linkType.ToString()));
			}
		
			Dictionary<Entity, HashSet<Entity>> typeLinks;
			if (!nodeParentLinks.TryGetValue(linkType, out typeLinks)) {
				return new HashSet<Entity>();
			}
			HashSet<Entity> parentLinks;
			if (typeLinks.TryGetValue(nodeKey, out parentLinks)) {
				HashSet<Entity> parents = new HashSet<Entity>();
				foreach (Entity link in parentLinks) {
					parents.Add(linkParent(link));
				}
				return parents;
			}
			else {
				return new HashSet<Entity>();
			}
		}

		//------------------------------------------------------------
		public HashSet<Entity> allParents(Entity nodeKey) {
			if (!exists(nodeKey) || nodeKey.type != EntType.node) {
				Debug.LogError("Can't call allParents on invalid nodeKey: " + nodeKey);
			}

			HashSet<Entity> parentList = new HashSet<Entity>();
			// look over all types
			foreach (Entity typeLink in allLinkTypes()) {
				Dictionary<Entity, HashSet<Entity>> typeLinks;
				if (nodeParentLinks.TryGetValue(typeLink, out typeLinks)) {
					HashSet<Entity> parents;
					if (typeLinks.TryGetValue(nodeKey, out parents)) {
						// check parents not already in list before adding
						foreach (Entity link in parents) {
							Entity parent = linkParent(link);
							if (!parentList.Contains(parent)) {
								parentList.Add(parent);
							}
						}
					}
				}
			}
			return parentList;
		}

		//------------------------------------------------------------
		public HashSet<Entity> descendents(Entity nodeKey, Entity linkType) {
			if (!exists(nodeKey) || nodeKey.type != EntType.node) {
				Debug.LogError("Can't call descendents on invalid nodeKey: " + nodeKey);
			}
			if (!exists(linkType) || linkType.type != EntType.linkType) {
				Debug.LogError("Can't call descendents on invalid linkType: " + linkType);
			}

			HashSet<Entity> descendents = new HashSet<Entity>();
			Stack<Entity> links = new Stack<Entity>(childLinks(nodeKey, linkType));

			while (links.Count > 0) {
				Entity link = links.Pop();
				Entity childNode = linkChild(link);

				if (!descendents.Contains(childNode)) {
					descendents.Add(childNode);

					// Add new children to stack
					HashSet<Entity> newChildren = childLinks(childNode, linkType);
					if (newChildren != null) {
						foreach (Entity newChild in newChildren) {
							links.Push(newChild);
						}
					}
				}
			}
			return descendents;
		}

		//------------------------------------------------------------
		public HashSet<Entity> ancestors(Entity nodeKey, Entity linkType) {
			if (!exists(nodeKey) || nodeKey.type != EntType.node) {
				Debug.LogError("Can't call ancestors on invalid nodeKey: " + nodeKey);
			}
			if (!exists(linkType) || linkType.type != EntType.linkType) {
				Debug.LogError("Can't call ancestors on invalid linkType: " + linkType);
			}

			HashSet<Entity> ancestors = new HashSet<Entity>();
			Stack<Entity> links = new Stack<Entity>(parentLinks(nodeKey, linkType));

			while (links.Count > 0) {
				Entity link = links.Pop();
				Entity parentNode = linkParent(link);

				if (!ancestors.Contains(parentNode)) {
					ancestors.Add(parentNode);

					// Add new parents to stack
					HashSet<Entity> newParents = parentLinks(parentNode, linkType);
					if (newParents != null) {
						foreach (Entity newParent in newParents) {
							links.Push(newParent);
						}
					}
				}
			}
			return ancestors;
		}

		//------------------------------------------------------------
		public HashSet<Entity> allNodes() {
			HashSet<Entity> nodeList = new HashSet<Entity>(entities);
			nodeList.RemoveWhere(e => e.type != EntType.node);
			return nodeList;
		}

		//------------------------------------------------------------
		public Entity renameNode(Entity node, string newName) {
			if (!exists(node)) {
				Debug.LogError("Can't rename non-existent node: " + node);
				return null;
			}
			if (newName == null || newName == "") {
				Debug.LogError("Invalid name: " + newName);
				return null;
			}
			
			Entity newNode = cloneNode(node, newName, true);
			rm(node);
			return newNode;
		}

		//------------------------------------------------------------
		public Entity cloneNode(Entity node, string newName, bool parentLinks=false) {
			if (!exists(node)) {
				Debug.LogError("Can't clone non-existent node: " + node);
				return null;
			}
			if (newName == null || newName == "") {
				Debug.LogError("Invalid name: " + newName);
				return null;
			}

			Entity newNode = addNode(newName);
			cloneFields(newNode, node);

			// clone types
			foreach (Entity nodeType in entTypes(node)) {
				addEntToType(newNode, nodeType);
			}
			// clone sets
			foreach (Entity nodeSet in entSets(node)) {
				addEntToSet(newNode, nodeSet);
			}
			// clone child links
			foreach (Entity link in allChildLinks(node)) {
				Entity type = linkType(link);
				Entity child = linkChild(link);

				Entity newLink = addLink(type, newNode, child);
				// Clone link Fields
				cloneFields(newLink, link);
			}

			if (parentLinks) {
				// clone parent links
				foreach (Entity link in allParentLinks(node)) {
					Entity type = linkType(link);
					Entity parent = linkParent(link);

					Entity newLink = addLink(type, parent, newNode);
					// Clone link Fields
					cloneFields(newLink, link);
				}
			}
			return newNode;
		}

		//========================================
		// DataLink Methods
		//========================================

		//------------------------------------------------------------
		public bool linkExists(Entity linkType, Entity parent, Entity child) {
			return entityByString.ContainsKey(linkType.id + ":" + parent.id + ":" + child.id);
		}

		//------------------------------------------------------------
		public Entity link(Entity linkType, Entity parent, Entity child) {
			if (!exists(linkType) || !exists(parent) || !exists(child)
				|| linkType.type != EntType.linkType
				|| parent.type != EntType.node
				|| child.type != EntType.node) {
				Debug.LogError("Can't call link with invalid params: " 
							   + linkType + "\n" + parent + "\n" + child);
			}
			return this[linkType.id + ":" + parent.id + ":" + child.id];
		}

		//------------------------------------------------------------
		public Entity linkType(Entity key) {
			if (!exists(key) || key.type != EntType.link) {
				Debug.LogError("Can't call linkType on invalid entity: " + key);
			}
			HashSet<Entity> types;
			if (entTypeLists.TryGetValue(key, out types)) {
				if (types.Count != 1) {
					Debug.LogError("Links should only have one type! " + key);
					return null;
				}
				return DataUtil.toSingle(types);
			}
			else {
				return null;
			}
		}

		//------------------------------------------------------------
		public Entity linkParent(Entity linkKey) {
			if (!exists(linkKey) || linkKey.type != EntType.link) {
				Debug.LogError("Can't call linkParent on invalid linkKey: " + linkKey);
			}
			Entity linkParent;
			linkParents.TryGetValue(linkKey, out linkParent);
			return linkParent;
		}

		//------------------------------------------------------------
		public Entity linkChild(Entity linkKey) {
			if (!exists(linkKey) || linkKey.type != EntType.link) {
				Debug.LogError("Can't call linkChild on invalid linkKey: " + linkKey);
			}
			Entity linkChild;
			linkChildren.TryGetValue(linkKey, out linkChild);
			return linkChild;
		}

		//------------------------------------------------------------
		public HashSet<Entity> childLinks(Entity nodeKey, Entity linkType) {
			if (!exists(nodeKey) || nodeKey.type != EntType.node) {
				Debug.LogError("Can't call childLinks on invalid nodeKey: " + nodeKey);
			}
			if (!exists(linkType) || linkType.type != EntType.linkType) {
				Debug.LogError("Can't call childLinks on invalid linkType: " + linkType);
			}
		
			Dictionary<Entity, HashSet<Entity>> typeLinks;
			if (!nodeChildLinks.TryGetValue(linkType, out typeLinks)) {
				return new HashSet<Entity>();
			}
			HashSet<Entity> childLinks;
			if (typeLinks.TryGetValue(nodeKey, out childLinks)) {
				return new HashSet<Entity>(childLinks);
			}
			else {
				return new HashSet<Entity>();
			}
		}

		//------------------------------------------------------------
		public HashSet<Entity> allChildLinks(Entity nodeKey) {
			if (!exists(nodeKey) || nodeKey.type != EntType.node) {
				Debug.LogError("Can't call allChildren on invalid nodeKey: " + nodeKey);
			}

			HashSet<Entity> childrenLinks = new HashSet<Entity>();
			// look over all types
			foreach (Entity typeLink in allLinkTypes()) {
				Dictionary<Entity, HashSet<Entity>> typeLinks;
				if (nodeChildLinks.TryGetValue(typeLink, out typeLinks)) {
					HashSet<Entity> childLinks;
					if (typeLinks.TryGetValue(nodeKey, out childLinks)) {
						// check children not already in list before adding
						foreach (Entity link in childLinks) {
							if (!childrenLinks.Contains(link)) {
								childrenLinks.Add(link);
							}
						}
					}
				}
			}
			return childrenLinks;
		}

		//------------------------------------------------------------
		public HashSet<Entity> parentLinks(Entity nodeKey, Entity linkType) {
			if (!exists(nodeKey) || nodeKey.type != EntType.node) {
				Debug.LogError("Can't call parents on invalid nodeKey: " + nodeKey);
			}
			if (!exists(linkType) || linkType.type != EntType.linkType) {
				Debug.LogError("Can't call parents on invalid linkType: " + linkType);
			}
		
			Dictionary<Entity, HashSet<Entity>> typeLinks;
			if (!nodeParentLinks.TryGetValue(linkType, out typeLinks)) {
				return new HashSet<Entity>();
			}
			HashSet<Entity> parentLinks;
			if (typeLinks.TryGetValue(nodeKey, out parentLinks)) {
				return new HashSet<Entity>(parentLinks);
			}
			else {
				return new HashSet<Entity>();
			}
		}

		//------------------------------------------------------------
		public HashSet<Entity> allParentLinks(Entity nodeKey) {
			if (!exists(nodeKey) || nodeKey.type != EntType.node) {
				Debug.LogError("Can't call allParents on invalid nodeKey: " + nodeKey);
			}

			HashSet<Entity> parentLinks = new HashSet<Entity>();
			// look over all types
			foreach (Entity typeLink in allLinkTypes()) {
				Dictionary<Entity, HashSet<Entity>> typeLinks;
				if (nodeParentLinks.TryGetValue(typeLink, out typeLinks)) {
					HashSet<Entity> parents;
					if (typeLinks.TryGetValue(nodeKey, out parents)) {
						// check parents not already in list before adding
						foreach (Entity link in parents) {
							if (!parentLinks.Contains(link)) {
								parentLinks.Add(link);
							}
						}
					}
				}
			}
			return parentLinks;
		}

		//------------------------------------------------------------
		public HashSet<Entity> allLinks() {
			return new HashSet<Entity>(linkChildren.Keys);
		}
	}
}
