﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public class DataFilterItem {
		public FilterType type;
		public FilterMode mode;
		public HashSet<string> ents = new HashSet<string>();
	}

}

