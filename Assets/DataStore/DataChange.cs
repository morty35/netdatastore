using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public class DataChange {
		public ChangeType type;
		public EntType entType; // used when adding ent
		public string ent = null;
		public string parent = null; // used for addLink
		public string child = null; // used for addLink
		public string linkType = null; // used for addLink
		public string field = null;
		public DataType dType;
		public string dataString;
		public int index = -1;
		public string group;

		//------------------------------------------------------------
		public override string ToString() {
			return "DataChange|type:" + type + "|entType:" + entType + "|ent:" + ent +
				"|field:" + field + "|dType:" + dType + "!dataString:" + dataString +
				"|index:" + index + "|group:" + group;
		}
	}
}
