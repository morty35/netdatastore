using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public interface DataSet {

		//========================================
		// Entity
		//========================================
		Entity this[string id] {get;}
		void clearAll();
		HashSet<Entity> allEnts();
		Entity ent(string id);
		DataMask mask(Entity key, bool specific=false);
		bool exists(Entity key);
		bool exists(string id);
		Entity addNode(string id);
		Entity addLink(Entity type, Entity parent, Entity child);
		Entity addNodeSet(string id);
		Entity addLinkSet(string id);
		Entity addNodeType(string id);
		Entity addLinkType(string id);
		void rm(Entity key);
		void setMask(Entity key, DataMask newMask);

		//========================================
		// Fields
		//========================================
		T data<T>(Field key);
		T data<T>(Entity entKey, string fieldId);
		T data<T>(Field key, int index);
		T data<T>(Entity entKey, string fieldId, int index);
		T data<T>(string entId, string fieldId);
		T data<T>(string entId, string fieldId, int index);
		List<T> dataList<T>(Field key);
		void insertDataList<T>(Field key, int index, T value);
		void addToDataList<T>(Field key, T value);
		int listCount(Field field);
		Field field(Entity entity, string fieldId);
		Field field(string entId, string fieldId);
		HashSet<Field> fields(Entity key, bool specific=false);
		bool exists(Field key, bool specific=false);
		bool exists(string entId, string fieldId, bool specific=false);
		DataMask mask(Field key, bool specific=false);
		Field addField<T>(Entity key, string fieldId, T value);
		Field addFieldList<T>(Entity key, string fieldId);
		void edit<T>(string entId, string fieldId, T newValue);
		void edit<T>(Entity ent, string fieldId, T newValue);
		void edit<T>(Field key, T newValue);
		void edit<T>(string entId, string fieldId, int index, T newValue);
		void edit<T>(Entity ent, string fieldId, int index, T newValue);
		void edit<T>(Field key, int index, T newValue);
		void rm(Field key);
		void rm(Entity key, string field);
		void rm(Field key, int index);
		void setMask(Field key, DataMask newMask);
		HashSet<Field> allFields();

		//========================================
		// Node methods
		//========================================
		Entity child(Entity nodeKey, Entity linkType);
		HashSet<Entity> children(Entity nodeKey, Entity linkType);
		HashSet<Entity> allChildren(Entity nodeKey);
		Entity parent(Entity nodeKey, Entity linkType);
		HashSet<Entity> parents(Entity nodeKey, Entity linkType);
		HashSet<Entity> allParents(Entity nodeKey);
		HashSet<Entity> descendents(Entity nodeKey, Entity linkType);
		HashSet<Entity> ancestors(Entity nodeKey, Entity linkType);
		HashSet<Entity> allNodes();
		Entity renameNode(Entity key, string newName);
		Entity cloneNode(Entity key, string newName, bool parentLinks=false);

		//========================================
		// DataLink Methods
		//========================================
		bool linkExists(Entity linkType, Entity parent, Entity child);
		Entity link(Entity linkType, Entity parent, Entity child);
		Entity linkType(Entity linkKey);
		Entity linkParent(Entity linkKey);
		Entity linkChild(Entity linkKey);
		HashSet<Entity> childLinks(Entity nodeKey, Entity linkType);
		HashSet<Entity> allChildLinks(Entity nodeKey);
		HashSet<Entity> parentLinks(Entity nodeKey, Entity linkType);
		HashSet<Entity> allParentLinks(Entity nodeKey);
		HashSet<Entity> allLinks();
//		void changeChild(Entity link, Entity newChild);
//		void changeParent(Entity link, Entity newChild);

		//========================================
		// Type methods
		//========================================
		bool isType(Entity key);
		bool isEntType(Entity key, Entity typeKey, bool specific=false);
		HashSet<Entity> entTypes(Entity entKey, bool specific=false);
		HashSet<Entity> typeEnts(Entity typekey, bool specific=false);
		HashSet<Entity> childTypes(Entity typeKey, bool specific=false);
		void addEntToType(Entity key, Entity typeKey);
		void rmEntFromType(Entity key, Entity typeKey);
		HashSet<Entity> allLinkTypes();
		HashSet<Entity> allNodeTypes();

		//========================================
		// Set methods
		//========================================
		bool isSet(Entity key);
		bool isEntSet(Entity key, Entity typeKey, bool specific=false);
		HashSet<Entity> entSets(Entity entKey, bool specific=false);
		HashSet<Entity> setEnts(Entity setKey, bool specific=false);
		HashSet<Entity> childSets(Entity typeKey, bool specific=false);
		void addEntToSet(Entity key, Entity setKey);
		void rmEntFromSet(Entity key, Entity setKey);
		HashSet<Entity> allLinkSets();
		HashSet<Entity> allNodeSets();

	}
}
