using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DS {

	public partial class DataStore : DataSet {

		//========================================
		// Type methods
		//========================================

		//------------------------------------------------------------
		public bool isType(Entity key) {
			if (key.type == EntType.nodeType ||
				key.type == EntType.linkType) {
				return true;
			}
			return false;
		}
		
		//------------------------------------------------------------
		public bool isEntType(Entity key, Entity typeKey, bool specific=false) {
			// check params
			if (!exists(key) || !exists(typeKey)) {
				Debug.LogError("Can't call isEntType when invalid params: " + "\n"
							   + key + "\n" + typeKey);
				return false;
			}
			if (typeKey.type != EntType.linkType && typeKey.type != EntType.nodeType) {
				Debug.LogError("isEntType(key, type) must have type with "
							   + "of type nodeType or linkType\n" + key + "\n" + typeKey);
				return false;
			}

			if (isType(key)) {
				HashSet<Entity> types;
				if (typeParentLists.TryGetValue(key, out types)) {
					if (types.Contains(typeKey)) {
						return true;
					}
					if (specific) {
						return false;
					}

					// look through hierarchy of types
					HashSet<Entity> parents;
					if (typeParentLists.TryGetValue(key, out parents)) {
						foreach (Entity parent in parents) {
							if (isEntType(parent, typeKey)) {
								return true;
							}
						}
					}
				}
			}
			else {
				HashSet<Entity> types;
				if (entTypeLists.TryGetValue(key, out types)) {
					if (types.Contains(typeKey)) {
						return true;
					}
				}
				if (specific) {
					return false;
				}

				// search through hierarchy of types
				if (types != null) {
					foreach (Entity type in types) {
						if (isEntType(type, typeKey)) {
							return true;
						}
					}
				}
			}

			return false;
		}

		//------------------------------------------------------------
		public HashSet<Entity> entTypes(Entity key, bool specific=false) {
			if (!exists(key)) {
				Debug.LogError("Can't call entTypes on invalid entity: " + key);
			}

			HashSet<Entity> types = new HashSet<Entity>();
			HashSet<Entity> returnTypes = new HashSet<Entity>();

			// Link key
			if (key.type == EntType.link) {
				returnTypes.Add(linkType(key));
			}
			else if (isType(key)) {
				if (typeParentLists.TryGetValue(key, out types)) {
					returnTypes = new HashSet<Entity>(types);
				}
			}
			else {
				if (entTypeLists.TryGetValue(key, out types)) {
					returnTypes = new HashSet<Entity>(types);
				}
			}
			
			if (specific || types == null) {
				return returnTypes;
			}

			foreach (Entity type in types) {
				returnTypes.UnionWith(entTypes(type));
			}
			return returnTypes;
		}

		//------------------------------------------------------------
		public HashSet<Entity> typeEnts(Entity typeKey, bool specific=false) {
			if (!exists(typeKey) 
				|| (typeKey.type != EntType.nodeType	
					&& typeKey.type != EntType.linkType)) {
				Debug.LogError("Can't call typeEnts on invalid entity: " + typeKey);
			}

			HashSet<Entity> ents;
			if (typeEntLists.TryGetValue(typeKey, out ents)) {
				if (specific) {
					return new HashSet<Entity>(ents);
				}
				else {
					HashSet<Entity> returnEnts = new HashSet<Entity>(ents);
					HashSet<Entity> children;
					if (typeChildLists.TryGetValue(typeKey, out children)) {
						foreach (Entity child in children) {
							returnEnts.UnionWith(typeEnts(child));
						}
					}
					return returnEnts;
				}
			}
			return new HashSet<Entity>();
		}

		//------------------------------------------------------------
		public HashSet<Entity> childTypes(Entity typeKey, bool specific=false) {
			if (!exists(typeKey)) {
				Debug.LogError("Can't call childTypes on invalid entity: " + typeKey);
			}

			HashSet<Entity> types;
			if (typeChildLists.TryGetValue(typeKey, out types)) {
				if (specific) {
					return new HashSet<Entity>(types);
				}
				else {
					HashSet<Entity> returnEnts = new HashSet<Entity>(types);
					foreach (Entity child in types) {
						returnEnts.UnionWith(childTypes(child));
					}
					return returnEnts;
				}
			}
			return new HashSet<Entity>();
		}
		
		//------------------------------------------------------------
		public void addEntToType(Entity key, Entity typeKey) {
			if (!exists(key) || !exists(typeKey)) {
				Debug.LogError("Can't call addEntToType when invalid params: " 
							   + key + " :" + typeKey);
				return;
			}
			if (key.type == EntType.link) {
				Debug.LogError("Links can only have one linkType:" + key);
				return;
			}
			
			HashSet<Entity> types;

			// add to type hierarchy
			if (isType(key)) {
				// add to typeParentLists
				if (!typeParentLists.ContainsKey(key)) {
					typeParentLists.Add(key, new HashSet<Entity>());
				}
				HashSet<Entity> parents = typeParentLists[key];
				if (parents.Contains(typeKey)) {
					return;
				}
				parents.Add(typeKey);

				// add to typeChildLists
				if (!typeChildLists.ContainsKey(typeKey)) {
					typeChildLists.Add(typeKey, new HashSet<Entity>());
				}
				HashSet<Entity> children = typeChildLists[typeKey];
				children.Add(key);

			}
			else {
				// add to entTypeLists
				if (!entTypeLists.TryGetValue(key, out types)) {
					entTypeLists.Add(key, new HashSet<Entity>());
					entTypeLists.TryGetValue(key, out types);
				}
				if (types.Contains(typeKey)) {
					return;
				}
				else {
					types.Add(typeKey);
				}
				
				// add to typeEntLists
				HashSet<Entity> ents;
				if (!typeEntLists.TryGetValue(typeKey, out ents)) {
					typeEntLists.Add(typeKey, new HashSet<Entity>());
					typeEntLists.TryGetValue(typeKey, out ents);
				}
				ents.Add(key);
			}
		}

		//------------------------------------------------------------
		public void rmEntFromType(Entity key, Entity typeKey) {
			if (!exists(key) || !exists(typeKey)) {
				Debug.LogError("Can't call rmEntFromType when invalid params: "
							   + key + "\n" + typeKey);
				return;
			}
			if ((key.type == EntType.link)) {
				Debug.LogError("Can't remove link's type: \n" + key);
				return;
			}

			if (isType(key)) {
				// rm from typeChildLists	
				HashSet<Entity> types;
				if (typeChildLists.TryGetValue(typeKey, out types)) {
					if (!types.Contains(key)) {
						return;
					}
					types.Remove(key);
					if (types.Count == 0) {
						typeChildLists.Remove(typeKey);
					}
				}
				
				// rm from typeParentLists
				HashSet<Entity> ents;
				if (typeParentLists.TryGetValue(key, out ents)) {
					ents.Remove(typeKey);
					if (ents.Count == 0) {
						typeParentLists.Remove(key);
					}
				}
			}
			else {
				// rm from entTypeHashSets	
				HashSet<Entity> types;
				if (entTypeLists.TryGetValue(key, out types)) {
					types.Remove(typeKey);
					if (types.Count == 0) {
						entTypeLists.Remove(key);
					}
				}
				
				// rm from typeEntLists
				HashSet<Entity> ents;
				if (typeEntLists.TryGetValue(typeKey, out ents)) {
					ents.Remove(key);
					if (ents.Count == 0) {
						typeEntLists.Remove(key);
					}
				}
			}
		}

		//------------------------------------------------------------
		public HashSet<Entity> allLinkTypes() {
			HashSet<Entity> returnList = allEnts();
			returnList.RemoveWhere(x => x.type != EntType.linkType);
			return returnList;
		}

		//------------------------------------------------------------
		public HashSet<Entity> allNodeTypes() {
			HashSet<Entity> returnList = allEnts();
			returnList.RemoveWhere(x => x.type != EntType.nodeType);
			return returnList;
		}

		//========================================
		// Set methods
		//========================================

		//------------------------------------------------------------
		public bool isSet(Entity key) {
			if (key.type == EntType.nodeSet ||
				key.type == EntType.linkSet) {
				return true;
			}
			return false;
		}
		
		//------------------------------------------------------------
		public bool isEntSet(Entity key, Entity setKey, bool specific=false) {
			// check params
			if (!exists(key) || !exists(setKey)) {
				Debug.LogError("Can't call isEntSet when invalid params: " + "\n" 
							   + key + "\n" + setKey);
				return false;
			}
			if (setKey.type != EntType.linkSet && setKey.type != EntType.nodeSet) {
				Debug.LogError("isEntSet(key, set) must have set with "
							   + "type nodeSet or linkSet|\n" + key + "|\n" + setKey);
				return false;
			}

			if (isSet(key)) {
				HashSet<Entity> sets;
				if (setParentLists.TryGetValue(key, out sets)) {
					if (sets.Contains(setKey)) {
						return true;
					}
					if (specific) {
						return false;
					}

					// look through hierarchy of sets
					HashSet<Entity> parents;
					if (setParentLists.TryGetValue(key, out parents)) {
						foreach (Entity parent in parents) {
							if (isEntSet(parent, setKey)) {
								return true;
							}
						}
					}
				}
			}
			else {
				HashSet<Entity> sets;
				if (entSetLists.TryGetValue(key, out sets)) {
					if (sets.Contains(setKey)) {
						return true;
					}
				}
				if (specific) {
					return false;
				}
				
				// search through hierarchy of sets
				if (sets != null) {
					foreach (Entity setEnt in sets) {
						if (isEntSet(setEnt, setKey)) {
							return true;
						}
					}
				}
			}

			return false;
		}

		//------------------------------------------------------------
		public HashSet<Entity> entSets(Entity key, bool specific=false) {
			if (!exists(key)) {
				Debug.LogError("Can't call entSets on invalid entity: " + key);
			}

			HashSet<Entity> sets = new HashSet<Entity>();
			HashSet<Entity> returnSets = new HashSet<Entity>();

			if (isSet(key)) {
				if (setParentLists.TryGetValue(key, out sets)) {
					returnSets = new HashSet<Entity>(sets);
				}
			}
			else {
				if (entSetLists.TryGetValue(key, out sets)) {
					returnSets = new HashSet<Entity>(sets);
				}
			}
			
			if (specific || sets == null) {
				return returnSets;
			}
			
			foreach (Entity setEnt in sets) {
				returnSets.UnionWith(entSets(setEnt));
			}
			return returnSets;
		}

		//------------------------------------------------------------
		public HashSet<Entity> setEnts(Entity setKey, bool specific = false) {
			if (!exists(setKey)
				|| (setKey.type != EntType.nodeSet	
					&& setKey.type != EntType.linkSet)) {
				Debug.LogError("Can't call entSets on invalid entity: " + setKey);
			}

			HashSet<Entity> ents;
			if (setEntLists.TryGetValue(setKey, out ents)) {
				if (specific) {
					return new HashSet<Entity>(ents);
				}
				else {
					HashSet<Entity> returnEnts = new HashSet<Entity>(ents);
					HashSet<Entity> children;
					if (setChildLists.TryGetValue(setKey, out children)) {
						foreach (Entity child in children) {
							returnEnts.UnionWith(setEnts(child));
						}
					}
					return returnEnts;
				}
			}
			return new HashSet<Entity>();
		}

		//------------------------------------------------------------
		public HashSet<Entity> childSets(Entity setKey, bool specific=false) {
			if (!exists(setKey)) {
				Debug.LogError("Can't call childSets on invalid entity: " + setKey);
			}

			HashSet<Entity> sets;
			if (setChildLists.TryGetValue(setKey, out sets)) {
				if (specific) {
					return new HashSet<Entity>(sets);
				}
				else {
					HashSet<Entity> returnEnts = new HashSet<Entity>(sets);
					foreach (Entity child in sets) {
						returnEnts.UnionWith(childSets(child));
					}
					return returnEnts;
				}
			}
			return new HashSet<Entity>();
		}
		

		//------------------------------------------------------------
		public void addEntToSet(Entity key, Entity setKey) {
			if (!exists(key) || !exists(setKey)) {
				Debug.LogError("Can't call addEntToSet when invalid params: " 
							   + key + " :" + setKey);
				return;
			}
			
			HashSet<Entity> sets;

			// add to set hierarchy
			if (isSet(key)) {
				// add to setParentLists
				if (!setParentLists.ContainsKey(key)) {
					typeParentLists.Add(key, new HashSet<Entity>());
				}
				HashSet<Entity> parents = setParentLists[key];
				if (parents.Contains(setKey)) {
					return;
				}
				parents.Add(setKey);

				// add to setChildLists
				if (!setChildLists.ContainsKey(setKey)) {
					setChildLists.Add(setKey, new HashSet<Entity>());
				}
				HashSet<Entity> children = setChildLists[setKey];
				children.Add(key);

			}
			else {
				// add to entSetLists
				if (!entSetLists.TryGetValue(key, out sets)) {
					entSetLists.Add(key, new HashSet<Entity>());
					entSetLists.TryGetValue(key, out sets);
				}
				if (sets.Contains(setKey)) {
					return;
				}
				else {
					sets.Add(setKey);
				}
				
				// add to setEntLists
				HashSet<Entity> ents;
				if (!setEntLists.TryGetValue(setKey, out ents)) {
					setEntLists.Add(setKey, new HashSet<Entity>());
					setEntLists.TryGetValue(setKey, out ents);
				}
				ents.Add(key);
			}
		}

		//------------------------------------------------------------
		public void rmEntFromSet(Entity key, Entity setKey) {
			if (!exists(key) || !exists(setKey)) {
				Debug.LogError("Can't call rmEntFromSet when invalid params: "
							   + key + "\n" + setKey);
				return;
			}

			if (isSet(key)) {
				// rm from setChildLists	
				HashSet<Entity> sets;
				if (setChildLists.TryGetValue(setKey, out sets)) {
					if (!sets.Contains(key)) {
						return;
					}
					sets.Remove(key);
					if (sets.Count == 0) {
						setChildLists.Remove(setKey);
					}
				}
				
				// rm from setParentLists
				HashSet<Entity> ents;
				if (setParentLists.TryGetValue(key, out ents)) {
					ents.Remove(setKey);
					if (ents.Count == 0) {
						setParentLists.Remove(key);
					}
				}
			}
			else {
				// rm from entSetHashSets	
				HashSet<Entity> sets;
				if (entSetLists.TryGetValue(key, out sets)) {
					sets.Remove(setKey);
					if (sets.Count == 0) {
						entSetLists.Remove(key);
					}
				}
				
				// rm from setEntLists
				HashSet<Entity> ents;
				if (setEntLists.TryGetValue(setKey, out ents)) {
					ents.Remove(key);
					if (ents.Count == 0) {
						setEntLists.Remove(key);
					}
				}
			}
		}

		//------------------------------------------------------------
		public HashSet<Entity> allLinkSets() {
			HashSet<Entity> returnList = allEnts();
			returnList.RemoveWhere(x => x.type != EntType.linkSet);
			return returnList;
		}

		//------------------------------------------------------------
		public HashSet<Entity> allNodeSets() {
			HashSet<Entity> returnList = allEnts();
			returnList.RemoveWhere(x => x.type != EntType.nodeSet);
			return returnList;
		}



	}
}
