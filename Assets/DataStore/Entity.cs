using System;
using UnityEngine;

public class Entity : IComparable {
	public readonly string id;
	public readonly EntType type;

	//------------------------------------------------------------
	public Entity(string id, EntType entityType) {
		this.id = id;
		this.type = entityType;
	}

	//------------------------------------------------------------
	public override bool Equals(System.Object obj) {
		if (obj == null) {
			return false;
		}
    
		Entity k = obj as Entity;
		if ((System.Object)k == null) {
			return false;
		}
    
		return (id == k.id && type == k.type);
	}
  
	//------------------------------------------------------------
	public bool Equals(Entity k) {
		if ((object)k == null) {
			return false;
		}

		return (id == k.id && type == k.type);
	}

	//------------------------------------------------------------
	public override int GetHashCode() {
		return id.GetHashCode() * 23 
			+ type.GetHashCode() * 13;

	}

	//------------------------------------------------------------
	public override string ToString() {
		return type.ToString() + ":" + id;
	}

	//------------------------------------------------------------
	public int CompareTo(System.Object other) { 
		if (other == null) {
			return 1;
		}

		Entity ent = other as Entity;
		if (ent != null) {
			if (type == ent.type) {
				return id.CompareTo(ent.id);
			}
			else {
				return type.CompareTo(ent.type);
			}				 
		}
		else {
			throw new ArgumentException("Object is not a Entity");
		}
	}

	//------------------------------------------------------------
	public static bool operator ==(Entity lhs, Entity rhs) {
		if (object.ReferenceEquals(lhs, null)) {
			return object.ReferenceEquals(rhs, null);
		}

		return lhs.Equals(rhs);
	}

	//------------------------------------------------------------
	public static bool operator !=(Entity lhs, Entity rhs) {
		if (object.ReferenceEquals(lhs, null)) {
			return !object.ReferenceEquals(rhs, null);
		}
		return !lhs.Equals(rhs);
	}

}

