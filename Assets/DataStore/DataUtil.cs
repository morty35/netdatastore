using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Object = System.Object;

namespace DS {

	public static class DataUtil {

		//------------------------------------------------------------
		public static bool toBool(int data) {
			if (0 == data) {
				return false;
			}
			return true;
		}
		
		//------------------------------------------------------------
		public static int toInt(string data) {
			if (data != null && !data.Equals("")) {
				return Convert.ToInt32(data);
			}
			else {
				return 0;
			}
		}

		//------------------------------------------------------------
		public static float toFloat(string data) {
			if (data != null && !data.Equals("")) {
				return float.Parse(data);
			}
			else {
				return 0f;
			}
		}

		//------------------------------------------------------------
		public static Color toColor(string data) {
			if (data != null && !data.Equals("")) {
				string[] vals = data.Split(':');
				byte r = (byte) toInt(vals[0]);
				byte g = (byte) toInt(vals[1]);
				byte b = (byte) toInt(vals[2]);
				byte a = (byte) toInt(vals[3]);
				return new Color32(r,g,b,a);
			}
			else {
				return Color.white;
			}
		}

		//------------------------------------------------------------
		public static Entity toSingle(HashSet<Entity> ents) {
			if (ents == null || ents.Count == 0) {
				return null;
			}
			else if (ents.Count > 1) {
				Debug.LogError("Can't call toSingle on HashSet with " +
							   "multiple ents");
			}
			foreach (Entity ent in ents) {
				return ent;
			}
			return null;
		}

		//------------------------------------------------------------
		public static void swapLink(DataSet dataSet, Entity linkType, Entity parent,
									Entity oldChild, Entity newChild) {
			if (dataSet.exists(linkType.id + ":" + parent.id + ":" + oldChild.id)) {
				Entity childLink = dataSet.link(linkType, parent, oldChild);
				dataSet.rm(childLink);
			}
			dataSet.addLink(linkType, parent, newChild);
		}
	
		//------------------------------------------------------------
		public static string dataTypeString(DataType type) {
			switch (type) {
			case DataType.intType:
				return "int";
			case DataType.floatType:
				return "float";
			case DataType.stringType:
				return "string";
			case DataType.entityType:
				return "entity";
			case DataType.colorType:
				return "color";
			case DataType.dataType:
				return "dataType";
			}
			return null;
		}

		//------------------------------------------------------------
		public static string dataString(DataType type, object value) {
			if (value == null) {
				return "";
			}
			switch (type) {
			case DataType.intType:
				return ((int) value).ToString();
			case DataType.floatType:
				return ((float) value).ToString();
			case DataType.stringType:
				return (string) value;
			case DataType.colorType:
				Color color = (Color) value;
				Color32 color32 = color;
				return color32.r + ":" + color32.g + ":" + color32.b + ":" + color32.a;
			case DataType.entityType:
				Entity ent = (Entity) value;
				return ent.id;
			case DataType.dataType:
				DataObject dataO = (DataObject) value;
				return dataO.ToString();
			}
			return null;
		}

		//------------------------------------------------------------
		public static DataType dataType(Object value) {
			if (value is int) {
				return DataType.intType;
			}
			else if (value is float) {
				return DataType.floatType;
			}
			else if (value is string) {
				return DataType.stringType;
			}
			else if (value is Entity || value == null) {
				return DataType.entityType;
			}
			else if (value is Color) {
				return DataType.colorType;
			}
			else if (value is DataObject) {
				return DataType.dataType;
			}
			else {
				return DataType.objectType;
			}
		}			

		//------------------------------------------------------------
		public static DataType listType<T>() {
			if (typeof(T) == typeof(int)) {
				return DataType.intType;
			}
			else if (typeof(T) == typeof(float)) {
				return DataType.floatType;
			}
			else if (typeof(T) == typeof(string)) {
				return DataType.stringType;
			}
			else if (typeof(T) == typeof(Entity)) {
				return DataType.entityType;
			}
			else if (typeof(T) == typeof(Color)) {
				return DataType.colorType;
			}
			else if (typeof(T) == typeof(DataObject)) {
				return DataType.dataType;
			}
			else {
				return DataType.objectType;
			}
		}
		
		//------------------------------------------------------------
		public static DataType listDataType(Object value) {
			if (value is List<int>) {
				return DataType.intType;
			}
			else if (value is List<float>) {
				return DataType.floatType;
			}
			else if (value is List<string>) {
				return DataType.stringType;
			}
			else if (value is List<Entity> || value == null) {
				return DataType.entityType;
			}
			else if (value is List<Color>) {
				return DataType.colorType;
			}
			else if (value is List<DataObject>) {
				return DataType.dataType;
			}
			else {
				return DataType.objectType;
			}
		}			

		//------------------------------------------------------------
		public static string[] split(string line) {
			line = line.Replace("\\,", "&comma;");
			
			string[] cells = line.Split(',');
			for (int i = 0; i < cells.Length; i++) {
				cells[i] = cells[i].Replace("&comma;", ",");
			}
			return cells;
		}

		//------------------------------------------------------------
		public static string[] splitIntoLines(string contents) {
            string[] lines = contents.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
			for (int i = 0; i < lines.Length; i++) {
				lines[i] = lines[i].Replace("<break>", "\n");
			}
			return lines;
		}

		//------------------------------------------------------------
		public static string escapeString(string text) {
			text = text.Replace(",", "\\,");
			text = text.Replace("\n", "<break>");
			text = text.Replace("\r", "<break>");
			return text;
		}
		
		//------------------------------------------------------------
		public static int increment(DataSet data, Field field) {
			int amount = data.data<int>(field);
			amount++;
			data.edit<int>(field, amount);
			return amount;
		}

		//------------------------------------------------------------
		public static int increment(DataSet data, Entity ent, string fieldId) {
			Field field = data.field(ent, fieldId);
			return increment(data, field);
		}

		//------------------------------------------------------------
		public static int increase(DataSet data, Field field, int addAmount) {
			int amount = data.data<int>(field);
			amount += addAmount;
			data.edit<int>(field, amount);
			return amount;
		}


		//------------------------------------------------------------
		public static int increase(DataSet data, Entity ent, string fieldId,
								   int addAmount) {
			Field field = data.field(ent, fieldId);
			return increase(data, field, addAmount);
		}
		
		//------------------------------------------------------------
		public static int decrement(DataSet data, Field field) {
			int amount = data.data<int>(field);
			amount--;
			data.edit<int>(field, amount);
			return amount;
		}

		//------------------------------------------------------------
		public static int decrement(DataSet data, Entity ent, string fieldId) {
			Field field = data.field(ent, fieldId);
			return decrement(data, field);
		}

		//------------------------------------------------------------
		public static int decrease(DataSet data, Field field, int subAmount) {
			int amount = data.data<int>(field);
			amount -= subAmount;
			data.edit<int>(field, amount);
			return amount;
		}


		//------------------------------------------------------------
		public static int decrease(DataSet data, Entity ent, string fieldId,
								   int subAmount) {
			Field field = data.field(ent, fieldId);
			return decrease(data, field, subAmount);
		}

		//------------------------------------------------------------
		public static List<Entity> sort(DataSet data, HashSet<Entity> entSet,
										string field) {
			List<Entity> sortList = new List<Entity>(entSet);
			return sortList.OrderBy(x => data.data<object>(x, field)).ToList();
		}

		//------------------------------------------------------------
		public static List<Entity> sortedChildren(DataSet data, Entity linkType,
												  Entity parent, string field) {
			List<Entity> childLinks = new List<Entity>(data.childLinks(parent, linkType));
			childLinks = childLinks.OrderBy(x => data.data<object>(x, field)).ToList();

			List<Entity> children = new List<Entity>();
			foreach (Entity childLink in childLinks) {
				children.Add(data.linkChild(childLink));
			}
			return children;
		}

		//------------------------------------------------------------
		public static void rmDescendents(DataSet data, Entity parent, Entity linkType) {
			HashSet<Entity> descendents = data.descendents(parent, linkType);
			List<Entity> rmList = new List<Entity>();
			foreach (Entity descendent in descendents) {
				rmList.Add(descendent);
			}
			for (int i = 0; i < rmList.Count; i++) {
				if (rmList[i] != null) {
					data.rm(rmList[i]);
				}
			}
		}

		//------------------------------------------------------------
		public static int newMaxId(DataSet data, Entity nodeType) {
			HashSet<Entity> nodes = data.typeEnts(nodeType);
			int max = 0;
			foreach (Entity node in nodes) {
				string numS = Regex.Match(node.id, @"\d+$").Value;
				if (numS == null || numS == "") {
					continue;
				}
				int num = DataUtil.toInt(numS);
				if (num >= max) {
					max = num + 1;
				}
			}
			return max;
		}

		//------------------------------------------------------------
		public static int newMaxId(HashSet<Entity> ents) {
			int max = 0;
			foreach (Entity ent in ents) {
				string numS = Regex.Match(ent.id, @"\d+$").Value;
				if (numS == null || numS == "") {
					continue;
				}
				int num = DataUtil.toInt(numS);
				if (num >= max) {
					max = num + 1;
				}
			}
			return max;
		}

		//------------------------------------------------------------
		public static void deleteData(DataSet data, DataFilter deleteFilter) {
			HashSet<Entity> matches = data.allEnts();
			matches = deleteFilter.matches(matches, data);
			foreach (Entity ent in matches) {
				if (data.exists(ent)) {
					data.rm(ent);
				}
			}
		}
	}
}
